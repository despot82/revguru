<?php

/*
 
 Plugin Name: Nexmo for Revguru
 Description: Plugin for management of SMSs sent through Nexmo 
 Version: 1.0
 Author: Vladimir Despotovic
 
*/


add_action( 'admin_menu', 'register_nexmo_menu_item');

function register_nexmo_menu_item(){
	add_menu_page( 
			'Nexmo Management',         //page title 
			'Nexmo',               //menu title
			'manage_options',          //capability
			'nexmo',               //menu slug
			'nexmo_page',          //function
			plugins_url( 'nexmo/images/logo5.png' ),    //icon url
			72 );                      //position

}


function nexmo_page() {
    require "nexmo-manager-page.php";
}



?>