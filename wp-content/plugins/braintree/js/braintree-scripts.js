$(document).ready(function () {
	
$("#update_button").click(function(){ 	
	
	$("#status").val("UPDATING...");
	
	//alert($("input[name=braintree_payment_option]:checked").val());
	
    $.ajax({  
    	       url: "../wp-content/plugins/braintree/change-payment-option.php", 
    	       type: "POST", 
    	       dataType:'json',					        		      
			   data: { "btpo" : $("input[name=braintree_payment_option]:checked").val() },    	       
    	       cache: false
    	       
           })           
           .done(function(html) {        	   
        	   $("#status").val("UPDATED...");        	       	             
	       });    
});


});
