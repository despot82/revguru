<script src="http://code.jquery.com/jquery-2.1.0.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<?php 
    wp_enqueue_script("SAA_scripts", plugins_url("/js/braintree-scripts.js", __FILE__) );
    
    if(!get_option("braintree_payment_option") ) {
    	$o = 0;
    } else {
    	$o = get_option("braintree_payment_option");
    }
    
?>

<br><br>
Choose the way guests will pay for the reservations:
<form>
    
    <input type="radio" name="braintree_payment_option" value="1" <?php if($o == 1) echo "checked='checked'"; ?>>Deposit (Pay 10% now, pay rest when you arrive at the hotel)<br>
    <input type="radio" name="braintree_payment_option" value="2" <?php if($o == 2) echo "checked='checked'"; ?>>Pay now (Pay the whole amount on our website)<br>
    <input type="radio" name="braintree_payment_option" value="3" <?php if($o == 3) echo "checked='checked'"; ?>>Pay later (Pay the whole amount at the hotel, preauthorization is needed)<br>
    <br>
    <input type="button" id="update_button" value="Update" />
    <br>
    <input type="text" id="status" disabled="disabled">
</form>