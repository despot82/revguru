<?php

require "Interfaces/IInboundEasyReservations.php";

class MobiusToERAdapter implements IInboundEasyReservations
{
	var $er;
	var $mapper;
	var $mobiousSoapClient;
	
	public function __construct(EasyReservationsInboundRestService $er_par, MobiusMapper $mp_par)
	{
		$this -> er = $er_par;		
		$this -> mapper = $mp_par;
	}
	
	private function convertToERData($type, $value)
	{
	    	
	    	
	}
	
	public function getAllResources()
	{
		return $this -> er -> getAllResources();
	}
	
	public function getResource($id)
	{
		
		return $this -> er -> getResource($id);
	}
	
	public function edit_resource($id, $data)
	{
		return $this -> er -> edit_resource($id, $data);
		
	}          

	public function get_availability_for_resource_on_date($resource, $date, $persons)
	{
		return $this -> er -> get_availability_for_resource_on_date($resource_id, $arrival, $persons);
		
	}	
	
	public function getAvailabilityForResourceInPeriod($resource, $arrival, $departure, $persons)
	{
		return $this -> er -> getAvailabilityForResourceInPeriod($resource_id, $arrival, $departure, $persons);
		
	}
		
	public function make_reservation_for_date($resource = null, 
			                               $resourcenumber = null,
			                               $arrival = null,
			                               $departure = null, 
			                               $name = null,
			                               $email, 
			                               $country = null,
			                               $adults = null,
			                               $childs = null,                        
			                               $custom = null,   
			                               $prices = null,   
			                               $status = null,   
			                               $interval = null,   
			                               $reservated = null,   
			                               $times = null,   
			                               $user = null,   
			                               $price = null,   
			                               $paid = null,		                              
			                               $pricepaid = null,   
			                               $resourcename = null)
	{
	    return $this -> er -> makeReservationForDate($resource, 
			                                     	 $resourcenumber,
			                                     	 $arrival,
			                                     	 $departure, 
			                                     	 $name,
			                                     	 $email, 
			                                     	 $country,
			                                     	 $adults,
			                                     	 $childs,                        
			                                     	 $custom,   
			                                     	 $prices,   
			                                     	 $status,   
			                                     	 $interval,   
			                                     	 $reservated,   
			                                     	 $times,   
			                                     	 $user,   
			                                     	 $price,   
			                                     	 $paid,		                              
			                                     	 $pricepaid,   
			                                     	 $resourcename);             	
			                               	
	}
	
	public function make_reservation_for_period($resource = null, 
			                            	   $resourcenumber = null,
			                               	   $arrival = null,
			                               	   $departure = null, 
			                                   $status = null,			
			                               	   $name = null,
			                               	   $email = null, 
			                               	   $country = null,
			                               	   $adults = 1,
			                               	   $childs = null,                        
			                               	   $custom = null,   
			                               	   $prices = null,   			                                      
			                               	   $interval = null,   
			                                   $reservated = null,   
			                                   $times = 1,   
			                                   $user = null,   
			                                   $price = null,   
			                                   $paid = null,		                              
			                                   $pricepaid = null,   
			                                   $resourcename = null)
	{
		return $this -> er -> make_reservation_for_period($resource,
				                                       $resourcenumber,
				                                       $arrival,
				                                       $departure,
				                                       $name,
				                                       $email,
				                                       $country,
				                                       $adults,
				                                       $childs,
				                                       $custom,
				                                       $prices,
				                                       $status,
				                                       $interval,
												       $reservated,
												       $times,
												       $user,
												       $price,
												       $paid,
												       $pricepaid,
												       $resourcename);
	    		                                   	
	}
	
	
	
	
}