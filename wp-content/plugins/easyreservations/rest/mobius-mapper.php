<?php
class MobiusMapper
{
		
	public function __construct()
	{
				
		
	}
	
	// Transform Id-s from Mobius to ER
  	public function getMobiusResourceIdAsEasyReservationsResourceId ( $mobiusResourceId )
   	{
   		global $wpdb;
   		
   		$sql = $wpdb -> prepare("SELECT id_er FROM mobius_resources_mappings WHERE id_mobius = %s", $mobiusResourceId );
   		$id_er = $wpdb ->get_results($sql);
   		
   		return $id_er[0] -> id_er;   		        		
   	}
    	
   	public function getMobiusHotelIdAsEasyReservationsHotelId ( $mobiusHotelId )
   	{
   		global $wpdb;
   		 
   		$sql = $wpdb -> prepare("SELECT id_er FROM mobius_hotels_mappings WHERE id_mobius = %s", $mobiusHotelId );
   		$id_er = $wpdb -> get_results($sql);
   		
   		return $id_er[0] -> id_er;        
   	}
    	
   	public function getMobiusReservationIdAsEasyReservationsReservationId ( $mobiusReservationId )
   	{
   		global $wpdb;
   		
   		$sql = $wpdb -> prepare("SELECT id_er FROM mobius_reservations_mappings WHERE id_mobius = %s", $mobiusReservationId );
   		$id_er = $wpdb -> get_results($sql);
   		 
   		return $id_er[0] -> id_er;
    		
   	}
   	
   	//Transform Id-s from ER to Mobius
   	public function getEasyReservationsResourceIdAsMobiusResourceId( $erResourceId ) 
   	{
   		global $wpdb;
   		 
   		$sql = $wpdb -> prepare("SELECT id_mobius FROM mobius_resources_mappings WHERE id_er = %s", $erResourceId );
   		$id_er = $wpdb -> get_results($sql);
   		 
   		return $id_er[0] -> id_mobius;
    	
   	}
    	 
   	public function getEasyReservationsHotelIdAsMobiusHotelId( $erHotelId ) 
   	{
   		global $wpdb;
   		
   		$sql = $wpdb -> prepare("SELECT id_mobius FROM mobius_hotels_mappings WHERE id_er = %s", $erHotelId );
   		$id_er = $wpdb -> get_results($sql);
   		 
   		return $id_er[0] -> id_mobius;    	
   	}
    	 
   	public function getEasyReservationsReservationIdAsMobiusReservationId( $erReservationId ) 
   	{
   		global $wpdb;
   		 
   		$sql = $wpdb -> prepare("SELECT id_mobius FROM mobius_reservations_mappings WHERE id_er = %s", $erReservationId );
   		$id_er = $wpdb -> get_results($sql);
   		
   		return $id_er[0] -> id_mobius;    	
   	}
    	
    	
}