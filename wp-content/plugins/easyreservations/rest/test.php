<?php
   
    
    $null = null;   //variable that has a null value
    $empty = "";    //variable that has a '' value
    global $t;    //variable that has not been set to any value at all
    
    // And now for the first variable, the null one.  **********************************
    if (empty($null)) {
    	print "empty() gives true when given null<br/>";
    } else {
    	print "empty() gives false when given null<br/>";
    }
    
    if (is_null($null)) {
    	print "is_null() gives true when given null<br/>";
    } else {
    	print "is_null() gives false when given null<br/>";
    }
    
    if (isset($null)) {
    	print "isset() gives true when given null<br/>";
    } else {
    	print "isset() gives false when given null<br/>";
    }
    
    echo "<br><br>";
    
    
    // And now for the second variable, the empty one.  ************************************************ 
    if (empty($empty)) {
    	print "empty() gives true when given ''<br/>";
    } else {
    	print "empty() gives false when given ''<br/>";
    }
    
    if (is_null($empty)) {
    	print "is_null() gives true when given ''<br/>";
    } else {
    	print "is_null() gives false when given ''<br/>";
    }
    
    if (isset($empty)) {
    	print "isset() gives true when given ''<br/>";
    } else {
    	print "isset() gives false when given ''<br/>";
    }
    
    
    echo "<br><br>";
    
    
    
    // And now for the third variable, the not set one.  ************************************************
    if (empty($t)) {
    	print "empty() gives true when given a not set variable<br/>";
    } else {
    	print "empty() gives false when given a not set variable<br/>";
    }
    
    if (is_null($t)) {
    	print "is_null() gives true when given a not set variable<br/>";
    } else {
    	print "is_null() gives false when given a not set variable<br/>";
    }
    
    if (isset($t)) {
    	print "isset() gives true when given a not set variable<br/>";
    } else {
    	print "isset() gives false when given a not set variable<br/>";
    }