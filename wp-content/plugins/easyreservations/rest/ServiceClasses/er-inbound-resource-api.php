<?php

//require_once  (realpath(dirname(dirname(__FILE__))) . "/Interfaces/i-inbound-resources.php");
require_once (ABSPATH . "wp-content/plugins/easyreservations/rest/Interfaces/i-inbound-resources.php");
require_once (ABSPATH . "wp-content/plugins/easyreservations/lib/classes/resource.class.php");

class ER_Inbound_Resource_API implements I_Inbound_Resources
{
	var $rates;
	var $rates_custom_field_id;
	
	public function ER_Inbound_Resource_API($rates = null, $rates_custom_field_id = null )
	{
		//If rates have not been explicitly given, get the default hotel board rates
		if(empty($rates) || empty($rates_custom_field_id)) {
			$hotel_custom_fields = get_option("reservations_custom_fields");
			
			//Find rates custom field
			if (!empty($hotel_custom_fields))
			foreach($hotel_custom_fields["fields"] as $custom_field_id => $custom_field) {
				if ($custom_field["title"] == "rates") {
					$rates = $custom_field["options"];
					$rates_custom_field_id = $custom_field_id;
					break;
				}
			}
		}
		
		$this -> rates = $rates;
		$this -> rates_custom_field_id = $rates_custom_field_id;
	}
	
	public function getAllResources() {
		
		$resources = $wpdb->get_results("SELECT ID, post_title FROM ".$wpdb->prefix."posts WHERE post_type = 'easy-rooms' ORDER BY menu_order");
		
		return $resources;
	}
	
	public function getResource($id) {
		 
		$resource = new Resource($id);
		
		return  $resource;
	}
	
	
	
	public function deleteResource($id)
	{
		$r = new Resource($id);	
		
		//Returns false on error, int on success, where int is number of rows affected
		$reservations_deleted = $wpdb -> query( $wpdb -> prepare("DELETE FROM " . $wpdb -> prefix . "reservations WHERE room='%s' ", $id) );
		
		//Returns false if ok, true if error
		$resource_deleted = $r -> deleteResource();		
		
		if(!$reservations_deleted || $resource_deleted) {
		    return array("success" => 0, "Reservations couldn't be deleted.");
		}
        
		if(!$resource_deleted) {
			return array("success" => 0, "Resource couldn't be deleted.");
		}
		 
		return array("success" => 1, "Resource and its reservations were deleted succesfuly.");
	
	}
	
	/**
	 * 
	 * Function returns the availability info of the resource in given period
     *
     * @param Integer $resource_id
     * @param formatted_date $arrival (in format yyyy-mm-dd_hh:ii:ss)
     * @param formatted_date $departure (in format yyyy-mm-dd_hh:ii:ss)
     * @param Integer $adults
     * @param Integer $childs
     * @param string $busyMode
     * @param Integer $hotelId
     *
     * @return For availability-per-object resource is:
     *  array(is_available,
     *        error_msg (in case is_available == 0),
     *        
     *		  roomcount (in case is_available == 1),
     *        number_of_available_rooms (in case is_available == 1),
     *        available_room_numbers (in case is_available == 1),
     *		  price (in case is_available == 1),
     *		  intervals (in case is_available == 1)
     *       ) 
	 */
	public function get_availability_for_resource_in_period($resource_id = 1, $arrival = 0, $departure = 0, $adults = 1, $childs = 0, $busyMode = false)
	{		
		/*
		print "DEV: resource, arrival, departure, adults, childs are: <pre>";
		print_r($resource_id);
		print_r($arrival);
		print_r($departure);
		print_r($adults);
		print_r($childs);
		print "</pre>";
		*/
		
		
		//error_log ("reservation arrival and departure from get_availability_for_resource_in_period from ERInboundResourceAPI are: " . $arrival . " and " . $departure);
		
		$roomcount = get_post_meta($resource_id, 'roomcount', true);
		 
		if(is_array($roomcount)) {			
			return $this -> getAvailabilityForPerPersonResource($resource_id, $arrival, $departure, $adults, $childs, $busyMode);
		} else {			
			return $this -> getAvailabilityForPerObjectResource($resource_id, $arrival, $departure, $adults, $childs, $busyMode);
		}
           
    }
    
    /**
     * @param number $resource_id
     * @param formatted_date $arrival (in format yyyy-mm-dd_hh:ii:ss)
     * @param formatted_date $departure (in format yyyy-mm-dd_hh:ii:ss)
     * @param number $adults
     * @param number $childs
     * @param string $busyMode
     * @param number $hotelId
     * 
     * @return array(is_available,
     *                error_msg (in case is_available == 0),
     *		         roomcount (in case is_available == 1),
     *		         number_of_available_rooms (in case is_available == 1),
     *		         available_room_numbers (in case is_available == 1),
     *		         price (in case is_available == 1),
     *		         intervals (in case is_available == 1)) 		     
     */
    public function getAvailabilityForPerObjectResource($resource_id = 1, $arrival = 0, $departure = 0, $adults = 1, $childs = 0, $busyMode = false, $hotelId = 0)
    {
    	/*
    	print "DEV: data from getAvaForPerObjectResource:<pre>";
    	print_r(array("resource_id" => $resource_id,
    	              "arrival" => $arrival,
    	              "departure" => $departure,
    	              "adults" => $adults,
    	              "childs" => $childs,
    	              "busymode" => $busyMode,
    	              "hotelId" => $hotelId));
    	print "</pre>";
    	*/
    	
    	if( get_post($resource_id) == null) return array("error_msg" => "Resource with ID " . $resource_id. " cannot be found.",
    				                                     "is_available" => -1
    		                                            );
    	
    	//error_log ("DEV: reservation arrival and departure from inbound resource API availability check are: " . $arrival . " and " . $departure);
    	//error_log ("DEV: resource is: " . $resource_id);
    	$interval = easyreservations_get_interval ( 0, $resource_id, 0 ) ;
    	
    	$roomcount = get_post_meta($resource_id, 'roomcount', true);
    	    	
    	if ($arrival == 0 || is_null($arrival)) {
    		
    		//print "DEV: arrival is null:<pre>"; print_r($arrival); print "</pre>";
    		
    		return array("error_msg" => "Arrival date must be set" ,
    				     "is_available" => -1
    		);
    	}
    	
    	$arrival_epoch = strtotime(str_replace("_", " ", $arrival));
    	if($departure == 0) {
    		$departure_epoch = $arrival_epoch + $interval;
    		$departure = date("Y-m-d H:i:s", $departure_epoch);
    			
    	} else {
    		$departure_epoch = strtotime(str_replace("_", " ", $departure));
    	
    	}
        
    	$res = new Reservation(false, array('dontclean',
    										'name' => 'Availability checker',
    										'email' => 'availability@checker.com',
    										'resource' => $resource_id,
    										'arrival' => $arrival_epoch,
    										'departure' => $departure_epoch,
    										'adults' => $adults,
    										'childs' => isset($childs) ? $childs : 0,
    										'status' => 'yes',
    										'admin' => '0'
    	
    	));
    	
    	$res -> interval = $interval;
    	 
    	if ($res -> Validate() ) {

    		$reason = "";
    		foreach ($res -> Validate() as $v) {
    			if(substr($v, 0, 9) != "easy-form") {
    				$reason .= $v."; ";
    			}
    		}
    		return array("error_msg" => "Resource with id " . $resource_id . " is not available due to filters and/or requirements. Reason(s): " . $reason,
    				     "is_available" => 0);
    	}
    	
    	
    	$roomDayPersons = round($roomcount - $res -> checkAvailability(3), 1);
    	if($roomDayPersons <= 0) {
    		return array("error_msg" => "Resource with id " . $resource_id . " is not available in the given period.",
    		             "is_available" => 0);
    	}    	
    	     	
    	$res -> Calculate();
    	
    	$vacant_rooms = $this -> get_instances_without_reservations_in_period($resource_id, $arrival, $departure, $hotelId);
    	
    	return array("roomcount" => $roomcount,
    			     "number_of_available_rooms" => count($vacant_rooms),
    				 "available_room_numbers" => $vacant_rooms,
    				 "price" => $res -> Calculate(),
    				 "intervals" => $res -> getTimes(),
    			     "is_available" => 1
    	);
    }
    
    /**
     * @param number $resource_id
     * @param number $arrival
     * @param number $departure
     * @param number $adults
     * @param number $childs
     * @param string $busyMode
     * 
     * @return array(
     *               roomcount,
     *   			 is_available,
     *               error_msg (if is_available == 0),
     *               price,
     *			     intervals
     *              )
     * 
     */
    public function getAvailabilityForPerPersonResource($resource_id = 1, $arrival = 0, $departure = 0, $adults = 1, $childs = 0, $busyMode = false)
    {    	
    	
    	$interval = easyreservations_get_interval ( 0, $resource_id, 0 ) ;
    	
    	$roomcount = get_post_meta($resource_id, 'roomcount', true);
    	
    	$roomcount = $roomcount[0];
    	
    	$arrival_epoch = strtotime(str_replace("_", " ", $arrival));
    	if($departure == 0) {
    		$departure_epoch = $arrival_epoch + $interval;
    		$departure = date("Y-m-d H:i:s", $departure_epoch);
    			
    	} else {
    		$departure_epoch = strtotime(str_replace("_", " ", $departure));    	
    	}
    	
    	$res = new Reservation(false, array('dontclean',
    										'name' => 'Availability checker',
    										'email' => 'availability@checker.com',
    										'resource' => (int) $resource_id,
    										'arrival' => $arrival_epoch,
    										'departure' => $departure_epoch,
    										'adults' => $adults,
    										'childs' => $childs,
    										'status' => 'yes',
    										'admin' => '0'
    	
    	));
    	
    	$res -> interval = $interval;
    	
    	/* Unavailability and filters check */
        /*
    	echo "<br/>res->Validate()<br/>";
    	print_r ($res->Validate());
    	echo "<br/>res->checkAvailability(0):<br/>";
    	print_r ($res->checkAvailability(0));
    	echo "<br/>res->checkAvailability(1):<br/>";
    	print_r ($res->checkAvailability(1));
    	echo "<br/>res->checkAvailability(2):<br/>";
    	print_r ($res->checkAvailability(2));
    	echo "<br/>res->checkAvailability(3):<br/>";
    	print_r ($res->checkAvailability(3));
    	echo "<br/>res->checkAvailability(4):<br/>";
    	print_r ($res->checkAvailability(4));
    	echo "<br/>res->checkAvailability(5):<br/>";
    	print_r ($res->checkAvailability(5));
    	echo "<br/>end printing of res->checkAvailability<br/>";
    	*/
    	     	 
    	$roomDayPersons = round($roomcount - $res -> checkAvailability(0), 1);
    	 
        if($roomDayPersons < $res -> adults + $res -> childs ) {
        	return array("roomcount" => $roomcount,
        			     "is_available" => 0,
        			     "error_msg" => "Resource with id " . $resource_id . " is not available in the given period. There is not enough room available."
        	);		     		
    	}
    	 
    	if ($res -> Validate() ) {
    	
    		$reason = "";
    		foreach ($res -> Validate() as $v) {
    			if(substr($v, 0, 9) != "easy-form") {
    				$reason .= $v."; ";
    			}
    		}
    		return array ("roomcount" => $roomcount,
    				      "is_available" => 0,
    				      "error_msg" => "Resource not available due to filters and/or requirements. Reason(s): ".$reason
    		);
    	}
    	
    	$res -> Calculate();
    
    	return array("roomcount" => $roomcount,
    			     "is_available" => 1,    			     
    			     "price" => $res -> price,
    			     "intervals" => $res -> getTimes());    	
    	 
    }
    
       
    public function getAllAvailablePerPersonResources($roomtype, $arrival, $departure, $adults, $childs )
    {
    	
    	/* 
    	 * algorythm:
    	 * 1.Go through all resource types 
    	 * 2.Save the ones who are per person and have that number of people available
    	 * 3.Of the ones from 2. select the ones that fullfill the availability requirement.
    	 * 4.Return those from 3
        */
    	
    	//Find all rooms with 
    	$candidateRooms = $wpdb->get_results(" SELECT post_id " 
    			                             ."FROM ".$wpdb->prefix ."postmeta " 
    			                             ."WHERE 1=1 "
    			                             ."AND meta_key = 'roomcount' "
    			                             ."AND meta_value ='a:1:{i:0;s:" . (($roomtype < 10) ? 1 : 2) . ":\"" . $roomtype . "\";}' "
    			                             ."ORDER BY post_id asc");    	
    	
    	$availableRoomtypes = array();
    	
    	foreach($candidateRooms as $cr) {
    		
    		$crAvailability = $this -> getAvailabilityForPerPersonResource($cr -> post_id ,    				
                $arrival, $departure, $adults, $childs );
    		
    		if ($crAvailability["is_available"] == 1) {
    			$availableRoomtypes[] = array("resource_id" => $cr -> post_id, "price" => $crAvailability["price"]) ;
    		}    		
    	}
    	
    	return $availableRoomtypes;
    	
    }    

    public function edit_resource($id, $title, $content, $price, $childDiscount, $isPricePerPerson, $availability  )
    {
    	
    	$resource = new Resource($id);
    	
    	$toEdit = array();
    	
    	if(!is_null($title) && ($resource -> title != $title)) {
    		$toEdit[] = "title";
    		$resource -> title = $title;
    	}
    	if(!is_null($content) && ($resource -> content != $content)) {
    		$toEdit[] = "content";
    		$resource -> content = $content;
    	} 
    	if(!is_null($price)) $toEdit[] = "groundprice";
    	if(!is_null($childDiscount)) $toEdit[] = "child";
    	if(!is_null($isPricePerPerson)) $toEdit[] = "persons";
    	if(!is_null($availability)) $toEdit[] = "count";
    	
    	$resource -> groundprice = $price;
    	$resource -> child = $childDiscount;    	
    	$resource -> count = $availability;
    	$resource -> persons = ($isPricePerPerson == 0) ? array(0) : array(1);    	
    	
    	//print "toedit is: <pre>"; print_r($toEdit); print "</pre>";
    
    	return $resource -> editResource($toEdit); 
    	   	
    }
    

    public function addResource($title, $content, $type, $price, $child, $availability, $isPricePerPerson, $minPersons, $maxPersons, $minNights, $maxNights)
    {
    	
    	$req = array("pers-min" => $minPersons,
    			     "pers-max" => $maxPersons,
    			     "nights-min" => $minNights,
    			     "nights-max" => $maxNights
    	  ); 
    	
    	//$filters = do similar as req above
    	
    	require_once (realpath(dirname(dirname(dirname(__FILE__)))) . "/lib/classes/resource.class.php");
    	
    	
    	$r = new Resource(false, array("title" => $title, 
    			                       "content" => $content, 
    			                       "type" => $type, 
    			                       "groundprice" => $price,
    			                       "child" => $child,   
    			                       "persons" => ($isPricePerPerson == 0)? null : array(1),    			                       
    			                       "count" => $availability,
    			                       "countnames" => null, 
    			                       "req" => $req
    			                     
    	 ) );
    	
    	$r -> addResource(); //Throwing exceptions if can't be added properly, returning null if all went fine
    	
    	//Per ER as it is now, this type is only changed after the resource is made, so we do it after adding the resource
    	$this -> setAvailabilityType($r -> id, $type);
    	 
    	return $r;
    	
    }
    
    //Only used for availability per-object resources
    public function get_instances_without_reservations_in_period($resource_id, $arrival, $departure, $hotelId = 0)
    {	
    	global $wpdb;
    	
    	if($hotelId != 0) {
    		$prefix = "wp_" . $hotelId . "_";
    	} else {
    		$prefix = $wpdb -> prefix;
    	}
    	
    	// Find all roomnumber-s that are occupied (have at least one approved reservation for them) 
    	// in any part of the period:
    	$sql = $wpdb -> prepare(" SELECT distinct roomnumber "
    	                        ."FROM " . $prefix . "reservations re " 
    	                        ."WHERE 1=1 "
    	                        ."AND approve = 'yes' "
    	                        ."AND room = %s "    	
    	                        ."AND NOT (arrival < %s AND departure < %s) "
    	                        ."AND NOT (arrival > %s AND departure > %s) ",  
    		                    $resource_id, 
    		                    $arrival, 
    		                    $arrival,  
    		                    $departure, 
    		                    $departure);
    	
    	//error_log($sql);
    	 
    	//Available per object resource instances in period of time
    	$occupied_rooms_result = $wpdb -> get_results($sql);
    	
    	$occupied_rooms = array();    	
    	
    	foreach ($occupied_rooms_result as $r)
    	{
    		$occupied_rooms[] = $r -> roomnumber;
    	}
    	
    	$vacant_rooms = array();
    	
    	//Find the vacant rooms, those are rooms that are not occupied
    	for ($i = 1; $i <= get_post_meta($resource_id, 'roomcount', true); $i++) {
    	        	
    	    if(!in_array($i, $occupied_rooms)) {
    	        $vacant_rooms[] = $i;	
    	    }		
    	}
    	
    	return $vacant_rooms;
    	
    }
    
    public function setAvailabilityType($resourceId, $type)
    {
    
    	if ($type == 1) { 	//Setting availability to per-object
    		if(is_array(get_post_meta($resourceId, 'roomcount', TRUE))) {
    			update_post_meta($resourceId, 'roomcount', get_post_meta($resourceId, 'roomcount', TRUE)[0]);
    		}
    	}
    
    	if ($type == 2) {   //Setting availability to per-person
    		if(!is_array(get_post_meta($resourceId, 'roomcount', TRUE))) {
    			update_post_meta($resourceId, 'roomcount', array(get_post_meta($resourceId, 'roomcount', TRUE)));
    		}
    	}
    	 
    }
    
    public function add_resource_filter($general_filter_parameters, $filter_specific_parameters)
    {
    	
    	$resourceId = $general_filter_parameters["resourceId"]; 
    	$name = $general_filter_parameters["name"];
    	$type = $general_filter_parameters["type"];   
    	
    	if(empty($type)) {
    		return array("success" => 0, "msg" => "Type for the filter is not set. Please set the filter type and try again.");
    	}
    	
    	$r = new Resource($resourceId);
    	
    	$filters = $r -> fitlers;
    	
    	$newFilter = array();
    	$newFilter["type"] = $type;
    	$newFilter["name"] = $name;
    	
    	//print "filters for the resource:<pre>"; print_r ($filters); print "</pre>";    	
    	
    	if(strtolower($type) == "rates") {
    		$newFilter["type"] = "rates";
    		
    		foreach($filter_specific_parameters as $board_rate_name => $price) {
    			$newFilter[$board_rate_name] = $price;
    		}
    	}
    	
    	if(strtolower($type) != "rates") {//make distinction for future more detailed care for the REST service method
    	
    	    foreach($filter_specific_parameters as $other_parameters_key => $other_parameter_value) {
    			$newFilter[$other_parameters_key] = $other_parameter_value;
    		}	
    	}
    	
    	$filters[] = $newFilter;
    	
    	$r -> fitlers = $filters;
    	
    	$result = $r -> editResource("fitlers"); 
    	
    	if(!$result) {
    	    return array("success" => 1, "msg" => "Filter was added successfuly.");
    	} else {
    		return array("success" => 0, "msg" => "Filter was not added successfuly, because resource couldn't be edited.");
    	}
    
    }
    
    public function edit_resource_filter($general_filter_parameters, $filter_specific_parameters)
    {
    	$resourceId = $general_filter_parameters["resourceId"];
    	$filter_id = $general_filter_parameters["filter_id"];
    	$type = $general_filter_parameters["type"];
    	$name = $general_filter_parameters["name"];
    	 
    	if(empty($type)) {
    		return array("success" => 0, "msg" => "Type for the filter is not set. Please set the filter type and try again.");
    	}
    	 
    	$r = new Resource($resourceId);
    	 
    	$filters = $r -> fitlers;
    	
    	if(empty($filters[$filter_id])) {
    		return array("success" => 0, "msg" => "The filter with id " . $filter_id . " does not exist.");
    	}
    	 
    	$filter_to_edit = $filters[$filter_id];
    	$filter_to_edit["type"] = $type;
    	$filter_to_edit["name"] = $name;
    	 
    	//print "filters for the resource:<pre>"; print_r ($filters); print "</pre>";
    	 
    	if(strtolower($type) == "rates") {
    	
    		foreach($filter_specific_parameters as $board_rate_name => $price) {
    			$filter_to_edit[$board_rate_name] = $price;
    		}
    	}
    	 
    	if(strtolower($type) != "rates") {//make distinction for future more detailed care for the REST service method
    	    	 
    		foreach($filter_specific_parameters as $other_parameter_key => $other_parameter_value) {
    			$filter_to_edit[$other_parameter_key] = $other_parameter_value;
    		}
    	}   
    	//print "filters for the resource:<pre>"; print_r ($filters); print "</pre>";
    	
    	$filters[$filter_id] = $filter_to_edit;
    	 
    	$r -> fitlers = $filters;
    	 
    	$result = $r -> editResource("fitlers");
    	 
        if(!$result) {
    	    return array("success" => 1, "msg" => "Filter was edited successfuly.");
    	} else {
    		return array("success" => 0, "msg" => "Filter was not edited successfuly, because resource couldn't be edited.");
    	}
    	
    }
    
    public function delete_resource_filter($array)
    {
    	$resourceId = $array["resourceId"];
    	$filter_id = $array["filter_id"];
    	
    	$r = new Resource($resourceId);
    	
    	$filters = $r -> fitlers;
    	
    	$updated_filters = array();
    
    	//print "filters for the resource:<pre>"; print_r ($filters); print "</pre>";
    
    	foreach($filters as $key => $value) {
    		if($key != $filter_id) $updated_filters[$key] = $value;
    	}
    
    	$r -> fitlers = $updated_filters;
    
    	$result = $r -> editResource("fitlers"); 
    	
        if(!$result) {
    	    return array("success" => 1, "msg" => "Filter was deleted successfuly.");
    	} else {
    		return array("success" => 0, "msg" => "Filter was not deleted successfuly, because resource couldn't be edited.");
    	}
    
    }
    
   
    /**
     * @param string $resource
     * @param unknown $arrival
     * @param number $adults
     * @param number $rates - Hotel custom field representing board rates
     * @param number $rates_custom_field_id - id of the rates custom field
     * @param number $adults - Specified number of people
     * 
     * Return array [$code1] = array("name" => "name of board 1", "price" => $price1);
     *              [$code2] = array("name" => "name of board 2", "price" => $price2);
     *              ....
     */
    public function get_all_board_rates_for_resource($resource_id = null, $quantity = 1, $arrival = null, $departure = null, $adults = 1)
    {
    	/*
    	 	
    	0. Get values for global custom price fields for the hotel, to get the rates dictionary
    	mappings for each board type.
    	1. For each resource ($rooms as $room) fetch rates filter and persons price increase/filter(s) filter.
    	2. For each resource, for each persons filter, for each rate filter make a fake 1-day reservation
    	with the selected values for resource, persons filter and rate filter, to determine price for each.
    	3. Each of the resuls from 2. is a row to display.
    
    	*/
    	
    	//print "DEV: rates and id are: <pre>"; print_r($rates); print_r($rates_custom_field_id); print "</pre>";
    	
    	$interval = easyreservations_get_interval(0, $resource_id, 0);
    
    	$arrival_epoch = strtotime(str_replace("_", " ", $arrival));
    
    	$departure_one_day_epoch = $arrival_epoch + $interval;
    	
    	$departure_whole_stay_epoch = strtotime(str_replace("_", " ", $departure));
    	
    	//echo "<br>departure_one_day_epoch: " . $departure_one_day_epoch;
    	//echo "<br>departure_whole_stay_epoch: " . $departure_whole_stay_epoch;
    	
    	//print "DEV: custom fields are:<pre>"; print_r($hotel_custom_fields); print "</pre>";
        $rates = $this -> rates;
        $rates_custom_field_id = $this -> rates_custom_field_id;
    
    	$rates_prices = array();
    	
    	//Form the reservation for one day (one interval) without custom fields
    	$prep = array("resource" => $resource_id,
    		"arrival" => $arrival_epoch,
    		"departure" => $departure_one_day_epoch,
    		"adults" => $adults,
    		"status" => "dis",
    		"admin" => 0,
    		"fake" => 1
    	);
    	 
    	$reservation_one_day = new Reservation(false, $prep, 1);
    	
    	//Form the reservation for whole stay (one interval) without custom fields
    	$prep = array("resource" => $resource_id,
    		"arrival" => $arrival_epoch,
    		"departure" => $departure_whole_stay_epoch,
    		"adults" => $adults,
    		"status" => "dis",
    		"admin" => 0,
    		"fake" => 1
    	);
    	
    	$reservation_whole_stay = new Reservation(false, $prep, 1);
    	
    	//Foreach rate calculate price of reservation
    	if(!empty($rates)) {
	    	foreach ($rates as $code => $name_array) {
	    
	    		$new_custom = array();
	    		$new_custom[] = array("type" => "cstm", "mode" => "edit", "id" => $rates_custom_field_id, "value" => $code );
	   
	    		//Add the custom board rate price to reservation for one day
	    		$reservation_one_day -> Customs($new_custom, 1, null, 1, "cstm", null, null);
	            $reservation_one_day -> Calculate();
	    		$price_one_day = $reservation_one_day -> price;
	    	
	    		//Add the custom board rate price to reservation for whole stay
	    		$reservation_whole_stay -> Customs($new_custom, 1, null, 1, "cstm", null, null);
	    		$reservation_whole_stay -> Calculate();
	    		$price_whole_stay = $reservation_whole_stay -> price;
	    	
	    		$rates_prices[$code] = array("name" => $name_array["value"], 
	    				                     "price_one_day" => $price_one_day * $quantity,
                                             "price_whole_stay" => $price_whole_stay * $quantity
	    		);
	    
	    		//print "DEV: Prices for " . $resource_id . " : <pre>"; print_r($rates_prices); print "</pre>";
	    	} 
    	} else {
    		 
    		$reservation_one_day -> Calculate();
    		
    		$reservation_whole_stay -> Calculate();
    		
	    	$rates_prices["0"] = array("name" => "Room only", 
	    				               "price_one_day" => $reservation_one_day -> price * $quantity,
                                       "price_whole_stay" => $reservation_whole_stay -> price * $quantity
	    	);
	    	
    	}
    	    	
    	return $rates_prices;
    }

    
    //Get price for given board for one day for resource with given number of adults
    public function get_board_price_for_resource($resource_id = null, $quantity = 1, $arrival = null, $departure = null, $adults = null, $rate = null) 
    {
    
    	
    }
    
    
}

