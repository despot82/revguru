<?php

require "search-helper.php";


class Search_Rest_Service
{		
	var $search_helper;
	
	
	public function Search_Rest_Service()
	{		
		$this -> search_helper = new Search_Helper();
		
		
	}
	
	/*
	 * Return all hotels that can accomodate specified groups of people
	 */
	public function get_all_hotels_that_can_accommodate($in_destination, $groups, $arriving_at, $departing_at)
	{			
		return $this -> search_helper -> get_hotels_that_can_accommodate($in_destination, $groups, $arriving_at, $departing_at);		
	    
	}
	
	
   
}





?>