<?php


	define("HOTEL", "hotel");
	define("CITY", "city");
	define("STATE", "state");
	define("REGION", "region");
	define("COUNTRY", "country");
	define("INFINITE_NUMBER_OF_PERSONS", 250);
	
	define("RESERVATION_STATUS_NOT_APPROVED", 1);
	define("RESERVATION_STATUS_APPROVED", 2);	
	define("RESERVATION_STATUS_PENDING", 3);
	define("RESERVATION_STATUS_CANCELLED", 3); //Same status as PENDING
	define("RESERVATION_STATUS_REJECTED", 4);
	define("RESERVATION_STATUS_TEMPORARY", 5);
	
	
	
	
?>