<?php


require_once (realpath(dirname(dirname(__FILE__))) . "/Interfaces/i-inbound-reservations.php");
require_once (realpath(dirname(__FILE__)) . "/er-inbound-resource-api.php");

require_once ABSPATH . "wp-content/plugins/SendGrid/vendor/autoload.php";
require_once ABSPATH . "wp-content/plugins/SendGrid/lib/SendGrid.php";

class ER_Inbound_Reservation_API implements I_Inbound_Reservations
{
	var $resource_api;
	
	public function __construct ()
	{
		$this -> resource_api = new Er_Inbound_Resource_API();
	}
	
	public function getReservation($id)
	{
		global $wpdb;
		$reservation = new Reservation($id);
		$reservation -> Calculate();
		return $reservation;
		
	}
	
	/* 
	 * @Makes reservation for the period: (given_arrival_date, given_arrival_date + interval)
	 */
	public function make_reservation_for_date( $information ) {	
		
		return $this -> make_reservation_for_period ( $information );		
		
	}
		
	
	public function edit_reservation($information) 
	{			
		
		$reservation = new Reservation($information["id"]);
		$reservation -> admin = 0;
		
		$toEdit = array();
		
		if (isset($information["resource"] )) {
			$reservation -> resource = $information["resource"];
			$toEdit[] = "resource";
		}		
		
		if (isset($information["resourcenumber"] )) {
			$reservation -> resourcenumber = $information["resourcenumber"];
			$toEdit[] = "resourcenumber";
		}		
		
		if (isset($information["arrival"])) {
			$reservation -> arrival = strtotime(str_replace("_", " ", $information["arrival"]));
			$toEdit[] = "arrival";
		}		
		
		if (isset($information["departure"])) {
			$reservation -> departure = strtotime(str_replace("_", " ", $information["departure"]));
			$toEdit[] = "departure";
		}		
		
		if (isset($information["adults"])) {
			$reservation -> adults = $information["adults"];
			$toEdit[] = "adults";
		}		
		
		if (isset($information["childs"])) {
			$reservation -> childs = $information["childs"];
			$toEdit[] = "childs";
		}
		
		if (isset($information["board_type"])) {
			$toEdit[] = "custom";
			$toEdit[] = "prices";
		}
		
		if (isset($information["name"])) {
			$reservation -> name = $information["name"];
			$toEdit[] = "name";
		}
		
		if (isset($information["email"])) {
			$reservation -> email = $information["email"];
			$toEdit[] = "email";
		}
		
		if (isset($information["country"])) {
			$reservation -> country = $information["country"];
			$toEdit[] = "country";
		}
		
		$toEdit[] = "pricepaid";
		$fixed_price = explode(";", $reservation -> pricepaid)[0]; 
		$paid = explode(";", $reservation -> pricepaid)[1];
		
		if (isset($information["price"])) {
			$fixed_price = $information["price"];					
		}
		
		if (isset($information["paid"])) {
			$paid = $information["paid"];				
		}
		
		$reservation -> pricepaid = $fixed_price . ";" . $paid;
		
		$validates = $reservation -> Validate('send', 0);
		
		if ($validates) {		    
			
			foreach ($validates as $v) {
				if(substr($v, 0, 9) != "easy-form") {
					$reason .= $v."; ";
				}
			}
			return array("success" => 0, "error_msg" => "Resource not available due to filters and/or requirements. Reason(s): ".$reason);
		}		
		
		$customs = $reservation -> getCustoms(array(), "cstm");
		//print "DEV: customs = getCustoms() is: <pre>"; print_r($customs); print "</pre>";
		//Add the board custom field to the reservation
		$customs[] = array("type" => "cstm", "mode" => "edit", "id" => $this -> resource_api -> rates_custom_field_id, "value" => $information["board_type"]);
		
		//print "DEV: extended customs is: <pre>"; print_r($customs); print "</pre>";
		
		//Override existing custom prices with expanded one (expanded with board rate above)
		$reservation -> Customs($customs, 1, $this -> resource_api -> rates_custom_field_id, 1, "cstm", null, null);
		
		$reservation -> Calculate();
		
		//print "DEV: reservation is: <pre>"; print_r($reservation); print "</pre>";
		
		$reservation -> editReservation($toEdit);
		
		$reservation -> destroy();
		
		return array("success" => 1, "msg" => "Reservation was edited successfuly.");
			
	}
	
	//Approve the reservation.
	public function approve_reservation ($id) {
		try 
		{
		    $theReservation = new Reservation($id, array('status' => 'yes', 'resource' => false));
		    $theReservation -> editReservation(array('status'), false);
		    $theReservation -> destroy();
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		return array("success" => 1, "msg" => "Reservation was approved successfuly.");
	
	}
	
	//Reject the reservation.
	public function reject_reservation ($id) {
		try 
		{
		    $theReservation = new Reservation($id, array('status' => 'no', 'resource' => false));
		    $theReservation -> editReservation(array('status'), false);
		    $theReservation -> destroy(); 
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		
		return array("success" => 1, "msg" => "Reservation was rejected successfuly.");
	
	}
	
	//Trash the reservation. Later it can still be reactivated and returned to other statuses
	public function cancel_reservation ($id) {
		
		try 
		{
			$theReservation = new Reservation($id, array('status' => 'del', 'resource' => false));
		    $theReservation -> editReservation(array('status'), false);
		    $theReservation -> destroy();
	    } catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		
		return array("success" => 1, "msg" => "Reservation was cancelled successfuly.");
	
	}
	
	//Wipe the reservation from DB permanently. The evidence of reservation will not exist afterwards.
	public function delete_reservation ($id) {
		
		//echo "Deleting reservation $id";
	    try 
	    {
			$theReservation = new Reservation($id);
			$theReservation -> deleteReservation();
		    $theReservation -> destroy();
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		
		return array("success" => 1, "msg" => "Reservation was deleted successfuly.");
	
	}	
	
	//Set reservation with failed payment status
	public function set_reservation_with_failed_payment_status ($id) {
		
		try 
		{
			$theReservation = new Reservation($id, array('status' => 'fpa', 'resource' => false));
		    $theReservation -> editReservation(array('status'), false);
		    $theReservation -> destroy();
	    } catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		return array("success" => 1, "msg" => "Reservation was cancelled successfuly.");
	
	}
	
	/**
	 * @param  $information
	 * Array
	 *  (
	 *      "resource" => Integer
	 *      "resourcenumber" => Integer
	 *      "arrival" => yyyy-mm-dd_hh:ii:ss
	 *      "departure" => yyyy-mm-dd_hh:ii:ss
	 *      "adults" => Integer
	 *      "childs" => Integer
	 *      "status" => String (one of "yes, "no")
	 *      "price" => Integer
	 *      "paid" => Integer
	 *      "name" => String
	 *      "email" => String (xy@xy.xy)
	 *      "country" => String
	 *      "interval" => Integer
	 *      "reservated" => Datetime
	 *  )
	 **/
	public function make_reservation_for_period($information)
	{
		/*
			print "information parameter for make_reservation_for_period is: <pre>";
			print_r($information);
			print "</pre>";
		*/
	
		$resource_id = $information["resource"];
	
		//Check the validity of the reservation
		$availability = $this -> resource_api -> get_availability_for_resource_in_period($resource_id,
				$information["arrival"],
				$information["departure"],
				empty($information["adults"]) ? 1 : $information["adults"],
				$information["childs"]);
	
	
		// Check if resource type is available in the period for this group of people
		if ($availability["is_available"] != 1) {
			return array("success" => 0, "msg" => $availability["error_msg"]  );
		}
		
		//Check if the specific resource instance (resourcenumber) is available
		if (!empty($information["resourcenumber"]) && !in_array($information["resourcenumber"], $availability["available_room_numbers"])) {
			return array("success" => 0, "msg" => "The resource number " . $information["resourcenumber"] . " in room " . $resource_id . " is occupied at this period in time or doesn't exist.");
				
		}
	
		//Check that arrival is set
		$interval = easyreservations_get_interval ( 0, $resource_id, 0 ) ;
	
		if (!isset($information["arrival"]) || empty($information["arrival"]) || $information["arrival"] == 0 ) {
			return array("success" => 0, "msg" => "Arrival date is not set.");
		}
	
		$arrival_epoch = strtotime(str_replace("_", " ", $information["arrival"]));
	
		//Check the departure is set, if not, set it to be arrival + interval
		if (!isset($information["departure"]) || empty($information["departure"]) || $information["departure"] == 0 ) {
			$departure_epoch = $arrival_epoch + $interval;
			$departure = date("Y-m-d H:i:s", $departure_epoch);
		} else {
			$departure_epoch = strtotime(str_replace("_", " ", $information["departure"]));
		}
			
			
		$prep = array("resource" => $resource_id,
		   	"resourcenumber" => ($information["resourcenumber"] != 0 ? $information["resourcenumber"] : null),
			"arrival" => $arrival_epoch,
			"departure" => $departure_epoch,
			"adults" => empty($information["adults"]) ? 1 : $information["adults"] ,
			"childs" => empty($information["childs"]) ? 0 : $information["childs"] ,
			"price" => empty($information["price"]) ? 0 : $information["price"],
			"paid" => empty($information["paid"]) ? 0 : $information["paid"],
			"name" => empty($information["name"]) ? "Default reservation name" : $information["name"],
			"email" => empty($information["email"]) ? "default@email.com" : $information["email"],
			"country" => $information["country"],
			"status" => $information["status"],
			"reservated" => date("Y-m-d H:i:s"),
			"user" => $information["user"],
			"admin" => 0,
		);
	
		// print "prep parameter for make_reservation_for_period is: <pre>"; print_r($information); print "</pre>";
	
		//If resource is per-object and roomnumber is not given, calculate all available rooms and
		//choose the first available room for reservation
		$roomcount = get_post_meta($resource_id, 'roomcount', true);
		$r = new Resource($prep["resource"]);
		if (!is_array($roomcount) && (is_null($prep["resourcenumber"]))) {
			$vacants = $availability["available_room_numbers"];
			$prep["resourcenumber"] =  $vacants[0];
	
		}
	
		$reservation = new Reservation(false, $prep, 1);
		
		$new_custom = array();		
		$new_custom[] = array("type" => "cstm", "mode" => "edit", "id" => $this -> resource_api -> rates_custom_field_id, "value" => $information["board_type"]);
		
		//Add the custom price (rate addition to the price)
		$reservation -> Customs($new_custom, 1, null, 1, "cstm", null, null);
		
		if($reservation -> price == 0) {
			$reservation -> Calculate();
		}
	
		//Form the pricepaid field for database
		//$reservation -> pricepaid = $reservation -> price . ";" . $reservation -> paid;
	
		$result = $reservation -> addReservation();
	
		if(!$result) {
			//echo "<br>SUCCESS!";
			return array("success" => 1, "msg" => "Reservation was created successfuly, id = " . $reservation ->id);
				
		} else {
			//echo "<br>FAILED!";
			return array("success" => 0, "msg" => "Error occured and the reservation was not properly created.");
				
		}
			
	}
	
	
    /**
     * @param unknown $rooms_to_reservate
     * array
	 * (
	 *     (resource_id_1 => qty_1),
	 *     (resource_id_2 => qty_2),
	 *     (resource_id_3 => qty_3),
	 *     ...
	 *     (resource_id_n => qty_n)
	 * )
     * @param unknown $arrival
     * @param unknown $departure
     * @param string $name
     * @param string $email
     * @param string $country
     * @param number $braintree_payment_option
     * 
     * @return number 0 for failure, id of the created grouped reservation otherwise 
     */
    public function make_grouped_reservation($rooms_to_reservate,
			$arrival,
			$departure,
			$name = "Default client name",
			$email = "default@email.com",
			$country = "XY",
    		$braintree_payment_option = 3,
    		$rates_custom_field_id)
	{
	
		//print "DEV: <pre>"; print_r($rooms_to_reservate); print "</pre>";
	
		$created_reservations = array();
	
		$arrival_epoch = strtotime(str_replace("_", " ", $arrival));
		$departure_epoch = strtotime(str_replace("_", " ", $departure));
	
		$total_sum = 0;
	
		foreach ($rooms_to_reservate as $resource_id => $personsRate_boardRate_qty) {
	
			//Find vacant rooms' roomnumbers. There should be more vacant rooms than sum of all $qty - s
			$vacant_rooms = $this -> resource_api -> get_instances_without_reservations_in_period($resource_id, $arrival, $departure);
			
			
			//Go through each selected number of persons
			$next_vacant_roomnumber = 0;
			foreach($personsRate_boardRate_qty as $persons => $boardRate_qty) {
				 
				foreach($boardRate_qty as $board_code => $qty) {
	
					for($i = 0; $i < $qty; $i++) {
	
						$prep = array("resource" => $resource_id,
								"resourcenumber" => $vacant_rooms[$next_vacant_roomnumber],
								"arrival" => $arrival_epoch,
								"departure" => $departure_epoch,
								"adults" => $persons,
								"childs" => 0,
								"status" => "tmp",
								"name" => $name,
								"email" => $email,
								"country" => $country,
								"reservated" => date("Y-m-d H:i:s"),
								"admin" => 0,
								"fake" => 1
						);
							
						$reservation = new Reservation(false, $prep, 1);
	
						$new_custom = array();
						$new_custom[] = array("type" => "cstm", "mode" => "edit", "id" => $rates_custom_field_id, "value" => $board_code );
						//print "DEV: code and name array are:<pre>"; print_r($code); print_r($name_array); print "</pre>";
						//Add the custom price (rate addition to the price)
						$reservation -> Customs($new_custom, 1, null, 1, "cstm", null, null);
	
						if($reservation -> price == 0) {
							$reservation -> Calculate();
						}
	
						//Form the pricepaid field for database
						$reservation -> pricepaid = ";" . $reservation -> paid;
	
						$adding_result = $reservation -> addReservation();
	
						if(!$adding_result) {
							//echo "DEV: <br>Newly added reservation's id is: " . $reservation -> id;
							$created_reservations[] = $reservation -> id;
							$total_sum += $reservation -> price;
							$next_vacant_roomnumber ++ ;
								
						} else {
								
							echo "<br>There was an error saving a reservation.";
								
							//print "DEV: adding_result is: <pre>"; print_r($adding_result); print "</pre>";
							//print "DEV: created_reservations<pre>"; print_r($created_reservations); print "</pre>";
								
							foreach ($created_reservations as $cr) {
								$this -> delete_reservation($cr);
							}
								
							return array("success" => 0);
								
						}
					} //END for($i = 0; $i < $qty; $i++)
				} //END foreach($rate_qty as $board_code => $qty)
			}
		}
	
		$grouped_reservation_content = $created_reservations[0];
	
		for ($i = 1; $i < count($created_reservations); $i++) {
			$grouped_reservation_content .= "," . $created_reservations[$i];
		}
	
		$grouped_reservation_content .= ";" . $braintree_payment_option;
	
		$post = array(
				'post_content'   => $grouped_reservation_content, // The full text of the post.
				'post_name'      => 'new_gr', // The name (slug) for your post
				'post_title'     => 'new_gr_title', // The title of your post.
				'post_status'    => 'private', // Default 'draft'.
				'post_type'      => 'groupedreservation', // Default 'post'.
				'post_author'    => 666, // The user ID number of the author. Default is the current user ID.
				'ping_status'    => 'default_ping_status', // Pingbacks or trackbacks allowed. Default is the option 'default_ping_status'.
				'post_parent'    => 0, // Sets the parent of the new post, if any. Default 0.
				'menu_order'     => 0, // If new post is a page, sets the order in which it should appear in supported menus. Default 0.
				'to_ping'        => '',// Space or carriage return-separated list of URLs to ping. Default empty string.
				'pinged'         => '', // Space or carriage return-separated list of URLs that have been pinged. Default empty string.
				'post_password'  => '', // Password for post, if any. Default empty string.
				//'guid'           =>  Skip this and let Wordpress handle it, usually.
				//'post_content_filtered' =>  Skip this and let Wordpress handle it, usually.
				'post_excerpt'   => 'Container for individual room reservations', // For all your post excerpt needs.
				//'post_date'      => [ Y-m-d H:i:s ] // The time post was made.
				//'post_date_gmt'  => [ Y-m-d H:i:s ] // The time post was made, in GMT.
				'comment_status' => 'closed' // Default is the option 'default_comment_status', or 'closed'.
				//'post_category'  => [ array(<category id>, ...) ] // Default empty.
				//'tags_input'     => [ '<tag>, <tag>, ...' | array ] // Default empty.
				//'tax_input'      => [ array( <taxonomy> => <array | string>, <taxonomy_other> => <array | string> ) ] // For custom taxonomies. Default empty.
				//'page_template'  => [ <string> ] // Requires name of template file, eg template.php. Default empty.
		);
	
		$result = wp_insert_post( $post, $wp_error );
	
		return array("success" => 1,
				     "created_reservations" => $created_reservations,
				     "total_sum" => $total_sum,
				     "grouped_reservation_post_id" => $result
		);
	
	}
	
	/**
	 * Approves all reservations grouped under the same grouped reservation
	 * 
	 * @param Number $grouped_reservation_id
	 */
	public function approve_grouped_reservations($grouped_reservation_id)
	{	
	    //echo "DEV: CHECKPOINT in approve_grouped_reservations<br>";
		$post = get_post($grouped_reservation_id);
		$reservations = explode(";", $post -> post_content);
		$reservations_ids = explode(",", $reservations[0]);
		
		foreach($reservations_ids as $reservation_id) {
			 $this -> approve_reservation($reservation_id);
		}
		
		//echo "DEV: CHECKPOINT after approve_reservation for each reservation<br>";
		
		$sendgrid = new SendGrid("emitto", "Sendgrid31415");
		$email    = new SendGrid\Email();
		
		//echo "DEV: CHECKPOINT after sendgrid init<br>";
		
		$first_reservation = new Reservation($reservations_ids[0]);
		//echo "DEV: first_reservation -> email is: " . $first_reservation -> email . "<br>";
		
		try {
		$email -> addTo(get_option("reservations_support_mail"))
		       -> addTo("reservations@revguru.com")
			   -> addTo($first_reservation -> email)
		       -> setFrom("administrator@revguru.com")
		       -> setSubject("Reservation notification")
		       -> setHtml("Grouped reservation " . $grouped_reservation_id . " in hotel " . get_bloginfo("name") . " and its associated reservations have been approved.");
		} catch (Exception $e) {
		    print "DEV: exception:<pre>"; print_r($e); print "</pre>";	
		}
		
		//echo "DEV: CHECKPOINT after sendgrid email sending<br>";
		
		//print "DEV: sendgrid is:<pre>"; print_r($sendgrid); print "</pre>";
		//print "DEV: email is:<pre>"; print_r($email); print "</pre>";
		
		//Send the confirmation notification email
		$email_sending_result = $sendgrid -> send($email);
		
		//print "DEV: email_sending_result is: <pre>"; print_r($email_sending_result); print "</pre>";
		
	}
	
	/**
	 * Updates price paid for all reservations grouped under the same grouped reservation
	 * 
	 * @param Number $grouped_reservation_id
	 */
	public function set_grouped_reservations_as_paid($grouped_reservation_id, $braintree_payment_option, $fully_paid = 1)
	{
		$post = get_post($grouped_reservation_id);
		$reservations = explode(";", $post -> post_content);
		$reservations_ids = explode(",", $reservations[0]);
		
		foreach($reservations_ids as $reservation_id) {
			$r = new Reservation($reservation_id);
			$r -> Calculate();
			
			//print "DEV: reservation to update paid for is: <pre>"; print_r($r); print "</pre>";
			
			switch($braintree_payment_option) {
				case 1: $this -> edit_reservation(array("id" => $reservation_id, "paid" => 0.1 * $r -> price  )); break;
				case 2: $this -> edit_reservation(array("id" => $reservation_id, "paid" => $r -> price  )); break;
				case 3: $this -> edit_reservation(array("id" => $reservation_id, "paid" => 0  )); break;
			}
		}
	}
	
	/**
	 * Update the grouped reservation and its reservations as having failed payment
	 *
	 * @param Number $grouped_reservation_id
	 */
	public function set_grouped_reservations_with_failed_payment_status($grouped_reservation_id)
	{
		$post = get_post($grouped_reservation_id);
		$reservations = explode(";", $post -> post_content);
		$reservations_ids = explode(",", $reservations[0]);
	
		foreach($reservations_ids as $reservation_id) {
			
			$this -> set_reservation_with_failed_payment_status($reservation_id);
			
		}
	}
	
	/**
	 * 
	 * Deletes all reservations grouped under the same grouped reservation
	 * 
	 * @param unknown $grouped_reservation_id
	 */
	public function delete_grouped_reservations($grouped_reservation_id)
	{
		$post = get_post($grouped_reservation_id);
		$reservations = explode(";", $post -> post_content);
		$reservations_ids = explode(",", $reservations[0]);
		
		foreach($reservations_ids as $reservation_id) {
			
			$this -> delete_reservation($reservation_id);
		}
	
	}
	
}
