<?php

require_once "constants.php";
require ABSPATH . "wp-content/plugins/easyreservations/rest/ServiceClasses/er-inbound-resource-api.php";

class Hotel_Search_Helper
{	
	var $hotel_id;
	var $resource_api;
	var $usable_rooms;
	var $optimal_room_set;
	var $smallest_room_set;
	var $cheapest_room_set;
	var $most_booked_room_set;
	
	
	
	public function Hotel_Search_Helper($hotel_id)
	{		
		global $wpdb;
		
		$this -> usable_rooms = null;
		
		$this -> hotel_id = $hotel_id;
		
		$this -> load_all_hotel_rooms();
		
		ini_set('max_execution_time', 60 * 10);
		
		$this -> resource_api = new ER_Inbound_Resource_API();
		
	}
	
	public function load_all_hotel_rooms()
	{		
		easyreservations_load_resources();		
	}
	
	/**
	 * 
	 * Calculate and save the rooms that can accommodate at least the smallest group of people in the given 
	 * array of groups of people
	 * 
	 * @param $groups
	 * @param $arrival
	 * @param $departure
	 * @param $hotel_id
	 * 
	 * @return string
	 */
	public function calculate_relevant_hotel_rooms_for_criteria($groups, $arrival, $departure, $hotel_id)
	{
		global $wpdb;
		global $the_rooms_array;
		
		$people = array();
	
		foreach ($groups as $group) {
			$p["adults"] = explode(",", $group)[0];
			$p["childs"] = explode(",", $group)[1];
				
			$people[] = $p;
				
		}
	
		//Find minimum group
		$min = $people[0]["adults"] + $people[0]["childs"];
		$key_min = 0;
	
		foreach($people as $key => $g) {
				
			if ($min > $g["adults"] + $g["childs"]) {
				$min = $g["adults"] + $g["childs"];
				$key_min = $key;
			}
		}
		
		foreach ($the_rooms_array as $id => $room) {
			
			/* print "DEV: room " . $id . ":<pre>"; print_r($room); print "</pre>"; */	
	
			$availability = $this -> resource_api -> get_availability_for_resource_in_period($id,
					$arrival,
					$departure,
					$people[$key_min]["adults"],
					//1,
					$people[$key_min]["childs"],
					//0,
					false );
			
			
			//print "DEV: availability for room " . $id . "<pre>";			print_r($availability);			print "</pre>";
			
				
			if ($availability["is_available"]) { //If $room can accommodate at least the smallest group
				
				$resource = $this -> resource_api -> getResource($id);
				
				//If maximum occupancy is 0, this is actually infinite
				$room_max_occupancy = (!isset($resource -> req["pers-max"]) || ($resource -> req["pers-max"] == 0)) ? INFINITE_NUMBER_OF_PERSONS : $resource -> req["pers-max"]; 
				
				$this -> usable_rooms[$id] = array("resource_id" => $id,
						                           "groundprice" => $resource -> groundprice,
						                           "price" => $availability["price"],
						                           "available_quantity" => $availability["number_of_available_rooms"],						                        
						                           "max_occupancy" => $room_max_occupancy);
				
	
			} else {
	
			}
		}	
		
		/*
		print "DEV: this usable rooms are:<pre>";
		print_r($this -> usable_rooms);
		print "</pre>";
		*/
		
	}
	
	//Fetch the cslculated relevant rooms
	public function get_usable_rooms()
	{
		return $this -> usable_rooms;
	}

    /*
	 * Get optimum set of rooms that can accommodate the need.
	 * Take the smallest room first and fit all groups in the minimum possible number of smallest rooms.
	 * Return the room set
	 * 
	 */
	public function get_smallest_room_set_for_criteria($hotel_id, $groups, $arrival, $departure)
	{
		//$exploded = array();
		foreach($groups as $key => $group) {
			 
		    $groups[$key] = explode(",", $group)[0] + explode(",", $group)[1];
		}		
		
		return  $this -> allocate_groups_to_smallest_rooms($groups, $this -> get_usable_rooms());
		
	}
	
	/*
	 * Get most-booked set of rooms that can accommodate the need.	 
	 * Return the room set
	 *
	 */
	public function get_most_booked_room_set_for_criteria($hotel_id, $adults, $childs, $arrival, $departure)
	{
		
	
	}
	
	/*
	 * Get cheapest set of rooms that can accommodate the need (group(s) of people in period of time).
	 * Take the cheapest room first and fit all groups in next cheapest room until all groups are accommodated. 
	 * Return the room set
	 * 
	 */
	public function get_cheapest_room_set_for_criteria($hotel_id, $adults, $childs, $arrival, $departure)
	{
		$plugins_dir = ABSPATH . 'wp-content/plugins/';
	
	}    
	
	
    /*
	 * @param $groups - array of numbers from 1 - 100
	 * @param $rooms - array(resource_id,
	 *					     groundprice,
	 *					     price,
	 *					     available_quantity,						                        
	 *					     max_occupancy)
	 * 
	 * @return list of smallest set of rooms that can accommodate the need, or null if the hotel can't accommodate the groups
	 */
    private function allocate_groups_to_smallest_rooms($groups, $rooms)
	{	
		
		
		//echo "CHECKPOINT - ALLOCATING GROUPS TO ROOMS: *****************************************************<br/>";		
				
		sort($groups);
		
		//Sort the filtered available rooms by maximum occupancy in ascending order			
	    $hotel_id = $this -> hotel_id;	
	 
		$max_occupancies = array();
		
		// Prepare the column to sort by
		foreach ($rooms as $key => $room) {
			$max_occupancies[$key]  = empty($room["max_occupancy"]) ? 1 : $room["max_occupancy"] ;
			
		}
		
		// Sort the hotel_rooms data by maxOccupancies ASC, using PHP built-in sorting function
		array_multisort($max_occupancies, SORT_ASC, $rooms);
		
		//See which is the room set that can fit all groups
		$fitting_room_set = array();	
		
		$hotel_id = $this -> hotel_id;
		$sorted_hotel_rooms = $rooms;						
		
		//print "DEV: <br/>Trying to fit groups in hotel " . $hotel_id;
	    $hotel_can_fit_all_groups = true;
	    $smallest_not_filled_room = 0;
			
	    $rooms_ready = array();
	    foreach ($groups as $group) {
				
	        //print "DEV: <br>Trying to accommodate group of " . $group . " people.";
        	$accommodated = false;
            //Try to put the group into smallest available room that is not already occupied by previous groups
			for($i = $smallest_not_filled_room; $i < count($sorted_hotel_rooms); $i++)
			{	
				$rid = $sorted_hotel_rooms[$i]["resource_id"];
			    //print "DEV: <br>See if the above group can be settled in room " . $sorted_hotel_rooms[$i]["resource_id"] . " that takes up to " . $sorted_hotel_rooms[$i]["max_occupancy"] . " people.";
		    	if($sorted_hotel_rooms[$i]["max_occupancy"] >= $group ) {
				    	
		    	    //print "DEV: <br>It can accommodate it.";
		    		$accommodated = true;
		    			
		    		if($rooms_ready[$rid]["counted"] == 0) {
		    			
		    			$rooms_ready[$rid] = array("group" => $group,
		    					                   "resource_id" => $rid,
		    					                   "available_quantity" => $rooms[$i]["available_quantity"],
		    					                   "groundprice" => $sorted_hotel_rooms[$i]["groundprice"],
		    					                   "price" => $sorted_hotel_rooms[$i]["price"],
		    					                   "max_occupancy" => $sorted_hotel_rooms[$i]["max_occupancy"]);
		    			
		    		} else {
		    			
		    		}
		    		
		    		$rooms_ready[$rid]["counted"] ++ ;
					    			
		    		$sorted_hotel_rooms[$i]["available_quantity"]--;
				    	
		    	    //print "DEV: <br>Hotel " . $hotel_id . " can accommodate group of " . $group . " people by putting them in resource_id: " . $sorted_hotel_rooms[$i]["resource_id"];
		    	    //print "DEV: ...leaving " . $sorted_hotel_rooms[$i]["available_quantity"] . " of these rooms behind.";
				    	
		    		if ($sorted_hotel_rooms[$i]["available_quantity"] < 1) {
		    	    	$smallest_not_filled_room ++ ;		
		    		}	
		    		break;			    	
		    	} else { // (The smallest-available-not-already-used-in-check room is too small for this group)
		    	    //print "DEV: <br>It cannot accommodate it.";
		    	    $smallest_not_filled_room++;
		    	}				
			}
				
			if(!$accommodated) {
				$hotel_can_fit_all_groups = false;
				//print "DEV: <br>Hotel " . $hotelId . " can't accommodate group of " . $group . " people.";
				return array("can_accommodate" => 0);
				
			}
	    }
			
	    if($hotel_can_fit_all_groups) {
		    //FULL OR ID-S ONLY RESULTS DECISION IS MADE HERE
		    $fitting_room_set = $rooms_ready;
		    $fitting_room_set["can_accommodate"] = 1;
		    //$hotels_that_can_fit[] = $hotel_id ;
	    } 
	    //END FOREACH HOTEL
		
		return $fitting_room_set;
	}

}

?>
