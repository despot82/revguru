<?php

require_once "constants.php";

class Search_Helper
{
	//Rooms respresented as their max-occupancy
	var $rooms;
	var $hotels;
	
	
	public function Search_Helper()
	{
		
		ini_set('max_execution_time', 60 * 10);
		
	}
	
	public function setup_rooms($rooms)
	{
		$this -> rooms = $rooms;
		
	}
	
	//Top level function - returning all hotels that can accommodate groups from the criteria
	public function get_hotels_that_can_accommodate($in_destination, $groups, $arriving_at, $departing_at)
	{			
		//1.Find the smallest group in order to cut off rooms that can't accommodate it
		//2.Find biggest room in case we have a quick solution for allocation(which is to allocate all groups with rooms that are bigger or equal than 
		//the largest group) 
		$totals = array();
		foreach ($groups as $group) {
			$total = $group["adults"] + $group["childs"];
			$totals[] = $total;
			if ($total < 1) {
				//echo "ERROR: One of the groups has 0 or less people.";
				return null;
			}			
			
		}
		
		//error_log("POINT 1");
		
		$smallest_group = min($totals);		
		$biggest_group = max($totals);
		
		//Find vacant rooms ( rooms that have no reservations that overlap the specified period)
		$unreservated_rooms = $this -> get_unreservated_per_object_rooms($arriving_at, $departing_at, $in_destination);
		
		//error_log("POINT 2");
		
		//Return vacant rooms further filtered by er filters and requirements 
		$available_rooms = $this -> filter_rooms_by_unavail_filters_and_reqs($unreservated_rooms, $arriving_at, $departing_at, $smallest_group, $biggest_group );
		
		//error_log("POINT 3");
		
		//Try to pack the groups into available rooms and return the hotels that can do this
		$hotels_that_can_accommodate = $this -> allocate_groups_to_rooms($totals, $available_rooms);
		
		//error_log("POINT 4");
		
		return $hotels_that_can_accommodate;
		
	}
	
	
	
	
	private function get_unreservated_per_object_rooms($arriving_at, $departing_at, $in_destination)
	{ 
		global $wpdb;
		
		if ($in_destination["destination_type"] == HOTEL) {
			$destination_condition_sql =  " AND hotels.blog_id = " . $in_destination["destination_id"] . " ";
			$destination_join_sql = " JOIN wp_blogs hotels ON(hotels.blog_id = room.hotelId) ";
			
		} else 
		if ($in_destination["destination_type"] == CITY) {
			$destination_condition_sql =  " AND city.cityId = " . $in_destination["destination_id"] . " ";
			$destination_join_sql = " JOIN wp_blogs hotels ON(hotels.blog_id = room.hotelId) "
					            . " JOIN city ON(hotels.cityId = city.cityId) ";
				
		} else 
		if ($in_destination["destination_type"] == REGION) {
			//?
			//For the time-being, there is nothing to connect to region, we don't have the data for cities to connect to regions table
			$destination_condition_sql =  "";
			$destination_join_sql = "";
			
		} else 
		if ($in_destination["destination_type"] == STATE) {
			$destination_condition_sql =  " AND state.stateId = " . $in_destination["destination_id"] . " ";
			$destination_join_sql = " JOIN wp_blogs hotels ON(hotels.blog_id = room.hotelId) "
					              . " JOIN city ON(hotels.cityId = city.cityId) "
					              . " JOIN state ON(city.stateId = state.stateId) ";
				
		} else 
		if ($in_destination["destination_type"] == COUNTRY) {
			$destination_condition_sql =  " AND country.countryId = " . $in_destination["destination_id"] . " ";
			$destination_join_sql = " JOIN wp_blogs hotel ON(hotel.blog_id = room.hotelId) "
					              . " JOIN city ON(hotel.cityId = city.cityId) "
					              . " JOIN state ON(city.stateId = state.stateId) "
					              . " JOIN country ON(state.countryId = country.countryId) ";
				
		}
		
		
		$sql = "SELECT room.hotelId, room.ID as room_resource_id, groundprice.meta_value as groundprice, roomcount.meta_value-count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, bill_interval.meta_value as intervall,  filters.meta_value as filters "
		     . "FROM all_posts as room " 
		     . "JOIN all_postmeta as roomcount on(room.hotelId = roomcount.hotelId "
		     . "                                  AND room.ID = roomcount.post_id "
		     . "                                  AND roomcount.meta_key = 'roomcount' "
			 . "	                              AND meta_value REGEXP '^-?[0-9]+$') " 
		     . "LEFT JOIN all_postmeta as requirements on(room.hotelId = requirements.hotelId "
			 . "	                                      AND room.ID = requirements.post_id "
			 . "	                                      AND requirements.meta_key = 'easy-resource-req') " 
			 		
			 		
			 . "LEFT JOIN all_postmeta as bill_interval on(room.hotelId = bill_interval.hotelId "
			 . "	                                       AND room.ID = bill_interval.post_id "
			 . "	                                       AND bill_interval.meta_key = 'easy-resource-interval') "

			 . "LEFT JOIN all_postmeta as groundprice on(room.hotelId = groundprice.hotelId " 	                                       
             . "                                         AND room.ID = groundprice.post_id "  	                                       
             . "     		                             AND groundprice.meta_key = 'reservations_groundprice') " 		
			 					 		
			 		
		     . "LEFT JOIN all_postmeta as filters on(room.hotelId = filters.hotelId "
			 . "                                     AND room.ID = filters.post_id "
		     . "               		                 AND filters.meta_key = 'easy_res_filter') "
			 
			 
		     . "LEFT JOIN all_reservations reservations on(room.hotelId = reservations.hotelId "
		     . "		                                   AND reservations.room = room.id "
			 . "	                                       AND NOT((reservations.arrival < '" . $arriving_at . "' AND reservations.departure < '" . $arriving_at . "') OR "
			 . "	                                               (reservations.arrival > '" . $departing_at . "' AND reservations.departure > '" . $departing_at . "') )) "
			 . $destination_join_sql
	         . "WHERE post_type = 'easy-rooms' "
		     . $destination_condition_sql
		     . "GROUP BY hotelId, room_resource_id "
		     . "HAVING available>0 "; 
		
		$link = new mysqli("localhost", "root", "", "wp");
		
		//error_log ("sql is: " . $sql);
		
		$results = $link -> query($sql);
		
		/*
		$wp -> - style - code requires an admin login		
		
        $user = get_user_by( 'id', 1 ); 
        if( $user ) {
    		wp_set_current_user( 1, $user -> user_login );
    		wp_set_auth_cookie( $user_id );
    		do_action( 'wp_login', $user->user_login );
		}
		$results = $wpdb -> get_results($sql);
		print ("results by wpdb get results function: <br/>" );				
		print "<pre>";
		print_r($wpdb -> get_results($sql));
		print "</pre>";
		
        
        */			
		
		
		return $results;
	}
	
	//Further filter the rooms by minimum required acceptancy filter and possibly unavailability filters
	private function filter_rooms_by_unavail_filters_and_reqs($unreservated_rooms, $arriving_at, $departing_at, $smallest_group)
	{
	    /*
		 * 1.Check unavailability filters
		 * 2.Check minimum and maximum stay requirements
		 * 3.Check that max-persons requirement of the room is bigger than the minimum number of persons
		 * 4.If passes all above, insert it into array for allocation of groups like so:
		 *   - [hotelId] => [resourceId => HERE, price => HERE, availability => HERE, min_persons => HERE, max_persons => HERE]
		 *   - 
		 *   - 
		 *
		 */
		
		$hotels_filtered_rooms = array();
		
		$arrival = strtotime($arriving_at);
		$departure = strtotime($departing_at);
		
	    while ($room = $unreservated_rooms -> fetch_object()) { 
	    		    	
	    	$fine = true;
	    	
	    	//1.
	        $filters = unserialize($room -> filters);
	        
            if(!empty($filters)) {            	
                foreach($filters as $filter) {
                    if($filter["type"] == 'unavail') {
                    	
                    	if(  !(  ($departure < $filter["from"])  ||  ($arrival > $filter["to"])  )  ) {
                    		$fine = false; 
                    		//echo "<br/>-Discard room " . $room -> room_resource_id . " in hotel " . $room -> hotelId . " because of unavail filter";
                    		continue;
                    	}
                    }    
                }
            }
            
            //2.
            $room_requirements = unserialize($room -> requirements);
            
            //Set minimum to 1 and maximum to 250 if they are empty, this is a minor bug in ER, these reqs per default(when resource is made) 
            //don't have any value.
            //print "<br>ROOM REQUIREMENTS FOR ROOM " . $room -> room_resource_id . " IN HOTEL " . $room -> hotelId . ": <pre>";            print_r (unserialize($room -> requirements));            print "</pre>";
            
            
            if ($room_requirements["pers-max"] == 0) $room_requirements["pers-max"] = INFINITE_NUMBER_OF_PERSONS;
            if ($room_requirements["pers-min"] == 0) $room_requirements["pers-min"] = 1;            
            if ($room_requirements["nights-min"] == 0) $room_requirements["nights-min"] = 1;
            if ($room_requirements["nights-max"] == 0) $room_requirements["nights-max"] = 30;            
            if(empty($room -> intervall) ) $room -> intervall = 86400;
            
            $stay = $departure - $arrival;
            $times = $stay / $room -> intervall;
            if(( $times > $room_requirements["nights-max"] ) || ($times < $room_requirements["nights-min"])) {
            	            	
            	$fine = false;
            	//echo "<br/>-Discard room " . $room -> room_resource_id . " in hotel " . $room -> hotelId . " because of nights max or min , times is: " . $times;
            	continue;
            }
            
            //3.
            if($smallest_group > $room_requirements["pers-max"] ) {
            	$fine = false;
            	//echo "<br/>-Discard room " . $room -> room_resource_id . " in hotel " . $room -> hotelId . " because smallest group to accomodate is bigger than max persons req of this room";
            	continue;
            }    

            //4.
            if($fine) {
                $hotels_filtered_rooms[$room -> hotelId][] = array("resource_id" => $room -> room_resource_id,
                		                                           "groundprice" => $room -> groundprice,
            	    	                                           "availability" => $room -> available,
            		                                               "min_persons" => $room_requirements["pers-min"], 
            		                                               "max_persons" => $room_requirements["pers-max"]
            		                                               );
            }
	        	
	    } //end foreach $unreservatedRoom

	    return $hotels_filtered_rooms;
	    
	}
	
	
	/*
	 * $groups - array of numbers from 1 - 100
	 * $rooms - array of hotels and rooms
	 * 
	 * @return list of hotels as final result
	 */
    private function allocate_groups_to_rooms($groups, $hotels_rooms)
	{				
		//echo "CHECKPOINT - ALLOCATING GROUPS TO ROOMS: *****************************************************<br/>";
		$final_hotels = array();
				
		sort($groups);		
		
		$sorted_hotels_rooms = array();
		
		//Sort the filtered available rooms by maximum occupancy in ascending order
		foreach($hotels_rooms as $hotel_id => $hotel_rooms) {
			
			$max_occupancies = array();
			
			// Prepare the column to sort by
			foreach ($hotel_rooms as $key => $room) {
				$max_occupancies[$key]  = empty($room["max_persons"]) ? 1 : $room["max_persons"] ;
				
			}
			
			// Sort the data by maxOccupancies ASC, using PHP's built-in sorting function
			array_multisort($max_occupancies, SORT_ASC, $hotel_rooms);
			
			//change the ordering of the rooms inside the hotel rooms array, if needed
			$sorted_hotels_rooms[$hotel_id] = $hotel_rooms;
			
		}
		
		
		//print "DEV: Sorted hotels rooms array:<br/><pre>";			print_r($sorted_hotels_rooms);			print "</pre>";
		
		//See which hotels can fit all groups
		$hotels_that_can_fit = array();
		
		foreach ($sorted_hotels_rooms as $hotel_id => $sorted_hotel_rooms) {
			//FOREACH HOTEL	
			//print "<br/>Trying to fit groupss in hotel " . $hotel_id . "<br/>";
			$hotel_can_fit_all_groups = true;
			$smallest_not_filled_room = 0;
			$rooms_ready = array();
			foreach ($groups as $group) {
				
				//print "Trying to accommodate group of " . $group . " people.<br/>";
                $accommodated = false;
                //Try to put the group into smallest available room that is not already occupied by previous groups
				for($i = $smallest_not_filled_room; $i < count($sorted_hotel_rooms); $i++)
				{	
					//print "See if the above group can be settled in room " . $sorted_hotel_rooms[$i]["resource_id"] . " that takes up to " . $sorted_hotel_rooms[$i]["max_persons"] . " people.<br/>";
				    if($sorted_hotel_rooms[$i]["max_persons"] >= $group ) {
				    	
				    	//print "It can accommodate it.<br/>";
				    	$accommodated = true;
				    	
				    	$rooms_ready[] = array("group" => $group, 				    			              
				    			               "resource_id" => $sorted_hotel_rooms[$i]["resource_id"],	
				    			               "groundprice" => $sorted_hotel_rooms[$i]["groundprice"],
				    			               "max_persons_req" => $sorted_hotel_rooms[$i]["max_persons"]);
				    			
				    	//print "Hotel " . $hotel_id . " can accommodate group of " . $group . " people by putting them in resource_id: " . $sorted_hotel_rooms[$smallest_not_filled_room]["resource_id"] . "<br>" ;
				    	
				    	$sorted_hotel_rooms[$i]["availability"] -- ;
				    	if ($sorted_hotel_rooms[$i]["availability"] < 1) {
				    	    $smallest_not_filled_room ++ ;		
				    	}	
				    	break;			    	
				    } else { // (The smallest-available-not-already-used-in-check room is too small for this group)
				    	//print "It cannot accommodate it.<br/>";
				    	$smallest_not_filled_room++;
				    }				
				}
				if(!$accommodated) {
					$hotel_can_fit_all_groups = false;
					//print "Hotel " . $hotelId . " can't accommodate group of " . $group . " people.<br>";
					break;
				}
			}
			
			if($hotel_can_fit_all_groups) {
				//FULL OR ID-S ONLY RESULTS DECISION IS MADE HERE
				$hotels_that_can_fit[] = array("hotel_id" => $hotel_id, "rooms" => $rooms_ready) ;
				//$hotels_that_can_fit[] = $hotel_id ;
			} 
			//END FOREACH HOTEL
		}		
		
	
		
		return $hotels_that_can_fit;
	}
	


}

?>


