<?php

require_once (ABSPATH . "wp-content/plugins/easyreservations/lib/classes/resource.class.php");

class ER_Inbound_Hotel_API
{
	
	public function ER_Inbound_Hotel_API()
	{
	    	
	}
	
	public function delete_custom_field($id)
	{
		$custom_fields = get_option('reservations_custom_fields');
		
		if (isset($custom_fields['fields'][$id])) {
			unset($custom_fields['fields'][$id]);
			update_option('reservations_custom_fields', $custom_fields);
			return 1;
			
		}
		
		return 0;
		
	}
	/*
	$data //array of format:
	(
		$custom_name,   // name of the custom field
		$custom_field_type,  // type of the custom field ("text", "select" ...)
		$custom_field_unused,  // value for field if unused
		$custom_field_value = null, // value if field is of simple type
		$values = array(),   //  values of each of the options
		$affects_price = 0,  // does field affect price or not
		$required = 0, // is the field required or not
		$ids = array(),    // id-s of the individual options of the custom field in form of internal_id => final_unique_id
		$prices = null,    // price values for each of the options
		$checkeds = null   // for radio button options
	)
	*/
	public function add_custom_field($data)
	{
		//Get the parameters into individual variables
		$custom_name = $data["name"];   // name of the custom field
		$custom_field_type = $data["type"];     // type of the custom field ("text", "select" ...)
		$custom_field_unused = $data["unused"];  // value for field if unused
		$custom_field_value = $data["value"];   // value if field is of simple type
		$values = explode(",", $data["values"]);   //  values of each of the options
		$affects_price = $data["affects_price"];    // does field affect price or not
		$required = $data["required"];   // is the field required or not
		$ids = explode(",", $data["ids"]);    // id-s of the individual options of the custom field in form of internal_id => final_unique_id
		$prices = explode(",", $data["prices"]);    // price values for each of the options
		$checkeds = $data["checkeds"];   // for radio button options
		
		
		$custom_fields = get_option('reservations_custom_fields');
		
		if(isset($custom_name)){
			
			$custom = array();
			$custom["title"] = $custom_name;
			$custom["type"] = $custom_field_type;
			$custom["unused"] = $custom_field_unused;
			
			if($custom["type"] == 'text' || 
			   $custom["type"] == 'area'){  //If this is a simple custom field type (text box)
				$custom["value"] = $custom_field_value;
			} else { //This is a complex custom field type
				$custom['options'] = array();
				
				foreach($ids as $nr => $id){ 
				// $nr is internal index of the option parameter that is POST-ed, used to connect the option's parameters - value, price, checked
				// $id is final id of the option parameter
				  	
					//$final_id will be formed from $id as unique
					$final_id = $id;
					if(is_numeric($id)){
						$uid = uniqid($id);
						$final_id = $uid;
					}
					
					$custom['options'][$final_id] = array();
					
					//Set the name of this custom field option
					$custom['options'][$final_id]["value"] = $values[$nr];
					
					// If price parameter is set, it holds the price array for each option
					if(isset($prices)) $custom['options'][$final_id]["price"] = $prices[$nr];
					
					//Set the checked state (applies to checkable fields)
					if(isset($checkeds[$nr]) && $checkeds[$nr] == 1) $custom['options'][$final_id]['checked'] = 1;
				}
		
				
			}
			
			//Set whether the custom field influences the price
			if(isset($affects_price)) $custom['price'] = 1;
			
			//Set whether the custom field is required or not
			if(isset($required)) $custom['required'] = 1;
			
			//Set the id of the custom field
			if(isset($_POST['custom_id'])){
				$custom_id = $_POST['custom_id'];
			} else { //This is a new custom field, so id of all custom fields group needs to be incremented 
				if(isset($custom_fields['id'])) $custom_fields['id'] = $custom_fields['id'] + 1;
				else $custom_fields['id'] = 1;
				$custom_id = $custom_fields['id'];
			}
			
			if(!isset($custom_fields['fields'])) $custom_fields['fields'] = '';
			$custom_fields['fields'][$custom_id] = $custom;
			update_option('reservations_custom_fields', $custom_fields);
			
			return array("success" => 1, "msg" => "The custom field was added succesfuly.");
		} else {
			return array("success" => 0, "msg" => "The name for the custom field was not given.");
			
		}
		
		
		
	}
}