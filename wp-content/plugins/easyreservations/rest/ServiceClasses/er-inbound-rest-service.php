<?php

require_once (realpath(dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))) . "/wp-admin/admin.php"));
require_once (ABSPATH . "wp-content/plugins/easyreservations/rest/Interfaces/i-inbound-easyreservations.php");
require_once (ABSPATH . "wp-content/plugins/easyreservations/rest/ServiceClasses/er-inbound-reservation-api.php");
require_once (ABSPATH . "wp-content/plugins/easyreservations/rest/ServiceClasses/er-inbound-resource-api.php");
require_once (ABSPATH . "wp-content/plugins/easyreservations/rest/ServiceClasses/er-inbound-hotel-api.php");

class ER_Inbound_Rest_Service implements I_Inbound_Easyreservations
{
	var $reservation_api;  
	var $resource_api;
	var $hotel_api;
	
	public function __construct()
	{		
		$this -> reservation_api = new ER_Inbound_Reservation_API();
		$this -> resource_api = new ER_Inbound_Resource_API();
		$this -> hotel_api = new ER_Inbound_Hotel_API();
	}
	
	public function getAllResources() 
	{
		try {
			$result = $this -> resource_api -> getAllResources();
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		
		return array("success" => 1, "msg" => "All resources were fetched successfuly", "result" => $result);
	
	}
	
	public function getResource($id) 
	{
		try {
			$result = $this -> resource_api -> getResource($id);
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		
		return array("success" => 1, "msg" => "Resource was fetched successfuly", "result" => $result);
		
			
	
	}
	
	public function edit_resource($id, $title = null, $content = null, $price = null, $childDiscount = null, $isPricePerPerson = null, $availability = null)
	{
		try {
			$result = $this -> resource_api -> edit_resource($id, $title, $content, $price, $childDiscount, $isPricePerPerson, $availability);
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		
		if(!$result) {
		    return array("success" => 1, "msg" => "Resource " . $id . " was edited successfuly", "result" => $result);
		} else {
			return array("success" => 0, "msg" => "Resource " . $id . " was not edited successfuly", "result" => $result);
		}
	
	}
	
	public function deleteResource($id)
	{
		try {
			$result = $this -> resource_api -> deleteResource($id);
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		
		return $result; //Returns 0 if resource or its reservations were not deleted properly, 1 otherwise
	
	}	
	
	public function addResource ($title = "Default resource title", $content = "Default resource content", $type = 1, $price = 0, $child = 0, $availability = 1, 
	    $isPricePerPerson = 0, $minPersons = 1, $maxPersons = 99, $minNights = 1, $maxNights = 999)
	{
		try {
			$result = $this -> resource_api -> addResource($title, $content, $type, $price, $child, $availability, $isPricePerPerson, $minPersons, $maxPersons, $minNights, $maxNights);
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		
		return array("success" => 1, "msg" => "Resource was created successfuly", "result" => $result);
		
	}
	
	public function get_availability_for_resource_on_date($resourceId = 1, $arrival = 0, $adults = 1, $childs = 0, $busyMode = false)
	{
		return $this -> resource_api -> get_availability_for_resource_in_period($resourceId, $arrival, $departure, $adults, $childs, $busyMode);
	
	}
	
    public function get_availability_for_resource_in_period($resourceId = 1, $arrival = 0, $departure = 0, $adults = 1, $childs = 0, $busyMode = false)
	{		
		return $this -> resource_api -> get_availability_for_resource_in_period($resourceId, $arrival, $departure, $adults, $childs, $busyMode);		
           
    }     
    
    public function get_all_roomtypes_available_for_criteria($roomtype, $arrival, $departure, $adults, $childs)
    {
    	return $this -> resource_api -> getAllAvailablePerPersonResources($roomtype, $arrival, $departure, $adults, $childs);
    	
    }
    
    public function add_resource_filter($general_filter_parameters, $filter_specific_parameters)
    {
    	//easyreservations Exception handling 
    	try {
    		$result = $this -> resource_api -> add_resource_filter($general_filter_parameters, $filter_specific_parameters);
    	} catch(easyException $e) {
    		return array("success" => 0, "msg" => $e -> getMessage());
    	}
    	
        return $result;
    	
    }
    
    public function edit_resource_filter($general_filter_parameters, $filter_specific_parameters)
    {
    	try {
    		$result = $this -> resource_api -> edit_resource_filter($general_filter_parameters, $filter_specific_parameters);
    	} catch(easyException $e) {
    		return array("success" => 0, "msg" => $e -> getMessage());
    	}
    	
    	return $result;
    	
        	
    }
    
    public function delete_resource_filter($array)
    {
    	try {
    		$result = $this -> resource_api -> delete_resource_filter($array);
    	} catch(easyException $e) {
    		return array("success" => 0, "msg" => $e -> getMessage() . $id);
    	}
    	
    	return $result;
    		
    }
   
    public function getReservation($id)
    {
    	try {
    		$result = $this -> reservation_api -> getReservation($id);
    	} catch(easyException $e) {
    		return array("success" => 0, "msg" => $e -> getMessage());
    	}
    	
    	return $result;
    	
    	
    }
	
	public function make_reservation_for_date($resource = null, 
		$resourcenumber = null, $arrival = null, $departure = null, $name = "Default name", $email = "default@email.com", $country = null, $adults = 1, $childs = 0, $custom = null,
		$prices = null,	$status = null,	$interval = null, $reservated = null, $times = null, $user = null, $price = null, $paid = null, $pricepaid = null, $resourcename = null) 
	{
		$information = array( "resource" => $resource, "resourcenumber" => $resourcenumber, "arrival" => $arrival, "departure" => $departure, "name" => $name,
            "email" => $email, "country" => $country, "adults" => $adults, "childs" => $childs, "custom" => $custom, "prices" => $prices, "status" => $status,
			"interval" => $interval, "reservated" => $reservated, "times" => $times, "user" => $user, "price" => $price, "paid" => $paid, "pricepaid" => $pricepaid,
			"resourcename" => $resourcename);		              

		return $this -> reservation_api -> make_reservation_for_date( $information );

	}
	
	public function make_reservation_for_period ($information)
	{	
		try {
			$result = $this -> $reservation_api -> make_reservation_for_period ( $information );
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		
        return $result;	
	
	}
	
	public function edit_reservation($information)
	{
	    try {
			$result = $this -> reservation_api -> edit_reservation ($information);
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		
		return $result;
		
	}
	
	public function cancel_reservation ($id) {
		try {
		    $result = $this -> reservation_api -> cancel_reservation($id);
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		    	
		return array("success" => 1, "msg" => "Reservation has been cancelled successfuly", "result" => $result);
	}
	
	
	public function delete_reservation ($id) {
		return $this -> reservation_api -> delete_reservation($id);
	}
	
	
	public function reject_reservation ($id) {
		try {
			$result = $this -> reservation_api -> reject_reservation($id);
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		 
		return array("success" => 1, "msg" => "Reservation has been rejected successfuly", "result" => $result);
	}
	
	
	public function approve_reservation ($id) {
		try {
			$result = $this -> reservation_api -> approve_reservation($id);
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		 
		return array("success" => 1, "msg" => "Reservation has been approved successfuly", "result" => $result);
	}
	
	
	public function get_rates_custom_fields()
	{
		try {
			$all = $this -> resource_api -> rates;
		} catch(Exception $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
		
		return array("success" => 1, "msg" => "Rates fields were fetched successfuly.", "result" => $all);
	}
	
	//Get all hotel's custom fields
	public function get_all_custom_fields()
	{
		try {
			$all = get_option("reservations_custom_fields");
		} catch(Exception $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
	
		return $all;
	}
	
	//Delete a hotel's custom field
	public function delete_custom_field($id)
	{
	    try {
			$result = $this -> hotel_api -> delete_custom_field($id);
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
			
		return $result;
	
	}
	
	//Add a new custom field to the hotel
	public function add_custom_field($parameters)
	{
		
		try {
			$result = $this -> hotel_api -> add_custom_field($parameters);
		} catch(easyException $e) {
			return array("success" => 0, "msg" => $e -> getMessage() . $id);
		}
			
		return $result;
		
	}
	
	
	
	
    
	
	
}