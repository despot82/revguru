<?php

interface I_Inbound_Reservations
{
	public function make_reservation_for_date($information);
	
	/**
	 * @param  $information
	 * Array
       (
           "resource" => Integer
           "resourcenumber" => Integer
           "arrival" => yyyy-mm-dd_hh:ii:ss
           "departure" => yyyy-mm-dd_hh:ii:ss
           "adults" => Integer
           "childs" => Integer 
           "status" => String (one of "yes, "no")
           "price" => 
           "paid" => 
           "name" => String
           "email" => String (xy@xy.xy)
           "country" => 
           "interval" => 
           "reservated" => 
       )
	 */
	public function make_reservation_for_period($information);
	
	/**
	 * @param $information
	 */
	public function edit_reservation($information);
	
	/**
	 * @param $id
	 */
	public function cancel_reservation ($id);
	
	/**
	 * @param $id
	 */
	public function delete_reservation ($id);
	
	/**
	 * @param $id
	 */
	public function reject_reservation ($id);
	
	/**
	 * @param $id
	 */
	public function approve_reservation ($id);
	
  	
  	
  	
}



?>