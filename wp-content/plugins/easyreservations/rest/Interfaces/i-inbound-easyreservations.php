<?php

interface I_Inbound_Easyreservations 
{
	public function getAllResources();
	
	public function getResource( $id );
		
	public function edit_resource($id, $title, $content, $price, $childDiscount, $isPricePerPerson, $availability);
	
	public function deleteResource ( $id );

	public function get_availability_for_resource_on_date( $resourceId = 1, $arrival = 0, $persons = 1, $busyMode = false );	
	
	public function get_availability_for_resource_in_period( $resourceId, $arrival, $departure, $persons, $busyMode );
		
	public function make_reservation_for_date( $resource = null, 
		$resourcenumber = null, $arrival = null, $departure = null, $name = "Default name", $email = "default@email.com", $country = null, $adults = 1, $childs = 0, $custom = null,
		$prices = null,	$status = null,	$interval = null, $reservated = null, $times = null, $user = null, $price = null, $paid = null, $pricepaid = null, $resourcename = null );
	
	public function make_reservation_for_period($information);
 	
	public function edit_reservation($information);
	
	public function cancel_reservation ( $id );
	
	public function delete_reservation ( $id );
	
	public function reject_reservation ( $id );
	
	public function approve_reservation ( $id );
	
}