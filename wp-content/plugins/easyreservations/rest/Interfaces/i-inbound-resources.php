<?php


interface I_Inbound_Resources
{
 	public function getAllResources();
 	
 	public function getResource($id);
 	
 	public function edit_resource($id, $title, $content, $price, $childDiscount, $isPricePerPerson, $availability);
 	
 	public function deleteResource($id); 	
 	
 	/**
 	 * @param  $information:
 	 * Array
	 *	 (
	 *	 resource_id,
	 *	 resourcenumber,
	 *	 arrival(in format yyyy-mm-dd_hh:ii:ss), 
	 *	 departure(in format yyyy-mm-dd_hh:ii:ss),
	 *	 adults,
	 *	 childs,
	 *	 status,
	 *	 price,
	 *	 paid,
	 *	 name,
	 *	 email,
	 *	 country,
	 *	 interval,
	 *	 reservated,
	 *	 )
 	 */
 	public function get_availability_for_resource_in_period($resource_id, $arrival, $departure, $persons, $busyMode = false);
 	
}


?>