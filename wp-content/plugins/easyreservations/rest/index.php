<?php

require "ServiceClasses/er-inbound-rest-service.php";
require "ServiceClasses/search-rest-service.php";
require_once "ServiceClasses/constants.php";

$ri = new ER_Inbound_Rest_Service();
$rs = new Search_Rest_Service();

$uri = $_SERVER['REQUEST_URI'];

$factors = explode("/", $uri);

$base = 3;

$entity = $factors[$base + 1];
$verb = $factors[$base + 2];

for ($i = $base + 3; $i < count($factors); $i = $i+2) {
	
	$parameters[$factors[$i]] = $factors[$i+1];
}

if ($entity == "resource") {
	
	if ($verb == "view") {
		
		if ($factors[$base + 3] == "all") {
			$all = $ri -> getAllResources();
			echo json_encode($all);
			return;
		}	    
		
		print "<pre>"; print_r($ri -> getResource($parameters["id"])); print "</pre>"; return;
		
		echo json_encode($ri -> getResource($parameters["id"]));		
		return;
	    
	}
	
	if ($verb == "availability") {
	    if ($parameters["roomtype"]) {
	    	echo json_encode($ri -> get_all_roomtypes_available_for_criteria ( $parameters["roomtype"], $parameters["arrival"], 
	    		empty($parameters["departure"]) ? 0 : $parameters["departure"], 
	    		empty($parameters["adults"]) ? 1 : $parameters["adults"], 
	    		empty($parameters["childs"]) ? 0 : $parameters["childs"] ) );
	    	return;
	    }
	    	
	    if ($parameters["id"]) {
	    	echo json_encode ( $ri -> get_availability_for_resource_in_period ( $parameters["id"], $parameters["arrival"], 
	    		empty($parameters["departure"]) ? 0 : $parameters["departure"], 
	    		empty($parameters["adults"]) ? 1 : $parameters["adults"], 
	    		empty($parameters["childs"]) ? 0 : $parameters["childs"] ));
	    	return;
	    	
	    }	
	    	
	}
	
	if($verb == "add") {
		echo json_encode($ri -> addResource( $parameters["title"], $parameters["content"], 
			$parameters["type"], $parameters["price"], $parameters["child"], $parameters["availability"], $parameters["ispriceperperson"], 
			$parameters["minpersons"], $parameters["maxpersons"], $parameters["minnights"], $parameters["maxnights"] ));
		return;
	}
	
	if($verb == "delete") {
		echo json_encode ( $ri -> deleteResource ( $parameters["id"] ) );
		return;
	}
	

	if ($verb == "edit") {
		echo json_encode($ri -> edit_resource( $parameters["id"], $parameters["title"],
		    $parameters["content"], $parameters["price"], $parameters["child"], $parameters["ispriceperperson"], $parameters["availability"] ) );				
		return;
	}

	
}

if ($entity == "reservation") {
	if($verb == "view") {
		
		if ($factors[$base + 3] == "all") {
			$all = $ri -> getAllReservations();
			echo json_encode($all);
			return;
		}	    
		
		//print "<pre>"; print_r($ri -> getReservation($parameters["id"])); print "</pre>"; return;
		print "<pre>";
		print_r ($ri -> getReservation($parameters["id"]));
		print "</pre>";
		
		return;
	
	}
	
	if ($verb == "add") {
		if($parameters["status"] == 1 || strtolower($parameters["status"]) == "yes") {
			$status_par = "yes";
		}	
		else
		if($parameters["status"] == 0 || strtolower($parameters["status"]) == "no") {
			$status_par = "no";
		}
		
		$information = array("resource" => $parameters["resource"], 
			"resourcenumber" => $parameters["resourcenumber"], 
		    "arrival" => $parameters["arrival"], 
			"departure" => $parameters["departure"], 
			"adults" => $parameters["adults"], 
			"childs" => $parameters["childs"], 
			"board_type" => $parameters["board_type"], 
			"price" => $parameters["price"], 
			"paid" => $parameters["paid"],
			"name" => $parameters["name"],
			"email" => $parameters["email"],
			"country" => $parameters["country"],
			"paid" => $parameters["paid"],
			"status" => $status_par
		);
		
		print "<pre>";
		print_r($ri -> make_reservation_for_period($information));
		print "</pre>";
		
		return;
	}
	
	if ($verb == "approve") {
	    echo json_encode ( $ri -> approve_reservation ( $parameters["id"] ));
	    return;
	}
	
	if ($verb == "edit" ) {
		if($parameters["status"] == 1 || !isset($parameters["status"])) {
			$status_par = "yes";
		}
		else
		if($parameters["status"] == 0) {
			$status_par = "no";
		}
		
		$information = array("id" => $parameters["id"],
				"resource" => $parameters["resource"],
				"resourcenumber" => $parameters["resourcenumber"],
				"arrival" => $parameters["arrival"],
				"departure" => $parameters["departure"],
				"adults" => $parameters["adults"],
				"childs" => $parameters["childs"],
				"board_type" => $parameters["board_type"],
				"price" => $parameters["price"],
				"paid" => $parameters["paid"],
				"name" => $parameters["name"],
				"email" => $parameters["email"],
				"country" => $parameters["country"],
				"paid" => $parameters["paid"],
				"status" => $status_par
		);
		
		print "<pre>";
		print_r($ri -> edit_reservation($information));
		print "</pre>";
		
		return;
	}
	

	if ($verb == "cancel") {
		echo json_encode ( $ri -> cancel_reservation ( $parameters["id"] ));
		return;
	}
	
	if ($verb == "reject") {
		echo json_encode ( $ri -> reject_reservation ( $parameters["id"] ));
		return;
	}
	
	if ($verb == "delete") {
		echo json_encode ( $ri -> delete_reservation ( $parameters["id"] ));
		return;
	}	
	
}

if ($entity == "filter") {
	if ($verb == "add") {
		
		$general_filter_parameters = array("resourceId" => $parameters["resourceid"],
	        "type" => $parameters["type"],
			"name" => $parameters["name"]
		);
		
		foreach ($parameters as $name => $value) {
			if (!in_array(strtolower($name), array("resourceid","type","name"))) {
			
				$other_parameters[$name] = $value;	
			}
		}
		
	    echo json_encode($ri -> add_resource_filter($general_filter_parameters, $other_parameters));
	    
		return;
		
	}
	
	if ($verb == "edit") {
		$general_filter_parameters = array("resourceId" => $parameters["resourceid"],
			"filter_id" => $parameters["filter_id"],
			"type" => $parameters["type"],
			"name" => $parameters["name"]
		);
		
		foreach ($parameters as $name => $value) {
			if (!in_array(strtolower($name), array("resourceid", "filter_id", "type", "name"))) {
			
				$other_parameters[$name] = $value;	
			}
		}
		
	    echo json_encode($ri -> edit_resource_filter($general_filter_parameters, $other_parameters));
	    
		return;
	}
	
	if ($verb == "delete") {
		echo json_encode ( $ri -> delete_resource_filter ( 
		    array(	
			    "resourceId" => $parameters["resourceid"], 
		    	"filter_id" => $parameters["filter_id"]
		    ))
		);
		return;
	}
}


if ($entity == "search") {  
	
	if ($verb == "all") {
				
		$groups = array();
		
		foreach ($_POST["adults"] as $key => $adults) {
			   $groups[] = array("adults" => $_POST["adults"][$key],
					          "childs" => $_POST["childs"][$key]);
					            
		}
		
		$arrExp = explode("/", $_POST["arrival"]);
		$depExp = explode("/", $_POST["departure"]);
		
		
		$arrival = $arrExp[2] . "-" . $arrExp[0] . "-" . $arrExp[1] . " 00:00:00";
		$departure = $depExp[2] . "-" . $depExp[0] . "-" . $depExp[1] . " 00:00:00";
		
		print "<pre>";
		print_r ( $rs -> get_all_hotels_that_can_accommodate(array("destinationType" => $_POST["destinationType"],
				                                                   "destinationId" => $_POST["destinationId"]),
				                                                   $groups,
				                                                   $arrival,
				                                                   $departure				                                                 
				                                                ));
		print "</pre>";
		
	}
	
	if ($verb == "hotel") {
		echo json_encode();
		return;
	}
	
}

if($entity == "hotel") {
	if($verb == "delete") {
		
	}
}

if($entity == "custom") {
	if($verb == "view") {
		if ($factors[$base + 3] == "all") {
			$all = $ri -> get_all_custom_fields();
			
			print "<pre>"; print_r($all); print "</pre>";
			
			return;
		}	
		
		if ($factors[$base + 3] == "rates") {
			$rates = $ri -> get_rates_custom_fields();
				
			print "<pre>"; print_r($rates); print "</pre>";
				
			return;
		}
	    	
	}
	
    if($verb == "add") {
		
			$rates = $ri -> add_custom_field($parameters);
				
			print "<pre>"; print_r($rates); print "</pre>";
				
			return;	
	}
	
	if($verb == "edit") {
		
	
	}
	
	if($verb == "delete") {
		$result = $ri -> delete_custom_field($parameters["id"]);
		
		print "<pre>"; print_r($result); print "</pre>";
		
		return;
	
	}
}



