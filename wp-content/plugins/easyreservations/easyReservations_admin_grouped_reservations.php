<?php

function grouped_reservations_page()
{
	global $wpdp;
	
	$args = array(
			'posts_per_page'   => 10,
			'orderby'          => 'ID',
			'order'            => 'ASC',			
			'post_type'        => 'groupedreservation',			
			'post_status'      => 'private'
			
	);
	
	$grouped_reservations = get_posts( $args ); 
	
	?>
	<style>
	table, th, td {
		   border: 1px solid black;
		   
	} 
	</style>
	
	<table cellpading=0 cellspacing=0 >
	<tr>
	    <th>Grouped Reservation (Multi-Reservation) ID</th>
	    <th>Reservation ID</th>
	    <th>Room ID (Resource ID)</th>
	    <th>Room number (Resource Number)</th>	
	    <th>Arrival date</th>	
	    <th>Departure date</th>	    
	    <!--  <th>Room maximum occupancy</th>  -->
	    <!--  <th>Price of the room</th>  -->
	    <th>Price/Paid amount for the whole stay (Reservation price)</th>
	</tr>
	
<?php

    //print "DEV: grouped reservations:<pre>"; print_r($grouped_reservations); print "</pre>";
    
	foreach ($grouped_reservations as $grouped_reservation_post ) {
		
		$post_exploded = explode(";", $grouped_reservation_post -> post_content);
		$reservation_ids = explode(",", $post_exploded[0]);
	    
		$gr_id_displayed = false;
		
		foreach ($reservation_ids as $reservation_id) {
			//echo "DEV: and the rid is: " . $reservation_id;
			echo "<tr>";
			
			if(!$gr_id_displayed) {
			    echo "<td rowspan=" . count($reservation_ids) . ">";
			    echo $grouped_reservation_post -> ID;
			    echo "</td>";
			    
			    $gr_id_displayed = true;
			}
			
			
			$reservation_ok = true;
			try {
			    $reservation = new Reservation($reservation_id);
			} catch(Exception $e) {
				//print "DEV: exception:<pre>"; print_r($e); print "</pre>";
			    $reservation_ok = false;
			}
			
			echo "<td>";
			echo $reservation_id;
			echo "</td>";
			
			if($reservation_ok) {
				echo "<td>";
				echo $reservation -> resource;
				echo "</td>";
				
				echo "<td>";
				echo $reservation -> resourcenumber;
				echo "</td>";
				
				echo "<td>";
				echo date('Y-m-d H:i', $reservation -> arrival);
				echo "</td>";
				
				echo "<td>";
				echo date('Y-m-d H:i', $reservation -> departure);
				echo "</td>";
				
				echo "<td>";
				echo $reservation -> pricepaid;
				echo "</td>";
			
			} else {
				echo "<td colspan = 5>";
			    echo "Reservation info is not available. ";
		        echo "</td>";
			    
			}
			
			echo "</tr>";
		}
	}
		
	
?>
		
		       
		</table>
<?php

}

?>