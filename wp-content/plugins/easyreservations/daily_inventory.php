<?php

    require_once "rest/ServiceClasses/er-inbound-resource-api.php";
    $resource_api = new ER_Inbound_Resource_API();
    
    $all_rooms = easyreservations_get_rooms();
    //print "<pre>"; print_r($all_rooms); print "</pre>";
    
    //Daily inventory for either the specified maximum occupancy or for all rooms
    $occupancy = $_POST["occupancy"];
    $day = $_POST["day"];
   
    
?>

<style>
	table#availability, table#availability th, table#availability td {
		   border: 1px solid black;
		   
	} 
</style>  
  
<table id='availability'>
    <tr>
        <th>Room ID</th>
        <th>Maximum occupancy</th>
        <th>Availability</th>
    </tr>    
    
<?php     
    if ($occupancy == "All") { //Display availabilities for all rooms, one in each row
    	//get_availability_for_resource_in_period
    	foreach($all_rooms as $id => $room) {
    		echo "<tr>";
    		echo "<td>" . $id . "</td>";
    		
    		$resource = new Resource($id);
    		$requirements = $resource -> req;
    		$max_occupancy = $requirements["pers-max"];
    		echo "<td>" . $max_occupancy . "</td>";
    		
    		//Reformat dates
    		$arrExp = explode("/", $_POST["day"]);
    	    $arrival = $arrExp[2] . "-" . $arrExp[1] . "-" . $arrExp[0] . "_00:00:00";
    	
    		$availability_info = $resource_api -> get_availability_for_resource_in_period($id, $arrival);
    		$availability = $availability_info["number_of_available_rooms"];
    		echo "<td>" . $availability . "</td>";
    		
    		
    		echo "</tr>";
    		
    		//print "<br><pre>A:"; print_r($availability); print "</pre>";
    	}
    	
    } else { //Just display the rooms that have the specified occupancy, and the total on the bottom of the availabillity column
    	$total = 0;
    	foreach($all_rooms as $id => $room) {
    	
    		$resource = new Resource($id);
    		$requirements = $resource -> req;
    		$max_occupancy = $requirements["pers-max"];
    		
    	    if($occupancy == $max_occupancy) {
	    		//Reformat dates
	    		$arrExp = explode("/", $_POST["day"]);
	    		$arrival = $arrExp[2] . "-" . $arrExp[1] . "-" . $arrExp[0] . "_00:00:00";
	    		 
	    		$availability_info = $resource_api -> get_availability_for_resource_in_period($id, $arrival);
	    		$availability = $availability_info["number_of_available_rooms"];
	    		$total += $availability;
	    		
	    		echo "<tr>";
	    		echo "<td>" . $id . "</td>";
	    		echo "<td>" . $max_occupancy . "</td>";
	    		echo "<td>" . $availability . "</td>";
	    	    echo "</tr>";
    	    }
    	
    		//print "<br><pre>A:"; print_r($availability); print "</pre>";
    	}
    	
    }
    
?>

<tr>
<td></td>
<td>Total:</td>
<td><?php echo $total; ?></td>
</tr>

</table>   

<?php  
    
    //echo "<br>day is: " . $day;
    //echo "<br>occ. is: " . $occupancy;
    
    die();
    
    
    
?>