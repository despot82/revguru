<?php

function daily_inventory_page()
{
	//Determine what to put in the occupancy select box and put it there below
	require_once "rest/ServiceClasses/er-inbound-resource-api.php";
	$resource_api = new ER_Inbound_Resource_API();
	
	$all_rooms = easyreservations_get_rooms();
	
	$max_occupancies = array();
	
	foreach($all_rooms as $id => $room) {
		
		$resource = new Resource($id);
		$requirements = $resource -> req;
		
		if(!in_array($requirements["pers-max"], $max_occupancies)) {
			$max_occupancies[] = $requirements["pers-max"];
		}
		
    }
    
    sort($max_occupancies);
  
    //print "MO:<br><pre>"; print_r($max_occupancies); print "</pre>";
	
?>

    <script type="text/javascript" src="<?php echo get_home_url(1) . "/js/jquery-1.12.0.min.js"; ?>"></script>
    <script type="text/javascript" src="<?php echo get_home_url(1) . "/js/jquery-ui.js"; ?>"></script>
	<script type="text/javascript" src="<?php echo plugin_dir_url(__FILE__). "js/daily_inventory.js"; ?>"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	

    <br>
	<div>
	    <input id='day'/>   
	    <select id='occupancy'>
	        <option>All</option>
	        <?php 
	            foreach ($max_occupancies as $max_occupancy) {
	            	echo "<option>" . $max_occupancy . "</option>";
	            }
	        ?>
	    </select>
	    <input type="button" id="refresh" value="Refresh" onclick="display();" />
	
	
	</div>
	
	<div id='results'>
	    
	    
	    
	    
	    
	</div>

<?php 

}

?>


