<?php

    //HOTEL HEADER IMAGE UPDATE HOOK

    //Hook into event when header image for a hotel is changed, to make note of it and update the table for it
	add_action('wp_header_image_attachment_metadata', 'update_header_images_table' );
	
	function update_header_images_table($par = null)
	{
		global $wpdb;
		
		$path = network_site_url() . "wp-content/uploads/sites/" 
			  . get_current_blog_id() . "/" 
			  . substr($par["file"], 0, 8) . $par["sizes"]["thumbnail"]["file"];
		
		$sql = "UPDATE blogs_header_images " 
			 . "SET blog_header_image_url = '" . $path . "' "
			 . "WHERE blog_id = " . get_current_blog_id();
		
		$wpdb -> get_results($sql);
	    
	}
	
	//CONDENSED TABLES UPDATE HOOKS
	
	//POST *********************************************************************************************
	
	//Hook into adding a post, catch the new post's id and update it in the quick search tables
	add_action("wp_insert_post", "post_added_handler", 10, 3);
	function post_added_handler($post_id, $post, $update)
	{
		global $wpdb;
		
		error_log("DEV: in post_added_handler for post: " . $post_id);
		
		$sql = "INSERT INTO all_posts SELECT " . get_current_blog_id() . ", p.* FROM wp_" . get_current_blog_id() 
		     . "_posts AS p WHERE ID=" . $post_id;
		error_log("DEV: sql is: " . $sql);
		$wpdb -> query ($sql);
		
		
		
		
	}
	
	//Hook into deleting a post, catch the changes and update it in the quick search tables (condensed tables)
	add_action("deleted_post", "post_deleted_handler",10, 1);	
	function post_deleted_handler($post_id)
	{
		global $wpdb;
		
		//error_log("DEV: in post_deleted_handler for post: " . $post_id);
		
		$sql = "DELETE FROM all_posts WHERE hotelId = " . get_current_blog_id() . " AND ID = " . $post_id;
		error_log("DEV: sql is: " . $sql);
		$wpdb -> query ($sql);
	}
	
	//POSTMETA ******************************************************************************************
	
	//Hook into adding postmeta for a post, catch the changes and update it in the quick search tables
	add_action( "added_post_meta", "postmeta_added_handler", 10, 4);
	function postmeta_added_handler($meta_id, $object_id, $meta_key, $meta_value)
	{	
		global $wpdb;
		
		//error_log("DEV: in postmeta_added_handler");
		 
		$sql = "INSERT INTO all_postmeta SELECT " . get_current_blog_id() . ", m.* FROM wp_" . get_current_blog_id() 
		       . "_postmeta AS m WHERE m.meta_id = " . $meta_id;
		error_log("DEV: sql is: " . $sql);
		$wpdb -> query ($sql);
		
		
	}
	
	//Hook into editing postmeta for a post, catch the changes and update it in the quick search tables
	add_action( 'updated_postmeta', 'postmeta_updated_handler', 10, 4);	
	function postmeta_updated_handler( $meta_id, $post_id, $meta_key, $meta_value=null ) 
	{
		global $wpdb;
		
		//error_log("DEV: in postmeta_updated_handler");
		
		$blog_id = get_current_blog_id();
		
		$sql = "DELETE FROM all_postmeta WHERE hotelId = " . $blog_id . " AND meta_id = " . $meta_id;	
		error_log("DEV: sql is: " . $sql);
		$wpdb -> query($sql);
		
		
		$sql = "INSERT INTO all_postmeta SELECT " . $blog_id . ", m.* FROM wp_" . $blog_id
		     . "_postmeta AS m WHERE m.meta_id = " . $meta_id;		
		error_log("DEV: sql is: " . $sql);
		$wpdb -> query($sql);
		
		
	}
	
	//Hook into deleting postmeta for a post, catch the changes and update it in the quick search tables
	add_action('deleted_postmeta', 'postmeta_deleted_handler', 10, 1);
	function postmeta_deleted_handler($meta_ids)
	{	
		global $wpdb;
		
		if (is_array($meta_ids)) {
			foreach ($meta_ids as $meta_id) {
				//error_log("DEV: in postmeta_deleted_handler for postmeta: " . $meta_id);
				
				$sql = "DELETE FROM all_postmeta WHERE hotelId = " . get_current_blog_id() . " AND meta_id = " . $meta_id;
				//error_log("DEV: sql is: " . $sql);
				$wpdb -> query ($sql);
			}
		} else {
			//error_log("DEV: in postmeta_deleted_handler for postmeta: " . $meta_ids);
			
			$sql = "DELETE FROM all_postmeta WHERE hotelId = " . get_current_blog_id() . " AND meta_id = " . $meta_ids;
			error_log("DEV: sql is: " . $sql);
			$wpdb -> query ($sql);
		}
		
	}
	
	//RESERVATIONS ********************************************************************************************
	
	//Hook into add reservation process, catch the changes and update it in the quick search tables
	add_action('easy-add-res', 'reservation_added_handler', 10, 1);
	function reservation_added_handler($reservation)
	{	
		global $wpdb;
		
		//error_log("DEV: in reservation_added_handler for reservation: " . $reservation -> id);
		
		$blog_id = get_current_blog_id();
		
		$sql = "INSERT INTO all_reservations SELECT " . $blog_id . ", r.* FROM wp_" . $blog_id
		     . "_reservations AS r WHERE r.id = " . $reservation -> id;
		//error_log("DEV: sql is: " . $sql);
		$wpdb -> query($sql);
	}
	
	//Hook into editing reservation process, catch the changes and update it in the quick search tables 
	add_action('easy-after-edit', 'reservation_edited_handler', 10, 1);	
	function reservation_edited_handler($reservation)
	{	
		global $wpdb;
		
		//error_log("DEV: in reservation_edited_handler for reservation: " . $reservation -> id);
		
		$blog_id = get_current_blog_id();
		
		$sql = "DELETE FROM all_reservations WHERE hotelId = " . $blog_id . " AND id = " . $reservation -> id;
		//error_log("DEV: sql is: " . $sql);
		$wpdb -> query($sql);
		
		$sql = "INSERT INTO all_reservations SELECT " . $blog_id . ", r.* FROM wp_" . $blog_id
		     . "_reservations AS r WHERE r.id = " . $reservation -> id;
		//error_log("DEV: sql is: " . $sql);
		$wpdb -> query($sql);
	}
	
	//Hook into delete reservation process, catch the changes and update it in the quick search tables
	add_action('easy-delete-res', 'reservation_deleted_handler', 10, 1);
	function reservation_deleted_handler($reservation)
	{
		global $wpdb;
		
		//error_log("DEV: in reservation_deleted_handler for reservation: " . $reservation -> id);
		
		$blog_id = get_current_blog_id();
		
		$sql = "DELETE FROM all_reservations WHERE hotelId = " . $blog_id . " AND id = " . $reservation -> id;
		//error_log("DEV: sql is: " . $sql);
		$wpdb -> query($sql);
	}	
	
	//Add the formatting of money as the ajax call whether front end user is admin or not
	add_action( 'wp_ajax_nopriv_money_format', 'money_format_callback' );
	add_action( 'wp_ajax_money_format', 'money_format_callback' );
	function money_format_callback()
	{
		switch_to_blog($_POST["current_blog_id"]);
		
		echo html_entity_decode(easyreservations_format_money($_POST["amount"], 1));
		
		if(isset($_POST["total"])) {
		    echo ";" . html_entity_decode(easyreservations_format_money($_POST["total"], 1));
		}
		
		
		wp_die();
	}
	
	//Enable featured image supprot for posts
	add_action( 'init', 'add_featured_image_support_for_rooms' );
	function add_featured_image_support_for_rooms() {
		add_post_type_support( 'easy-rooms', 'thumbnail' );
		add_theme_support('post-thumbnails');
	}
	
	//Add daily report admin ajax call for refreshing of the report
    add_action( 'wp_ajax_daily_inventory', 'daily_inventory_callback' );
	function daily_inventory_callback()
	{
	    include "daily_inventory.php";
	}
	
	
	
	

?>