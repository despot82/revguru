<?php

/*
 
 Plugin Name: SendGrid for Revguru
 Description: Plugin for management of emails sent through SendGrid 
 Version: 1.0
 Author: Vladimir Despotovic
 
*/


add_action( 'admin_menu', 'register_sendgrid_menu_item');

function register_sendgrid_menu_item(){
	add_menu_page( 
			'SendGrid Management',         //page title 
			'SendGrid',               //menu title
			'manage_options',          //capability
			'SendGrid',               //menu slug
			'SendGrid_management_page',          //function
			plugins_url( 'SendGrid/images/logo.png' ),    //icon url
			73 );                      //position

}


function SendGrid_management_page() {
    require "sendgrid-manager-page.php";
}



?>