<html>
<head>
    
    <script src="http://code.jquery.com/jquery-2.1.0.js"></script>
    <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    
<?php 
    wp_enqueue_script("SAA_scripts", plugins_url("/js/scripts.js", __FILE__) );    
    wp_enqueue_style("SAA_styles", plugins_url("/styles.css", __FILE__) );  
?> 

</head>
<body>

<div id="title" class="section-title" >
Administration for the accommodation search page 
</div>

<div id="es_suggestion_box" class="postbox es-form-box">

	<div id="es_title" class="section-title" >
	ElasticSearch (ES) Indexers
	</div>
	
	<!-- Rebuild suggestion index section -->
	<div id="reset_es_suggestion_index" name="reset_es_suggestion_index" >
	  <input class="es-setup-action-button" type="button" id="reset_es_suggestion_index_button" value="Reset ES suggestion index" />
	  <input class="action-result" type="text" id="reset_es_suggestion_index_result" name ="reset_es_suggestion_index_result" disabled="disabled" >
	  
	</div>
	<br/>
	
	
	<!-- Index hotels for suggestion  -->
	<div id="index_hotels_for_suggestions" name="index_hotels_for_suggestions" >
	  <input class="es-setup-action-button" type="button" id="index_hotels_for_suggestions_button" value="Index hotels for suggestions" />
	  <input class="action-result" type="text" id="index_hotels_for_suggestions_result" name ="index_hotels_for_suggestions_result" disabled="disabled">
	</div>
	<br/>
	
	
	<!-- Index cities for suggestions -->
	<div id="index_cities_for_suggestions" name="index_cities_for_suggestions" >
	  <input class="es-setup-action-button" type="button" id="index_cities_for_suggestions_button" value="Index cities for suggestions" />
	  <input class="action-result" type="text" id="index_cities_for_suggestions_result" name ="index_cities_for_suggestions_result" disabled="disabled">
	</div>
	<br/>
	
	
	<!-- Index states for suggestions -->
	<div id="index_states_for_suggestions" name="index_states_for_suggestions" >
	  <input class="es-setup-action-button" type="button" id="index_states_for_suggestions_button" value="Index states for suggestions" />
	  <input class="action-result" type="text" id="index_states_for_suggestions_result" name ="index_states_for_suggestions_result" disabled="disabled">
	</div>
	<br/>
	
	
	<!-- Index regions section for suggestions -->
	<div id="index_regions_for_suggestions" name="index_regions_for_suggestions" >
	  <input class="es-setup-action-button" type="button" id="index_regions_for_suggestions_button" value="Index regions for suggestions" />
	  <input class="action-result" type="text" id="index_regions_for_suggestions_result" name ="index_regions_for_suggestions_result" disabled="disabled">
	</div>
	<br/>
	
	
	<!-- Index countries for suggestions -->
	<div id="index_countries_for_suggestions" name="index_countries_for_suggestions" >
	  <input class="es-setup-action-button" type="button" id="index_countries_for_suggestions_button" value="Index countries for suggestions" />
	  <input class="action-result" type="text" id="index_countries_for_suggestions_result" name ="index_countries_for_suggestions_result" disabled="disabled">
	</div>
	<br/>
	
	
	<!-- Index points of interest for suggestions -->
	<div id="index_pointsofinterest_for_suggestions" name="index_pointsofinterest_for_suggestions" >
	  <input class="es-setup-action-button" type="button" id="index_pointsofinterest_for_suggestions_button" value="Index POI-s for suggestions" />
	  <input class="action-result" type="text" id="index_pointsofinterest_for_suggestions_result" name ="index_pointsofinterest_for_suggestions_result" disabled="disabled">
	</div>
	<br/>
	
</div>

<div id="es_suggestion_box" class="postbox es-form-box">

	<div id="es_title" class="section-title" >
	ElasticSearch (ES) Deleters
	</div>
	
	<!-- Delete hotels for suggestion  -->
	<div id="delete_hotels_for_suggestions" name="delete_hotels_for_suggestions" >
	  <input class="es-setup-action-button" type="button" id="delete_hotels_for_suggestions_button" value="Delete hotels for suggestions" />
	  <input class="action-result" type="text" id="delete_hotels_for_suggestions_result" name="delete_hotels_for_suggestions_result" disabled="disabled">
	</div>
	<br/>
	
	
	<!-- Delete cities for suggestions -->
	<div id="delete_cities_for_suggestions" name="delete_cities_for_suggestions" >
	  <input class="es-setup-action-button" type="button" id="delete_cities_for_suggestions_button" value="Delete cities for suggestions" />
	  <input class="action-result" type="text" id="delete_cities_for_suggestions_result" name ="delete_cities_for_suggestions_result" disabled="disabled">
	</div>
	<br/>
	
	
	<!-- Delete states for suggestions -->
	<div id="delete_states_for_suggestions" name="delete_states_for_suggestions" >
	  <input class="es-setup-action-button" type="button" id="delete_states_for_suggestions_button" value="Delete states for suggestions" />
	  <input class="action-result" type="text" id="delete_states_for_suggestions_result" name ="delete_states_for_suggestions_result" disabled="disabled">
	</div>
	<br/>
	
	
	<!-- Delete regions section for suggestions -->
	<div id="delete_regions_for_suggestions" name="delete_regions_for_suggestions" >
	  <input class="es-setup-action-button" type="button" id="delete_regions_for_suggestions_button" value="Delete regions for suggestions" />
	  <input class="action-result" type="text" id="delete_regions_for_suggestions_result" name ="delete_regions_for_suggestions_result" disabled="disabled">
	</div>
	<br/>
	
	
	<!-- Delete countries for suggestions -->
	<div id="delete_countries_for_suggestions" name="delete_countries_for_suggestions" >
	  <input class="es-setup-action-button" type="button" id="delete_countries_for_suggestions_button" value="Delete countries for suggestions" />
	  <input class="action-result" type="text" id="delete_countries_for_suggestions_result" name ="delete_countries_for_suggestions_result" disabled="disabled">
	</div>
	<br/>
	
	
	<!-- Delete points of interest for suggestions -->
	<div id="delete_pointsofinterest_for_suggestions" name="delete_pointsofinterest_for_suggestions" >
	  <input class="es-setup-action-button" type="button" id="delete_pointsofinterest_for_suggestions_button" value="Delete POI-s for suggestions" />
	  <input class="action-result" type="text" id="delete_pointsofinterest_for_suggestions_result" name ="delete_pointsofinterest_for_suggestions_result" disabled="disabled">
	</div>
	<br/>
	
</div>

<div id = "hotels" class="postbox hotels-form-box">

    <div id="hotels_title" class="section-title" >
	Hotels
	</div>

	<!-- Delete all blogs section -->
	<div id="delete_all_blogs" name="delete_all_blogs" >
	  <input class="hotels-setup-action-button" type="button" id="delete_all_blogs_button" value="Delete all blogs" />
	  <input class="action-result" type="text" id="delete_all_blogs_result" name ="delete_all_blogs_result" disabled="disabled">
	</div>
	<br/>
	 
	 
	<!-- Import hotels from MySQL table  --> 
	<div id="import_hotels" name="import_hotels" >
	  <input class="hotels-setup-action-button" type="button" id="import_hotels_button" value="Import hotels MySQL -> WP"/>
	  <input class="action-result" type="text" id="import_hotels_result" name ="import_hotels_result" disabled="disabled">
	</div>
	<br/>
	
	<!-- Activate EasyReservations plugin on all hotels blogs  --> 
	<div id="activate_er" name="activate_er" >
	  <input class="hotels-setup-action-button" type="button" id="activate_er_button" value="Activate EasyReservations on all hotels"/>
	  <input class="action-result" type="text" id="activate_er_result" name ="activate_er_result" disabled="disabled">
	</div>
	<br/>
	
	<!-- Empty all hotels  --> 
	<div id="empty_hotels" name="empty_hotels" >
	  <input class="hotels-setup-action-button" type="button" id="empty_hotels_button" value="Delete all rooms from all hotels"/>
	  <input class="action-result" type="text" id="empty_hotels_result" name ="empty_hotels_result" disabled="disabled">
	</div>
	<br/>
	 
	<!-- Import rooms from MySQL table  -->  
	<div id="import_rooms" name="import_rooms" >
	  <input class="hotels-setup-action-button" type="button" id="import_rooms_button" value="Import rooms MySQL -> WP"/>
	  <input class="action-result" type="text" id="import_rooms_result" name ="import_rooms_result" disabled="disabled">
	  
	</div>
	<br/>
	
	<!--  Condense tables for search --> 
	<div id="condense_tables" name="condense_tables" >
	  <input class="hotels-setup-action-button" type="button" id="condense_tables_button" value="Condense tables for search" />
	  <input class="action-result" type="text" id="condense_tables_result" name ="condense_tables_result" disabled="disabled">
	  
	  
	</div>
	<br/>

</div>
<br/>

</body>



</html>


