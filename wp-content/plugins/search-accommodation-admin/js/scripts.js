$(document).ready(function () {

/*****************************  ES INDEXERS  *****************************/
	
$("#reset_es_suggestion_index_button").click(function(){ 	
	$( "#reset_es_suggestion_index_result" ).val( "DELETING..." );
    $.ajax({  
    	       url: "../elasticsearch/import/delete-index.php", 
    	       type: "GET",    	   
    	       cache: false,
    	       
           })
           .done(function( html ) {
        	   $( "#reset_es_suggestion_index_result" ).val( "REBULDING..." );
        	   $.ajax({  
    	                 url: "../elasticsearch/import/setup-destination-suggest-index.php", 
    	                 method: "GET", 
    	                 cache: false
    	             })
    	             .done(function( html ) {
    	            	 $( "#reset_es_suggestion_index_result" ).val( "DONE" );
	                 });
	       });    
});


$("#index_hotels_for_suggestions_button").click(function(){     
	$( "#index_hotels_for_suggestions_result" ).val("INDEXING...");
    
	$.ajax({  
    	       url: ajaxurl, 
    	       type: "POST", 
    	       cache: false,
    	       data: { 
    	    	   action: 'es_index_hotels'
    	       }
    	       
    	   })
    	   .done(function(html) {    		   
    		   $( "#index_hotels_for_suggestions_result" ).val( "DONE" );  
    	   });
    
});



$("#index_cities_for_suggestions_button").click(function(){   
	$( "#index_cities_for_suggestions_result" ).val( "INDEXING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'es_index_cities'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#index_cities_for_suggestions_result" ).val( "DONE" );  
	   });
});


$("#index_regions_for_suggestions_button").click(function(){   
	$( "#index_regions_for_suggestions_result" ).val( "INDEXING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'es_index_regions'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#index_regions_for_suggestions_result" ).val( "DONE" );  
	   });
});


$("#index_states_for_suggestions_button").click(function(){    
	$( "#index_states_for_suggestions_result" ).val( "INDEXING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'es_index_states'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#index_states_for_suggestions_result" ).val( "DONE" );  
	   });
});




$("#index_countries_for_suggestions_button").click(function(){     
	$( "#index_countries_for_suggestions_result" ).val( "INDEXING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'es_index_countries'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#index_countries_for_suggestions_result" ).val( "DONE" );  
	   });
});


$("#index_pointsofinterest_for_suggestions_button").click(function(){    
	$( "#index_pointsofinterest_for_suggestions_result" ).val( "INDEXING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'es_index_points_of_interest'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#index_pointsofinterest_for_suggestions_result" ).val( "DONE" );  
	   });
});


/*****************************  ES DELETERS  *****************************/


$("#delete_hotels_for_suggestions_button").click(function(){     
	$( "#delete_hotels_for_suggestions_result" ).val("DELETING...");
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'es_delete_hotels'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#delete_hotels_for_suggestions_result" ).val( "DONE" );  
	   });
});



$("#delete_cities_for_suggestions_button").click(function(){   
	$( "#delete_cities_for_suggestions_result" ).val( "DELETING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'es_delete_cities'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#delete_cities_for_suggestions_result" ).val( "DONE" );  
	   });
});


$("#delete_states_for_suggestions_button").click(function(){   
	$( "#delete_states_for_suggestions_result" ).val( "DELETING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'es_delete_states'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#delete_states_for_suggestions_result" ).val( "DONE" );  
	   });
});


$("#delete_regions_for_suggestions_button").click(function(){    
	$( "#delete_regions_for_suggestions_result" ).val( "DELETING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'es_delete_regions'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#delete_regions_for_suggestions_result" ).val( "DONE" );  
	   });
});




$("#delete_countries_for_suggestions_button").click(function(){     
	$( "#delete_countries_for_suggestions_result" ).val( "DELETING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'es_delete_countries'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#delete_countries_for_suggestions_result" ).val( "DONE" );  
	   });
});


$("#delete_pointsofinterest_for_suggestions_button").click(function(){    
	$( "#delete_pointsofinterest_for_suggestions_result" ).val( "DELETING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'es_delete_pointsofinterest'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#delete_pointsofinterest_for_suggestions_result" ).val( "DONE" );  
	   });
});


/*****************************  HOTEL IMPORTERS  *****************************/


$("#delete_all_blogs_button").click(function(){  
	$( "#delete_all_blogs_result" ).val( "DELETING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'delete_all_blogs'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#delete_all_blogs_result" ).val( "DONE" );  
	   });
});

	
$("#import_hotels_button").click(function(){  
	$( "#import_hotels_result" ).val( "IMPORTING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'import_hotels'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#import_hotels_result" ).val( "DONE" );  
	   });
});


$("#activate_er_button").click(function(){  
	$( "#activate_er_result" ).val( "ACTIVATING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'activate_er_on_all_hotels'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#activate_er_result" ).val( "DONE" );  
	   });
});



$("#empty_hotels_button").click(function(){  
	$( "#empty_hotels_result" ).val( "EMPTYING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'empty_all_hotels'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#empty_hotels_result" ).val( "DONE" );  
	   });
});


$("#import_rooms_button").click(function(){    
	$( "#import_rooms_result" ).val( "IMPORTING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'import_rooms'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#import_rooms_result" ).val( "DONE" );  
	   });
	
});


$("#condense_tables_button").click(function(){ 
	$( "#condense_tables_result" ).val( "CONDENSING..." );
    
	$.ajax({  
	       url: ajaxurl, 
	       type: "POST", 
	       cache: false,
	       data: { 
	    	   action: 'condense_tables'
	       }
	       
	   })
	   .done(function(html) {    		   
		   $( "#condense_tables_result" ).val( "DONE" );  
	   });
});






});
