<?php 
    
    require_once (ABSPATH . "wp-admin/admin.php");
    
    ini_set('max_execution_time', 60 * 60 * 2);
	
	global $wpdb;
    
    $results = $wpdb -> get_results("SELECT * FROM imported_hotels WHERE Hotel_Id < 1000 AND Hotel_Id > 0");

    error_reporting(E_ALL);
    
    if(SUBDOMAIN_INSTALL) {
    	foreach ($results as $h) {
    	    
			$domain = (str_replace(" ", "", $h -> Hotel_Name)) . "." . DOMAIN_CURRENT_SITE;
		    $path = PATH_CURRENT_SITE;
		
		    echo "<br>Importing hotel " . $h -> Hotel_Name . " with domain " . $domain . " and path " . $path;
                			
		
    		//Create new blog
    		$id = wpmu_create_blog($domain, 
    				               $path, $h -> Hotel_Name, 1, array( 'public' => 1 ), 1);
    		$sql = "UPDATE wp_blogs SET cityId = '48635', hotel_import_id=" . $h -> Hotel_Id . " WHERE blog_id=" . $id;
    		echo "SQL for updating the foreign key is: " . $sql;
    	
    		//Set field for connecting rooms to original import ds
    		$wpdb -> get_results( $sql );
    	
    	
    	}	
    } else {
    
	    foreach ($results as $h) {
	    	
	    	
	    	//Create new blog
	    	$id = wpmu_create_blog($_SERVER["SERVER_NAME"], 
            (str_replace("/", "", get_current_site() -> path) == "" ? "" : "/") . str_replace("/", "", get_current_site() -> path) . "/" . str_replace(" ", "", $h -> Hotel_Name), $h -> Hotel_Name, 1, array( 'public' => 1 ), 1);    	
	    	$sql = "UPDATE wp_blogs SET cityId = '48635', hotel_import_id=" . $h -> Hotel_Id . " WHERE blog_id=" . $id;    	
	    	echo "SQL for updating the foreign key is: " . $sql;
	    	
	    	//Set field for connecting rooms to original import ds
	    	$wpdb -> get_results( $sql );      
	    			
	    	
	    }
    
    }
    
?>