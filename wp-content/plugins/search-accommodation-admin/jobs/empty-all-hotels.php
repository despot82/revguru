<?php
    
    require_once (ABSPATH . "wp-admin/admin.php"); 
    
    global $wpdb;
    
    ini_set('max_execution_time', 60 * 60);
        
    foreach($wpdb -> get_results("SELECT blog_id from wp_blogs WHERE blog_id >= 0") as $blog ) {
    	echo "Deleting all posts from blog: " . $blog -> blog_id . "<br/>";
    	switch_to_blog($blog -> blog_id);
    	
    	//Delete all posts
    	$wpdb -> get_results("DELETE FROM " . $wpdb -> prefix . "reservations WHERE room in(SELECT ID FROM " . $wpdb -> prefix . "posts WHERE post_type='easy-rooms')");
    	$wpdb -> get_results("DELETE FROM " . $wpdb -> prefix . "postmeta WHERE post_id in (SELECT ID FROM " . $wpdb -> prefix . "posts WHERE post_type='easy-rooms')");
    	$wpdb -> get_results("DELETE FROM " . $wpdb -> prefix . "posts WHERE post_type='easy-rooms'");
    	
    	
    	$wpdb -> get_results("ALTER TABLE " . $wpdb -> prefix . "posts AUTO_INCREMENT = 1");
		$wpdb -> get_results("ALTER TABLE " . $wpdb -> prefix . "postmeta AUTO_INCREMENT = 1");
		$wpdb -> get_results("ALTER TABLE " . $wpdb -> prefix . "reservations AUTO_INCREMENT = 1");
    	
    	
    } 
    
   
	
?>