<?php 
    
    require_once (ABSPATH . "wp-admin/admin.php");
    
    require_once ABSPATH . "wp-content/plugins/easyreservations/rest/ServiceClasses/er-inbound-resource-api.php";
    
    ini_set('max_execution_time', 60 * 60 * 2);
     
    global $wpdb;
    
    //Fill hotels with rooms    
    $results = $wpdb -> get_results( "SELECT ir.*, blog_id " 
    		                        ."FROM imported_rooms ir "
    		                        ."JOIN wp_blogs b ON(ir.Hotel_Id = b.hotel_import_id) "
    		 		                ."WHERE ir.Hotel_Id > 0 "
    		                        ."ORDER BY Hotel_Id ASC, Room_Id ASC");
    
    $api = new ER_Inbound_Resource_API();
    
    
    switch_to_blog(2);    
    $current_hotel = 2;
    
    foreach ($results as $r) {
    	
    	print "<pre>"; print_r($r); print "</pre>";
    	
    	echo "<br/> Importing room: " . $r -> Room_Id . " , which is type: " . $r -> Room_Type_Id . " from hotel with import id: " . $r -> Hotel_Id . " and " . $r -> blog_id;
    	
    	$hotel_id = $r -> blog_id;
    	$bedding_id = $r -> Bedding_ID;
    	$official_rack_rate = $r -> Official_Rack_Rate;
    	$min_rate = $r -> Min_Rate;
    	$no_of_rooms = $r -> No_Of_Rooms;
    	$is_base_room = $r -> IsBaseRoom;
    	$min_adults = $r -> Min_Adults;
    	$max_adults = $r -> Max_Adults;
    	$min_pax = $r -> Min_Pax;
    	$max_pax = $r -> Max_Pax;
    	
    	if ( $hotel_id <> $current_hotel) { 
    		echo "<br>****************** LOADING ROOMS FOR HOTEL " . $hotel_id . " ******************<br>";
    	    switch_to_blog($hotel_id);
    	    $current_hotel = $hotel_id;
    	}
    	
    	$api -> addResource($bedding_id, $bedding_id, 1, $official_rack_rate, $official_rack_rate - $min_rate, $no_of_rooms,
    			            $is_base_room, $min_adults, $max_adults, $min_pax, $max_pax);
    	
    	
    	
    }
    
?>

