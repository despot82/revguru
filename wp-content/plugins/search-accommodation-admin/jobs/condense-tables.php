<?php

    //This file serves to condense the individual posts, postmeta and reservations tables into one, for each (post, postmeta and reservations).
    //This way, there is only one SQL query executed to find hotels that can accomodate the search criteria   
    
    require_once (ABSPATH . "wp-admin/admin.php");
	
	global $wpdb;
	
	$originalMaxExecutionTime = ini_get('max_execution_time');
	
	ini_set('max_execution_time', 60 * 60 * 3);
	
	$blogIds = $wpdb -> get_results("SELECT blog_id FROM wp_blogs WHERE blog_id > 2");
	
	
	/*
	//Condense hotels tables (AS WELL) into one - finish this part in case of slowness ************************************	
	$blog_list = get_blog_list( 0, 'all' );
	foreach ($blog_list AS $blog) {
		echo '<br>Blog '.$blog['blog_id'].': '.$blog['domain'].$blog['path'].' ';
		echo "<br>" . get_blog_option($blog['blog_id'], 'blogname');
	}
	
	return;
	
	$wpdb -> query("DROP TABLE IF EXISTS all_hotels;");
	$wpdb -> query("CREATE TABLE all_hotels AS SELECT 1 AS hotelId, r.* FROM wp_2_postmeta r;");
	$wpdb -> query("DELETE FROM all_hotels;");
	
	$condense_postmeta_query = "INSERT INTO all_postmeta SELECT 2, p2.* FROM wp_2_postmeta AS p2 ";
	
	foreach ($blogIds as $id) {
		$id = $id -> blog_id;
		$condense_postmeta_query .= " UNION SELECT " . $id . ", p" . $id . ".* FROM wp_" . $id . "_postmeta AS p" . $id;
	}
	
	//error_log ("postmeta query: " . $condense_postmeta_query);
	
	$wpdb -> query ($condense_postmeta_query);
	
	*/
	
	//Condense postmeta tables into one ****************************************************
	$wpdb -> query("DROP TABLE IF EXISTS all_postmeta;");	
	$wpdb -> query("CREATE TABLE all_postmeta AS SELECT 1 AS hotelId, r.* FROM wp_2_postmeta r;");	
	$wpdb -> query("DELETE FROM all_postmeta;");
		
	$condense_postmeta_query = "INSERT INTO all_postmeta SELECT 2, p2.* FROM wp_2_postmeta AS p2 ";
    		
    foreach ($blogIds as $id) {
    	$id = $id -> blog_id;
    	$condense_postmeta_query .= " UNION SELECT " . $id . ", p" . $id . ".* FROM wp_" . $id . "_postmeta AS p" . $id;    	
    }

    error_log ("postmeta query: " . $condense_postmeta_query);
    
	$wpdb -> query ($condense_postmeta_query);
	
	
	//Condense posts tables into one *********************************************************
	$wpdb -> query("DROP TABLE IF EXISTS all_posts;");	
	$wpdb -> query("CREATE TABLE all_posts AS SELECT 1 AS hotelId, r.* FROM wp_2_posts r;");	
	$wpdb -> query("DELETE FROM all_posts;");
	
	$condense_posts_query = "INSERT INTO all_posts select 2, p2.* from wp_2_posts as p2 ";
			
    foreach ($blogIds as $id) {
    	$id = $id -> blog_id;
    	$condense_posts_query .= " UNION SELECT " . $id . ", p" . $id . ".* FROM wp_" . $id . "_posts AS p" . $id;    	
    }					
    
    error_log ("posts query: " . $condense_posts_query);
    
	$wpdb -> query ($condense_posts_query);
	
	
	//Condense reservations tables into one **********************************************************	
	$wpdb -> query("DROP TABLE IF EXISTS all_reservations;");	
	$wpdb -> query("CREATE TABLE all_reservations AS SELECT 1 AS hotelId, r.* FROM wp_2_reservations r;");	
	$wpdb -> query("DELETE FROM all_reservations;");
	
	$condense_reservations_query = "INSERT INTO all_reservations SELECT 2, p2.* FROM wp_2_reservations AS p2 ";
			
    foreach ($blogIds as $id) {
    	$id = $id -> blog_id;
    	$condense_reservations_query .= " UNION SELECT " . $id . ", p" . $id . ".* FROM wp_" . $id . "_reservations AS p" . $id;    	
    }					
    
    error_log ("reservations query: " . $condense_reservations_query);
    
	$wpdb -> query ($condense_reservations_query);
	
	ini_set('max_execution_time', $originalMaxExecutionTime);
    
    

?>