<?php
	
    require_once (ABSPATH . "wp-admin/admin.php");
    
    global $wpdb;
        
    ini_set('max_execution_time', 60 * 60);
    
    foreach($wpdb -> get_results("SELECT blog_id from wp_blogs WHERE blog_id >= 2") as $blog ) {
    	echo "Activating ER on blog: " . $blog -> blog_id . "<br/>";
    	switch_to_blog($blog -> blog_id);
    	activate_plugin( "easyreservations/easyReservations.php" );
    	
    } 
    
   
	
?>