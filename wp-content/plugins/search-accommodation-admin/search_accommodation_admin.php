<?php
/*
 Plugin Name: Search accommodation admin 
 Description: Maintain all aspects of search page, importing into Elasticsearch, importing into Wordpress MS and condensing the MySQL tables into one
 Version: 1.0
 Author: Vladimir Despotovic
 Author URI: http://despotovicvladimir.com
 */

add_action('admin_menu', 'searchaccommodationadmin_admin_actions');


function searchaccommodationadmin_admin_actions() {
	add_options_page("SearchAccommodationAdmin", "Search Accommodation Admin", "manage_options", __FILE__, "searchaccommodationadmin_admin");
}

function searchaccommodationadmin_admin() {
	include "form.php";	
	
}


/* SAA ajax-admin actions for elasticsearch buttons */

wp_enqueue_script("saa-scripts", plugin_dir_url(__FILE__) . "/js/scripts.js");

wp_localize_script("saa-scripts", "ajaxurl", admin_url( "admin-ajax.php"));

/* SAA ElasticSearch indexers */

add_action( 'wp_ajax_es_index_hotels', 'es_index_hotels_callback' );
function es_index_hotels_callback()
{
    include ABSPATH . "elasticsearch/import/indexers/index-hotels.php";
	wp_die();
}

add_action( 'wp_ajax_es_index_cities', 'es_index_cities_callback' );
function es_index_cities_callback()
{
	include ABSPATH . "elasticsearch/import/indexers/index-cities.php";
	wp_die();
}

add_action( 'wp_ajax_es_index_states', 'es_index_states_callback' );
function es_index_states_callback()
{
	include ABSPATH . "elasticsearch/import/indexers/index-states.php";
	wp_die();
}

add_action( 'wp_ajax_es_index_regions', 'es_index_regions_callback' );
function es_index_regions_callback()
{
	include ABSPATH . "elasticsearch/import/indexers/index-regions.php";
	wp_die();
}

add_action( 'wp_ajax_es_index_countries', 'es_index_countries_callback' );
function es_index_countries_callback()
{
	include ABSPATH . "elasticsearch/import/indexers/index-countries.php";
	wp_die();
}

add_action( 'wp_ajax_es_index_points_of_interest', 'es_index_points_of_interest_callback' );
function es_index_points_of_interest_callback()
{
	include ABSPATH . "elasticsearch/import/indexers/index_points_of_interest.php";
	wp_die();
}

/* SAA ElasticSearch deleters */

add_action( 'wp_ajax_es_delete_hotels', 'es_delete_hotels_callback' );
function es_delete_hotels_callback()
{
	include ABSPATH . "elasticsearch/import/deleters/delete-hotels.php";
	wp_die();
}

add_action( 'wp_ajax_es_delete_cities', 'es_delete_cities_callback' );
function es_delete_cities_callback()
{
	include ABSPATH . "elasticsearch/import/deleters/delete-cities.php";
	wp_die();
}

add_action( 'wp_ajax_es_delete_states', 'es_delete_states_callback' );
function es_delete_states_callback()
{
	include ABSPATH . "elasticsearch/import/deleters/delete-states.php";
	wp_die();
}

add_action( 'wp_ajax_es_delete_regions', 'es_delete_regions_callback' );
function es_delete_regions_callback()
{
	include ABSPATH . "elasticsearch/import/deleters/delete-regions.php";
	wp_die();
}

add_action( 'wp_ajax_es_delete_countries', 'es_delete_countries_callback' );
function es_delete_countries_callback()
{
	include ABSPATH . "elasticsearch/import/deleters/delete-countries.php";
	wp_die();
}

add_action( 'wp_ajax_es_delete_pointsofinterest', 'es_delete_pointsofinterest_callback' );
function es_delete_pointsofinterest_callback()
{
	include ABSPATH . "elasticsearch/import/deleters/delete-pointsofinterest.php";
	wp_die();
}

/* SAA Hotels maintenance */

add_action( 'wp_ajax_delete_all_blogs', 'delete_all_blogs_callback' );
function delete_all_blogs_callback()
{
	include ABSPATH . "wp-content/plugins/search-accommodation-admin/jobs/delete-all-hotels.php";
	wp_die();
}

add_action('wp_ajax_import_hotels', 'import_hotels_callback');
function import_hotels_callback()
{
	include ABSPATH . "wp-content/plugins/search-accommodation-admin/jobs/import-hotels-from-table.php";
	wp_die();
}

add_action( 'wp_ajax_activate_er_on_all_hotels', 'activate_er_on_all_hotels_callback' );
function activate_er_on_all_hotels_callback()
{
	include ABSPATH . "wp-content/plugins/search-accommodation-admin/jobs/activate-er-on-all-hotels.php";
	wp_die();
}

add_action( 'wp_ajax_empty_all_hotels', 'empty_all_hotels_callback' );
function empty_all_hotels_callback()
{
	include ABSPATH . "wp-content/plugins/search-accommodation-admin/jobs/empty-all-hotels.php";
	wp_die();
}

add_action('wp_ajax_import_rooms', 'import_rooms_callback');
function import_rooms_callback()
{
	include ABSPATH . "wp-content/plugins/search-accommodation-admin/jobs/import-rooms-from-table.php";
	wp_die();
}

add_action('wp_ajax_condense_tables', 'condense_tables_callback');
function condense_tables_callback()
{
	include ABSPATH . "wp-content/plugins/search-accommodation-admin/jobs/condense-tables.php";
	wp_die();
}



?>