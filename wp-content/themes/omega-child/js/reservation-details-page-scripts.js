function quantity_changed( room, persons, board_code, element, initial) {
	var sum = 0;
	
	//Set maximum selectable quantities for all of this room's rates
    $.each($(".room_" + room + "_quantity"), function(i, sel){		
		sum += +sel.value;		
	}); 
    
	$.each($(".room_" + room + "_quantity"), function(i, sel){
	    set_max_selectable_quantity(sel, localStorage.getItem("availability_" + room) - sum + +sel.value );
	}); 
	
	if(initial) return; //If this is the first time values are set (page load), return
	else { //Update current value of subtotal and total
		
		var price_whole_stay = $(element).parent().parent().children(".room_price_whole_stay").data("price");
		
		var quantity = element.options[element.selectedIndex].text;
		
		var subtotal = price_whole_stay * quantity;
		
		$(element).parent().parent().children(".subtotal").data("price", subtotal);
		
		var total = 0;
		
		$.each($(".subtotal"), function(i, sel){
			total += $(sel).data("price");
		}); 
		
		$.ajax({  
 	       url: ajaxurl, 
 	       type: "POST",    	   
 	       cache: false,
 	       data: { 
 	    	   action: 'money_format',
 	    	   amount: subtotal,
 	    	   total: total,
 	       	   current_blog_id: current_blog_id
      	   }
 	       
        })
        .done(function( html ) {
     	    //alert(html);
     	    
     	    subtotal_money = html.split(";")[0];
     	    total_money = html.split(";")[1];
     	    
     	    //alert("subtotal is: " + subtotal_money);
     	    //alert("total is: " + total_money);
     	    
     	    $(element).parent().parent().children(".subtotal").text(subtotal_money);
     	    $(".totals-row-sum-field").text(total_money);
     	   
	    });
	    
	}
	
}

//For the select element, disable options bigger than max
function set_max_selectable_quantity(select, max)
{
    $.each($(select).children(), function(i, option){
  	
	    if (option.value > max) {
	        $(option).prop('disabled', true);
	    } else {
	        $(option).prop('disabled', false);  	
	    }  		
    })
}

function save_availability_for_room(room, availability)
{	
	localStorage.setItem("availability_" + room, availability);
}

/* 
 * parameter date - in format dd/mm/yyyy
 * 
 */
function set_arrival_field(date)
{ 
	
	
	//alert("in set arrival field and setting it up for date: " + date);
	var parsedDate = $.datepicker.parseDate('dd/mm/yy', date);	
	$("#arrival_display").datepicker({
								minDate: 0,
						        dateFormat: 'dd/mm/yy',							    
		                        disabled: true  
		                     });	
	
	$("#arrival_display").datepicker("setDate", parsedDate);
 
}

/* 
 * parameter date - in format dd/mm/yyyy
 * 
 */
function set_departure_field(date)
{ 
	//alert("in set arrival field and setting it up for date: " + date);
	var parsedDate = $.datepicker.parseDate('dd/mm/yy', date);	
	$("#departure_display").datepicker({
								minDate: 0,
						        dateFormat: 'dd/mm/yy',							    
		                        disabled: true  
		                     });	
	
	$("#departure_display").datepicker("setDate", parsedDate);
 
}





