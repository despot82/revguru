$(document).ready(function () {

    //alert("DEV: validation js file is being read in");

    $("#optimal_roomset_form").submit(function(e){ 
    	
    	var validates_non_empty = false;
    	
    	$.each($("#optimal_roomset_form").find("select"), function(i, sel){
			if (sel.value != 0) {
				validates_non_empty = true;
			}
			
		}); 
    	
    	if(!validates_non_empty) {
    		alert("You haven't selected any rooms. Please select some rooms and try again.");
    		e.preventDefault();
    	}
    	
    	return;
    });
    
    $("#all_rooms_form").submit(function(e){ 
    	
    	var validates_non_empty = false;
    	
    	$.each($("#all_rooms_form").find("select"), function(i, sel){
    		
			if (sel.value != 0) {
				validates_non_empty = true;
			}
			
		}); 
    	
    	if(!validates_non_empty) {
    		alert("You haven't selected any rooms. Please select some rooms and try again.");
    		e.preventDefault();
    	}
    	
    	return;
    });
    
});    
