function optimum_roomset_form_quantity_changed(room) {
	
	var sum = 0;
	
    $.each($(".optimum_form_room_" + room + "_quantity"), function(i, sel){		
		sum += +sel.value;		
	}); 
    
	$.each($(".optimum_form_room_" + room + "_quantity"), function(i, sel){
	    set_max_selectable_quantity(sel, localStorage.getItem("availability_" + room) - sum + +sel.value );
	}); 
}


function all_rooms_form_quantity_changed(room) {
	//alert($("td").css("display"));
	var sum = 0;
	
    $.each($(".all_rooms_form_room_" + room + "_quantity"), function(i, sel){		
		sum += +sel.value;		
	}); 
    
	$.each($(".all_rooms_form_room_" + room + "_quantity"), function(i, sel){
	    set_max_selectable_quantity(sel, localStorage.getItem("availability_" + room) - sum + +sel.value );
	}); 
}


//For the select element, disable options bigger than max
function set_max_selectable_quantity(select, max)
{
    $.each($(select).children(), function(i, option){
  	
	    if (option.value > max) {
	        $(option).prop('disabled', true);
	    } else {
	        $(option).prop('disabled', false);  	
	    }  		
    })
}


function save_availability_for_room(room, availability)
{	
	localStorage.setItem("availability_" + room, availability);
}


//Parameter date in format 01/01/2015
function set_arrival_field(date, disabled)
{ 
	//alert("in set arrival field and setting it up for date: " + date);
	var parsedDate = $.datepicker.parseDate('dd/mm/yy', date);
	$('#arrival_recap').datepicker({ 
		                             dateFormat: 'dd/mm/yy', 
		                             disabled: disabled, 
		                             minDate: 0,
		                             onSelect: function(d) {
			    	                        var newdate = new Date($("#arrival_recap").datepicker("getDate"));
			    	                        newdate.setDate(newdate.getDate() + 1);
			    	                        $("#departure_recap").datepicker("setDate",newdate);	    	                        
			    	                 }
			    	               });	
	
	$('#arrival_recap').datepicker('setDate', parsedDate);
 
}

//Parameter date in format 01/01/2015
function set_departure_field(date, disabled)
{ 
	//alert("in set departure field and setting it up for date: " + date);
	var parsedDate = $.datepicker.parseDate('dd/mm/yy', date);
	$('#departure_recap').datepicker({ dateFormat: 'dd/mm/yy', 
		                                 disabled: disabled, 
		                                 minDate: 0,
		                                 onSelect: function(dateText) {
			                            	    //alert("Selected date: " + dateText + "; input's current value: " + this.value);
			                            	  }});
	
	$('#departure_recap').datepicker('setDate', parsedDate);
 
}


//This function prepopulates the rooms fields
//people parameter is a string with adequate format ie. 1,0;2,0;2,1;1,1;1,1
function set_rooms_fields(people)
{ 
	//alert(people);
	
	rooms = people.split(";");
	
	form = $("#search_form_recap");
	                                    
	//Remove the default displayed room and start over - add the rooms
	$("div.room").remove();     
	
	//first add bare rooms
	if (rooms.length <= 1) {
		i = 0;
		form.append("<div class='room'><label class='room-label'>" + (i + 1) + ".</label> &nbsp; <label>Adults </label> <select name='adults[]' class='form-control adults-number-field'>		<option value=1>1</option>		<option value=2>2</option>		<option value=3>3</option>		<option value=4>4</option>		<option value=5>5</option>		<option value=6>6</option>		<option value=7>7</option>		<option value=8>8</option>		<option value=9>9</option>		<option value=10>10</option>				</select>								<label>Children </label>				<select name='childs[]' class='form-control children-number-field'>		<option value=0>0</option>			<option value=1>1</option>		<option value=2>2</option>		<option value=3>3</option>					<option value=4>4</option>					<option value=5>5</option>					<option value=6>6</option>					<option value=7>7</option>					<option value=8>8</option>					<option value=9>9</option>					<option value=10>10</option>				</select> </div>");
	} else {	
	    for(i = 0; i < rooms.length; i++ ) {
		    form.append("<div class='room'><label class='room-label'>" + (i + 1) + ".</label> &nbsp; <label>Adults </label> <select name='adults[]' class='form-control adults-number-field'>		<option value=1>1</option>		<option value=2>2</option>		<option value=3>3</option>		<option value=4>4</option>		<option value=5>5</option>		<option value=6>6</option>		<option value=7>7</option>		<option value=8>8</option>		<option value=9>9</option>		<option value=10>10</option>				</select>								<label>Children </label>				<select name='childs[]' class='form-control children-number-field'>		<option value=0>0</option>			<option value=1>1</option>		<option value=2>2</option>		<option value=3>3</option>					<option value=4>4</option>					<option value=5>5</option>					<option value=6>6</option>					<option value=7>7</option>					<option value=8>8</option>					<option value=9>9</option>					<option value=10>10</option>				</select> <input class='btn btn-default remove-group-button' type='button' value=' - ' /></div>");
	    }
	}
	
	html_rooms = form.children(".room");
	
	//Go through each one, one by one and setup the adults and childs for each
	for(i = 0; i < html_rooms.length; i++) {
		
		html_room = html_rooms[i];
		
	    adults_childs = rooms[i].split(",");
	    
	    adults_number_selector = "select.adults-number-field option[value='" + adults_childs[0] + "']";
	    children_number_selector = "select.children-number-field option[value='" + adults_childs[1] + "']";
	    
	    $(html_room).find(adults_number_selector).attr("selected",true);
	    $(html_room).find(children_number_selector).attr("selected",true);
	   
	    
	}
	   
}


