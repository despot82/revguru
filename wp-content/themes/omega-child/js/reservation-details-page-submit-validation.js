function validateEmail($email) {
	  var emailReg = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
	  return emailReg.test( $email );
}

$(document).ready(function () {
    
	//Validate that there is at least one room selected in quantity >0
	$("#rooms_form").submit(function(e){ 
		
		var validates_quantities = false;
		
		$.each($("#rooms_form").find("select.quantity"), function(i, sel){
			
			if (sel.value != 0) {
				validates_quantities = true;
			}
			
		}); 
		
		if(!validates_quantities) {
			alert("There are no selected rooms. Please select some room(s) and try again.");
			e.stopPropagation();
			e.preventDefault();
			stopImmediatePropagation();
			return false;
		}
		
        var emailaddress =  $("#easy-form-email").val();
		
		if(emailaddress == '') {
		    alert("The email address is empty. Please enter your email address and try again.");
            e.stopPropagation();
			e.preventDefault();
			stopImmediatePropagation();
			return false;
		}
		
        if(!validateEmail(emailaddress)) {
        	
        	alert("Your email address doesn't look like it's correct. Please correct it and try again.");
            e.stopPropagation();
			e.preventDefault();
			stopImmediatePropagation();
			return false;
		} 
        
		return;
	});

});