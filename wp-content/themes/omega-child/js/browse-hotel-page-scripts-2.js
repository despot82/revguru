$(document).ready(function () {
	
	//alert("DEV: inside the document ready of browse-hotel-page-scripts-2");
	
    $("#add_more_groups").click(function(){ 
    	
    	form = $("#search_form_recap");
    	howmanyrooms = form.children(".room").size();
    	if(howmanyrooms == 1) {  
    		//If there is only one room, then there is no remove button next to it yet, so we need to add it        	
        	$(".room").append("<input class='remove-group-button btn' type='button' value=' - ' />");
        }
    	
        form.append("<div class='room'><label class='room-label'>" + (howmanyrooms+1) + ". </label> &nbsp; <label>Adults </label> <select name='adults[]' class='form-control adults-number-field'>					<option>1</option>					<option>2</option>					<option>3</option>					<option>4</option>					<option>5</option>					<option>6</option>					<option>7</option>					<option>8</option>					<option>9</option>					<option>10</option>				</select>								<label>Children </label>				<select name='childs[]' class='form-control children-number-field'>		<option>0</option>			<option>1</option>					<option>2</option>					<option>3</option>					<option>4</option>					<option>5</option>					<option>6</option>					<option>7</option>					<option>8</option>					<option>9</option>					<option>10</option>				</select> <input class='remove-group-button btn' type='button' value=' - ' /></div>");
    	
                    
    });
    
    $("#search_form_recap").on('click', '.room .remove-group-button', function() {
    	
    	form = $(this).parent().parent();
    	
    	if(form.children(".room").size() > 1) {
    		
    		$(this).parent().remove();    		
    		if(form.children(".room").size() == 1) {    	
    			$(".remove-group-button").remove();
    		}        		
    	}
    	
        ordinal = 1;
    	
    	$(".room-label").each(function(){
    		$(this).text(ordinal + ".");
    		ordinal ++ ;
    	});
    	
    }); 
    
    
    //Setup search button in the recap form
    $("#search_button").click(function(){ 
    	
    	people = "";
    	
    	$.each($(".room"), function(i, e){		
    		
    		adults = $(this).children(".adults-number-field").val();
    		children = $(this).children(".children-number-field").val();
    		
    		//alert("Adults:" + adults);
    		//alert("Children:" + children);
    		
    		people += adults + ",";
    		people += children + ";";
    		
    	}); 
    	
    	people = people.substr(0, people.length-1);    	
    	
    	hotel = $('#hotel').val();
    	arrival = $('#arrival_recap').val();
    	departure = $('#departure_recap').val();
    	
    	qs = "hotel=" + hotel + "&arrival=" + arrival + "&departure=" + departure + "&people=" + people;
    	
        $.ajax({
        	    url: "../browse-hotel-roomsets",	        	   
        	    type: "GET",		    					        		      
		        data: qs,					
		        success: function(result) 
		        {   						     
		        	$('#roomsets').html(result);
		        } 			         			         
        }); 
    	
    	            
    });  

    

});