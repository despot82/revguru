//Search button in the mini recap form
$("#search_button_ajax").click(function(){
    	
    	//alert("search button clicked");
	    var adults = Array();
	    var children = Array();
    	
    	$.each($(".room"), function(i, e){		
    		
    		a = $(this).children(".adults-number-field").val();
    		c = $(this).children(".children-number-field").val();
    		
    		//alert("Adults:" + adults);
    		//alert("Children:" + children);
    		
    		adults.push(a);
    		children.push(c);
    		
    	});    	
    	  	
    	
    	destination = $('#destination').val();
    	destination_type = $('#destination_type').val();
    	destination_id = $('#destination_id').val();
    	arrival = $('#arrival').val();
    	departure = $('#departure').val();
    
    	
        $.ajax({
        	    url: "../search-results-hotels",
		        type: "GET",
		        data: { destination: destination,
		        	    destination_type: destination_type,
		        	    destination_id: destination_id,
		        	    arrival: arrival,
		        	    departure: departure,
		        	    adults: adults,
		        	    childs: children},
		        success: function(result) 
		        {   						     
		        	$('#content').html(result);
		        } 			         			         
            }); 
    	
    	
    	//alert("arrival:");
    	//alert("departure:");
});

