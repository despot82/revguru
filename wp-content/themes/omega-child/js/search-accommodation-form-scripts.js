$(function() 
{ 
	$( "#destination" ).autocomplete(
	{ 
		source: 
		function( request, response ) 
		{ 			
			$.ajax({ url: site_root + "/elasticsearch/client/suggest_with_sections.php",				
				     type: "POST",
				     dataType:'json',					        		      
				     data: { "text" : request.term },					
				     success: function( d ) 
				     {   						     
					     response( $.map( d, function( item )					      
								 					   { 							     			 
								 					       return { 
								 					   	              label: item.highlight.geo_name[0], 
								 					   	    	      value: item._source.geo_name, 
								 					   	    	      type: item._type, 
								 					   	    	      id: item._id 
								 					   	    	  }
								 					   	     
								 					   })
						         ); 
					 } 			         			         
		    }); 
		},		
		minLength: 1, 
		select: function( event, ui ) { 
			event.preventDefault(); 
			$("#destination").val(ui.item.value);
		    $("#destination_id").val(ui.item.id); 
		    $("#destination_type").val(ui.item.type);
		    
		},
		open: function() { $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" ); }, 
		close: function() { $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" ); } 
	})
	.autocomplete("instance")._renderItem = function( ul, item ) {
		
        var hotelImagePath = "<img src='" + theme_root + "/images/hotel_icon_transparent.png'/>" ;       
        var cityImagePath = "<img src='" + theme_root + "/images/city_icon_transparent.png'/>" ;
        var regionImagePath = "<img src='" + theme_root + "/images/region_icon_transparent.png'/>" ;
        var stateImagePath = "<img src='" + theme_root + "/images/state_icon_transparent.png'/>" ;
        var countryImagePath = "<img src='" + theme_root + "/images/country_icon_transparent.png'/>" ;
        var pointOfInterestImagePath = "<img src='" + theme_root + "/images/pointofinterest_icon_transparent.png'/>" ;

        var iconImage;
        
        switch(item.type) {
        	case "hotel" : iconImage = hotelImagePath; break;
        	case "city" : iconImage = cityImagePath; break;
        	case "state" : iconImage = stateImagePath; break;
        	case "region" : iconImage = regionImagePath; break;
        	case "country" : iconImage = countryImagePath; break;
        	case "pointofinterest" : iconImage = pointOfInterestImagePath; break;
        }
  
        return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append(  iconImage +  item.label  )
        .appendTo( ul );
    };
});
	


$(document).ready(function () {
		
    $("#add_more_groups").click(function(){ 
    	
    	form = $("#search_form_inner");
    	howmanyrooms = form.children(".room").size();
    	if(howmanyrooms == 1) {  //If there is only one room, then there is no remove button next to it yet.        	
        	$(".room").append("<input class='remove-group-button btn btn-default' type='button' value=' - ' />");
            
    	}
    	
        form.append("<div class='room margined'><label class='room-label'>" + (howmanyrooms+1) + ". </label><label>Adults </label> <select name='adults[]' class='form-control adults-number-field'>					<option>1</option>					<option>2</option>					<option>3</option>					<option>4</option>					<option>5</option>					<option>6</option>					<option>7</option>					<option>8</option>					<option>9</option>					<option>10</option>				</select>								<label>Children </label>				<select name='childs[]' class='form-control children-number-field'>		<option>0</option>			<option>1</option>					<option>2</option>					<option>3</option>					<option>4</option>					<option>5</option>					<option>6</option>					<option>7</option>					<option>8</option>					<option>9</option>					<option>10</option>				</select> <input class='btn btn-default remove-group-button' type='button' value=' - ' /></div>");
        
    });                                                                                                 
    
    $("#search_form_inner").on('click', '.room .remove-group-button', function() {
    	
    	form = $(this).parent().parent();
    	
    	if(form.children(".room").size() > 1) {
    		
    		$(this).parent().remove();    		
    		if(form.children(".room").size() == 1) {    	
    			$(".remove-group-button").remove();
    		}        		
        }
    	
    	ordinal = 1;
    	
    	$(".room-label").each(function(){
    		$(this).text(ordinal + ".");
    		ordinal ++ ;
    	});
    
    	
    	
    	
    });  
    
});   

//Parameter date in format 31/12/2015
function set_arrival_field(date, disabled)
{   
	//alert("in set arrival field and setting it up for date: " + date);
	var parsedDate = $.datepicker.parseDate('dd/mm/yy', date);	
	$("#arrival").datepicker({
								minDate: 0,
						        dateFormat: 'dd/mm/yy',
							    onSelect: function(d) {
							    	                        var newdate = new Date($("#arrival").datepicker("getDate"));
							    	                        newdate.setDate(newdate.getDate() + 1);
							    	                        $("#departure").datepicker("setDate",newdate);	    	                        
							    	                    }, 
		                        disabled: disabled  
		                     });	
	
	$("#arrival").datepicker("setDate", parsedDate);
 
}

//Parameter date in format 31/12/2015
function set_departure_field(date, disabled)
{ 
	//alert("in set departure field and setting it up for date: " + date);
	var parsedDate = $.datepicker.parseDate('dd/mm/yy', date);
	$("#departure").datepicker({ 
		                         disabled: disabled,
							     minDate: 0,
						         dateFormat: 'dd/mm/yy'  
	                           });
	
	$('#departure').datepicker('setDate', parsedDate);
 
}

//Prepopulate destination fields (name, type, id)
function set_destination_fields(destination_name, destination_type, destination_id)
{ 
	$("#destination").val(destination_name);
	$("#destination_type").val(destination_type);
	$("#destination_id").val(destination_id);
 
}

//This function prepopulates the rooms fields
//people parameter is a string with format like 1,0;2,0;2,1;1,1;1,1
function set_rooms_fields(people)
{ 
	rooms = people.split(";");
	
	form = $("#search_form_inner");
	                                    
	//Remove the first room that was meant for search page, and start over - add the rooms
	$("div.room.margined").remove();     
	
	//first add bare rooms
	for(i = 0; i < rooms.length; i++ ) {
	
		form.append("<div class='room margined'><label class='room-label'>" + (i + 1) + ".</label><label>Adults </label> <select name='adults[]' class='form-control adults-number-field'>		<option value=1>1</option>		<option value=2>2</option>		<option value=3>3</option>		<option value=4>4</option>		<option value=5>5</option>		<option value=6>6</option>		<option value=7>7</option>		<option value=8>8</option>		<option value=9>9</option>		<option value=10>10</option>				</select>								<label>Children </label>				<select name='childs[]' class='form-control children-number-field'>		<option value=0>0</option>			<option value=1>1</option>		<option value=2>2</option>		<option value=3>3</option>					<option value=4>4</option>					<option value=5>5</option>					<option value=6>6</option>					<option value=7>7</option>					<option value=8>8</option>					<option value=9>9</option>					<option value=10>10</option>				</select> <input class='btn btn-default remove-group-button' type='button' value=' - ' /></div>");
	}
	
	html_rooms = form.children(".room");
	
	//Remove the minus if only one room - it shouldn't be removable 
	if(html_rooms.length == 1) {
		$(".remove-group-button").remove();
	}
	
	//Go through each one, one by one and setup the adults and childs for each
	for(i = 0; i < html_rooms.length; i++) {
		
		html_room = html_rooms[i];
		
	    adults_childs = rooms[i].split(",");
	    
	    adults_number_selector = "select.adults-number-field option[value='" + adults_childs[0] + "']";
	    children_number_selector = "select.children-number-field option[value='" + adults_childs[1] + "']";
	    
	    $(html_room).find(adults_number_selector).attr("selected",true);
	    $(html_room).find(children_number_selector).attr("selected",true);
	   
	    
	}
	   
}


