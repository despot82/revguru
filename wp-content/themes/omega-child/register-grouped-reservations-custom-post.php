<?php

function register_grouped_reservation_post_type() {
	$labels = array(
			'name'               => _x( 'Grouped reservations', 'post type general name' ),
			'singular_name'      => _x( 'Grouped Reservation', 'post type singular name' ),
			'add_new'            => _x( 'Add New', 'book' ),
			'add_new_item'       => __( 'Add New Grouped Reservation' ),
			'edit_item'          => __( 'Edit Grouped Reservation' ),
			'new_item'           => __( 'New Grouped Reservation' ),
			'all_items'          => __( 'All Grouped Reservations' ),
			'view_item'          => __( 'View Grouped Reservation' ),
			'search_items'       => __( 'Search Grouped Reservations' ),
			'not_found'          => __( 'No grouped reservations found' ),
			'not_found_in_trash' => __( 'No grouped reservations found in the Trash' ),
			'parent_item_colon'  => '',
			//'menu_name'          => 'Grouped Reservations'
	);
	
	$args = array(
			'labels'              => $labels,			
			'description'         => __( 'Post Type Description', 'text_domain' ),
			'public'              => false,
			'menu_position'       => 5,
			'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
			'has_archive'         => true,
			
			/*
			'taxonomies'          => array( 'category', 'post_tag' ),
			'hierarchical'        => false,			
			'show_ui'             => true,
			'show_in_menu'        => true,			
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
			*/
	);
	
	register_post_type( 'grouped_reservation', $args );
}

add_action( 'init', 'register_grouped_reservation_post_type' );

?>