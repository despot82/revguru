<?php
/**
 * 
 * Template Name:ReservationDetails
 * 
 * The template for filling all remaining details for a reservation 
 *
 * @package omega-child
 */
?>
    <!DOCTYPE html>
    <!-- First get jquery -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">	    

<?php 
    wp_enqueue_script("reservation-details-page-scripts", get_stylesheet_directory_uri() . "/js/reservation-details-page-scripts.js");
    wp_enqueue_script("reservation-details-page-submit-validation", get_stylesheet_directory_uri() . "/js/reservation-details-page-submit-validation.js");
    
    //jCarousel
    wp_enqueue_script("jcarousel-init-scripts", get_stylesheet_directory_uri() . "/js/jquery.jcarousel.js");
    wp_enqueue_script("jcarousel-responsive-scripts", get_stylesheet_directory_uri() . "/js/jcarousel.responsive.js");
    
    wp_localize_script('reservation-details-page-scripts', 'WPURLS', array( 'siteurl' => get_option('siteurl') ));
    wp_localize_script('reservation-details-page-scripts', 'ajaxurl', admin_url( 'admin-ajax.php' ));
    wp_localize_script('reservation-details-page-scripts', 'current_blog_id', $_POST["hotel"]);
    
    get_header(); 
    
    
?>

	<main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
	
<?php 
		
	do_action( 'omega_before_content' ); 

	wp_enqueue_style("reservation-details-page-css", get_stylesheet_directory_uri() . "/css/reservation-details-page.css" );
	
	wp_enqueue_style("responsive-table-design", get_stylesheet_directory_uri() . "/css/responsive-design-table.css" );
	wp_enqueue_style("responsive-reservation-details-css", get_stylesheet_directory_uri() . "/css/responsive-reservation-details.css" );
	
	wp_enqueue_style("jcarousel-responsive-css", get_stylesheet_directory_uri() . "/css/jcarousel.responsive.css");
		
	include "pages/reservation-details-page-content.php";
		
	do_action( 'omega_after_content' );
		  
?>

	</main><!-- .content -->

<?php get_footer(); ?>
