<?php 

/**
 *   Search Page Ad Widget
 */
class Search_Page_Ad_Widget extends WP_Widget
{
	public function __construct()
	{                      // id_base        ,  visible name
		parent::__construct( 'search_page_ad_widget', 'Search Page Ad Widget' );
	}
	
	public function widget( $args, $instance )
	{
		echo $args['before_widget'], wpautop( $instance['text'] ), $args['after_widget'];
	}
	
	public function form( $instance )
	{	
				
		$text = isset ( $instance['text'] )
			? esc_textarea( $instance['text'] ) : '';
		printf(
			'<textarea class="widefat" rows="7" cols="20" id="%1$s" name="%2$s">%3$s</textarea>',
			$this->get_field_id( 'text' ),
			$this->get_field_name( 'text' ),
			$text
		);
		
	}
	
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function ERRORupdate( $new_instance, $old_instance ) {
	
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	
		return $instance;
	}
	
}

?>