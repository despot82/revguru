<?php 

/**
 *   Search Results Search Widget
 */
class Search_Results_Search_Widget extends WP_Widget
{
	public function __construct()
	{                      // id_base        ,  visible name
		parent::__construct( 'Search_Results_Search_Widget', 'Search Results Search Widget' );
	}
	
	public function widget( $args, $instance )
	{	
		
		require ABSPATH . "/wp-content/themes/omega-child/pages/page_elements/search-accommodation-form.php";
		
	}
	
	public function form( $instance )
	{	
				
		$text = isset ( $instance['text'] )
			? esc_textarea( $instance['text'] ) : '';
		printf(
			'<textarea class="widefat" rows="7" cols="20" id="%1$s" name="%2$s">%3$s</textarea>',
			$this -> get_field_id( 'text' ),
			$this -> get_field_name( 'text' ),
			$text
		);
		
	}
	
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function ERRORupdate( $new_instance, $old_instance ) {
	
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	
		return $instance;
	}
	
}

?>