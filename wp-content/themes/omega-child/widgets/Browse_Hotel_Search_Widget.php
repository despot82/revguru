<?php 

/**
 *   Browse Hotel Search Widget
 */
class Browse_Hotel_Search_Widget extends WP_Widget
{
	public function __construct()
	{                      // id_base        ,  visible name
		parent::__construct( 'browse_hotel_search_widget', 'Browse Hotel Search Widget' );
	}
	
	public function widget( $args, $instance )
	{
		
		$people = $_GET["people"];
		
?>   

	<div id='search_form_recap' class='clearfix'>
	
		<div class='form-group date-recap'>
			<label for='arrival_recap'>Arrival</label>
			<input id='arrival_recap' class='form-control' type='text' />
		</div>
			
		<div class='form-group date-recap'>
			<label for='departure_recap'>Departure</label>
			<input id='departure_recap' class='form-control' type='text' />
		</div>
	
	
		<div class="form-group">
		    <input id="destination_type" name="destination_type" type="hidden" />
		</div>
		
		<div class="form-group">
			<input id="destination_id" name="destination_id" type="hidden" />
		</div>
		
		<label>Rooms: </label> &nbsp; 
		<br>
		<div class="room">
		
		<label>Adults</label> 
			<select class="form-control adults-number-field" name="adults[]">				
				<option>1</option><option>2</option><option>3</option><option>4</option><option>5</option>
				<option>6</option><option>7</option><option>8</option><option>9</option><option>10</option>
			</select> 
			<label>Children</label> 
			<select class="form-control children-number-field" name="childs[]">
				<option>0</option><option>1</option><option>2</option><option>3</option><option>4</option>
				<option>5</option><option>6</option><option>7</option><option>8</option><option>9</option>
				<option>10</option>
			</select>
		</div>		
    </div>
		   
    <input id="add_more_groups" class="btn btn-default add-more-groups-button margined"
			   name="add_more_groups" type="button"
			   value="Add more rooms" />
	
	<button id="search_button" class="btn btn-default search-button"
			name="search_button" type="submit">Search in this hotel
	</button>
	
	<script type="text/javascript">
	
        set_arrival_field("<?php echo $_GET["arrival"]; ?>", false);
        set_departure_field("<?php echo $_GET["departure"]; ?>", false);

        set_rooms_fields("<?php echo $people; ?>");
        
    </script>

    <input id='hotel' type='hidden' value='<?php echo $_GET["hotel"];  ?>' />
    
	    
<?php 
		//Using this, omit displaying the admin created content in favor of outputting the above text
		//echo $args['before_widget'], wpautop( $instance['text'] ), $args['after_widget'];
	}
	
	public function form( $instance )
	{	
				
		$text = isset ( $instance['text'] )
			? esc_textarea( $instance['text'] ) : '';
		printf(
			'<textarea class="widefat" rows="7" cols="20" id="%1$s" name="%2$s">%3$s</textarea>',
			$this -> get_field_id( 'text' ),
			$this -> get_field_name( 'text' ),
			$text
		);
		
	}
	
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function ERRORupdate( $new_instance, $old_instance ) {
	
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
	
		return $instance;
	}
	
}

?>