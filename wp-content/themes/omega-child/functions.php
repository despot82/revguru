<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function theme_enqueue_styles() {
	
	$theme_dir = get_stylesheet_directory_uri();
	
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); 
        
    wp_enqueue_style('bootstrap-min-css', $theme_dir . '/css/bootstrap.min.css' );
    wp_enqueue_style('bootstrap-theme-min-css', $theme_dir . '/css/bootstrap-theme.min.css' );    
    wp_enqueue_script('bootstrapmin.js', $theme_dir . "/js/bootstrap.min.js");
    
    
}


include "register-widgets.php";

include "register-grouped-reservations-custom-post.php";



?>


