<?php
/**
 * 
 * Template Name:SearchResults 
 *
 * This is the template that displays the search results
 *
 * @package omega-child
 */
?>
    <!-- First get jquery needed for everything else -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">	
<?php 

    //Content is processed during get_header call, and calls scripts from search-accommodation-form-script.js, so this script
    //needs to be ready for the call to its functions
    wp_enqueue_script("search-accommodation-form-scripts", get_stylesheet_directory_uri() . "/js/search-accommodation-form-scripts.js");
    wp_localize_script('search-accommodation-form-scripts', 'site_root', get_option('siteurl'));
    wp_localize_script('search-accommodation-form-scripts', 'theme_root', get_stylesheet_directory_uri());
    
    get_header(); 
?>
    
	<main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
    
<?php 
		
		do_action( 'omega_before_content' ); 
		
		wp_enqueue_script("search-results-page-ajax-scripts.js", get_stylesheet_directory_uri() . "/js/search-results-page-scripts.js");
		
		wp_enqueue_style("search-results-page-specific-css", get_stylesheet_directory_uri() . "/css/search-results-page.css");
		
		include "pages/search-results-page-content.php";
		
		do_action( 'omega_after_content' );
		  
		?>

	</main><!-- .content -->

<?php get_footer(); ?>
