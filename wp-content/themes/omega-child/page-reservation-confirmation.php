<?php
/**
 * 
 * Template Name:ReservationConfirmation
 * 
 * The template for confirmation of the reservation 
 *
 * @package omega-child
 */
 ?>
    <!DOCTYPE html>
    <!-- First get jquery -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">	
    
<?php 
     
    //jCarousel
    wp_enqueue_script("jcarousel-init-scripts", get_stylesheet_directory_uri() . "/js/jquery.jcarousel.js");
    wp_enqueue_script("jcarousel-responsive-scripts", get_stylesheet_directory_uri() . "/js/jcarousel.responsive.js");
     
    get_header(); 
?>

	<main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>

<?php 
		
    do_action( 'omega_before_content' ); 
		
	wp_enqueue_style("reservation-confirmation-page-css", get_stylesheet_directory_uri() . "/css/reservation-confirmation-page.css" );
	
	wp_enqueue_style("responsive-design-table", get_stylesheet_directory_uri() . "/css/responsive-design-table.css" );
	
	wp_enqueue_style("jcarousel-responsive-css", get_stylesheet_directory_uri() . "/css/jcarousel.responsive.css");
	
	include "pages/reservation-confirmation-page-content.php";
		
	do_action( 'omega_after_content' );
		  
?>

	</main><!-- .content -->

<?php get_footer(); ?>
