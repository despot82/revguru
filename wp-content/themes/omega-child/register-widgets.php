<?php 

require_once "widgets/Search_Page_Ad_Widget.php";
require_once "widgets/Browse_Hotel_Search_Widget.php";
require_once "widgets/Search_Results_Search_Widget.php";

add_action( 'widgets_init', 'add_widget' );

function add_widget()
{
	// Register our own widget.
	register_widget( 'Search_Page_Ad_Widget' );
	register_widget( 'Search_Results_Search_Widget' );
	register_widget( 'Browse_Hotel_Search_Widget' );
	
}






?>