<?php
/**
 * 
 * Template Name:BrowseHotel
 * 
 * The template for displaying available accommodation in the selected hotel. 
 *
 * @package omega-child
 */ 
?>
    <!DOCTYPE html>
    <!-- First get jquery needed for everything else -->
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">	
    
<?php 

    //For this page, get_header calls scripts from browse-hotel-page-scripts.js, so this script
    //needs to be ready for the call to its functions
    wp_enqueue_script("browse-hotel-page-scripts.js", get_stylesheet_directory_uri() . "/js/browse-hotel-page-scripts.js");
    
    //jCarousel for the on screen carousel, not the popup one
    wp_enqueue_script("jcarousel-init-scripts", get_stylesheet_directory_uri() . "/js/jquery.jcarousel.js");
    wp_enqueue_script("jcarousel-responsive-scripts", get_stylesheet_directory_uri() . "/js/jcarousel.responsive.js");
    
    get_header();
        
?>
    <main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
  	
<?php 
	

    do_action( 'omega_before_content' ); 
		
	wp_enqueue_script("browse-hotel-page-scripts-2.js", get_stylesheet_directory_uri() . "/js/browse-hotel-page-scripts-2.js");
	wp_enqueue_script("browse-hotel-page-submit-validation", get_stylesheet_directory_uri() . "/js/browse-hotel-page-submit-validation.js");
	
	wp_enqueue_style("browse-hotel-page-css", get_stylesheet_directory_uri() . "/css/browse-hotel-page.css");
	wp_enqueue_style("responsive-design-table-css", get_stylesheet_directory_uri() . "/css/responsive-design-table.css" );
	wp_enqueue_style("responsive-browse-hotel-css", get_stylesheet_directory_uri() . "/css/responsive-browse-hotel.css" );
	wp_enqueue_style("jcarousel-responsive-css", get_stylesheet_directory_uri() . "/css/jcarousel.responsive.css");
	
	include "pages/browse-hotel-page-content.php";
		
	do_action( 'omega_after_content' );
	
		  
?>

	</main><!-- .content -->

<?php get_footer(); ?>
