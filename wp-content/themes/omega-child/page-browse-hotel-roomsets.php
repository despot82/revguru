<?php
/**
 * 
 * Template Name:BrowseHotelRoomsets
 * 
 * The template for the page element that displays only roomsets
 *
 * @package omega-child
 */ 
 
require ABSPATH . "wp-content/themes/omega-child/pages/page_elements/browse-hotel-page-roomsets.php";

?>
  
<script src="<?php echo $network_theme_url; ?>/js/browse-hotel-page-submit-validation.js"></script>

