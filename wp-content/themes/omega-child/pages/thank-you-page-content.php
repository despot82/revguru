<?php 
    
    require_once ABSPATH . "wp-content/plugins/easyreservations/rest/ServiceClasses/er-inbound-reservation-api.php";
    require_once ABSPATH . "wp-content/plugins/easyreservations/rest/ServiceClasses/er-inbound-resource-api.php";
    require_once ABSPATH . "braintree-php-3.2.0/lib/Braintree.php";
    
    global $wp;
    
    $reservation_api = new ER_Inbound_Reservation_API();
    
    $braintree_payment_option = get_option("braintree_payment_option");
    
    $network_theme_url = get_stylesheet_directory_uri();
    
    switch_to_blog($_POST["hotel"]);

    Braintree_Configuration::environment('sandbox');
    Braintree_Configuration::merchantId('9sk9j69284sm4c5h');
    Braintree_Configuration::publicKey('hsqzztygnpvpxk2p');
    Braintree_Configuration::privateKey('3ec5b86dc1dca25e6b1aac2c0907e711');
    
    $nonce = $_POST["payment_method_nonce"];
    $amount = $_POST["amount"];
    $grouped_reservation_id = $_POST["grouped_reservation_id"];
    
    //echo "DEV: nonce: " . $nonce;
    
    $result = Braintree_Transaction::sale([
    		'amount' => $amount,
    		'paymentMethodNonce' => $nonce
    ]);
    
    //print "DEV: result is: <pre>"; print_r($result); print "</pre>";
    
    if(!$result -> success ) {
    	$result_attributes = $result -> _attributes;
    	echo "<br>We are sorry, but there was an error processing your transaction, and the reservation couldn't be completed. Error message: " . $result_attributes["message"];
    	
    	$reservation_api -> set_grouped_reservations_with_failed_payment_status($grouped_reservation_id);
    	$gr = get_post($grouped_reservation_id);
    	$gr -> post_excerpt = $result_attributes["message"];
    	wp_update_post($gr);
    	
    	return;
    }
    
    echo "<br>Thank you! Your reservation(s) have been sucesfully made.";
    //echo "DEV: CHECKPOINT in thank you page.";	
    $reservation_api -> approve_grouped_reservations($grouped_reservation_id);   
   
    $reservation_api -> set_grouped_reservations_as_paid($grouped_reservation_id, $braintree_payment_option);     
     
    $gr = get_post($grouped_reservation_id);
    $gr -> post_excerpt = "Paid";
    wp_update_post($gr);
    
    //Send the confirmation SMS
    $url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
           'api_key' => "4b8ac12f",
           'api_secret' => "e11f4a27",
           'to' => "381669292164",
           'from' => "447958560876",
           'text' => "This is a confirmation SMS of grouped reservation " . $grouped_reservation_id . " for the hotel: " . get_blog_option($_POST["hotel"], 'blogname')
    ]);
        
    $ch = curl_init($url);
        
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      
    $response = curl_exec($ch);
        
    //print "DEV: response is: <pre>"; print_r($response); print "</pre>";
        
    if(curl_errno($ch)){
     	echo 'Curl error: ' . curl_error($ch);
    }
    
?>

<!-- Reservation confirmation page content:  -->

<?php

        
    //Display the hotel metadata
    if(!empty(get_option("reservations_custom_fields")["fields"])) {
    	foreach(get_option("reservations_custom_fields")["fields"] as $option) {
    		
    	    if($option["title"] == "star_rating") {
    		    $star_rating = $option["value"];
    		    include "page_elements/star_rating_image.php";
    	    }
    
    		if($option["title"] == "description") {
    			echo "Description: " . $option["value"] . "<br>";
    		}
    		 
    		if($option["title"] == "address") {
    			echo "Address: " . $option["value"] . "<br>";
    		}
    	}
    }
    
	$rooms_to_reservate = unserialize(base64_decode($_POST["selected"]));
    $name = $_POST["thename"];
    $email = $_POST["email"];
    $country = $_POST["country"];
	
	//print "DEV: rooms_to_reservate:<pre>"; print_r($rooms_to_reservate); print "</pre>";
	
    echo "<br>Hotel " . $_POST["hotel"] . " - " . get_blog_option($_POST["hotel"], 'blogname') . "<br>";
    
?>
    <div class="jcarousel-wrapper">
        <div class="jcarousel">
            <ul>
<?php                 
                foreach (get_uploaded_header_images() as $header_image ) {                	
                    echo "<li><img src=" . $header_image['url'] . " alt='Image'></li>";	                	
                }       			
?>
    		</ul>
        </div>
    
    	<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
    	<a href="#" class="jcarousel-control-next">&rsaquo;</a>
    
    	<p class="jcarousel-pagination"></p>    	
    </div>

<?php 
    
    echo "<br>Arrival: " . $_POST["arrival"];
    echo "<br>Departure: " . $_POST["departure"];
    
    //Reformat dates
    $arrExp = explode("/", $_POST["arrival"]);
    $depExp = explode("/", $_POST["departure"]);
    
    $arrival = $arrExp[2] . "-" . $arrExp[1] . "-" . $arrExp[0] . "_00:00:00";
    $departure = $depExp[2] . "-" . $depExp[1] . "-" . $depExp[0] . "_00:00:00";
                                                                                  
   
?>
    <table>
    <thead> 
    <tr>
        <th align='center'>Room</th>
        <th align='center'>Persons allowed</th>        
        <th align='center'>Room price for one day</th>   
        <th align='center'>Room price for whole stay</th>      
        <th align='center'>Conditions (Board type)</th>
        <th align='center'>Nr. of rooms</th>    
        <th align='center'>Subtotal</th>    
    </tr>
    </thead>
<?php    
    
    $reserve_button_displayed = false;
        
    //Calculate rowspans for rooms (count number of non-zero POST-ed quantities for each room)    
    $rowspans = array();
    foreach ($rooms_to_reservate as $id => $room_persons_boards_quantities) {
    	$rowspans[$id] = 0;
    	foreach($room_persons_boards_quantities as $persons => $boards_quantities) {
    		foreach($boards_quantities as $board => $quantity) {
    			if($quantity != 0) {
    				$rowspans[$id] ++ ;
    			}
    		}
    	}
    }
    
    $hotel_custom_fields = get_option("reservations_custom_fields");
    
    //Find rates custom field
    if (!empty($hotel_custom_fields))
    foreach($hotel_custom_fields["fields"] as $custom_field_id => $custom_field) {
    	if ($custom_field["title"] == "rates") {
    		$rates = $custom_field["options"];
    		$rates_custom_field_id = $custom_field_id;
    		break;
    	}
    }
    
    $resource_api = new ER_Inbound_Resource_API($rates, $rates_custom_field_id);
    
    $total = 0;
    
    foreach($rooms_to_reservate as $id => $room_persons_boards_quantities) { 
        
    	$description_displayed = false;
    	
    	foreach($room_persons_boards_quantities as $persons => $boards_quantities) {	
    		
    		//print "DEV: board_rates is: <pre>"; print_r($board_rates); print "</pre>";
    		//print "DEV: boards_quantities is: <pre>"; print_r($boards_quantities); print "</pre>";
    		
    		foreach($boards_quantities as $board_code => $quantity) {
    			
    			$board_rates = $resource_api -> get_all_board_rates_for_resource($id, $quantity, $arrival, $departure, $persons);
    			
    			if($quantity != 0) {
	    			
    				$board_rate = $board_rates[$board_code];
    				
    				$total += $board_rate["price_whole_stay"];
    				
    			    //Set the class for responsive to know to mark it as room description td field
		    		if (!$description_displayed) {
		    		    echo "<tr class='room-first-row'>";
		    		} else {
		    			echo "<tr>";
		    		}
			        
		    	    if (!$description_displayed) {
			            echo "<td align='center' rowspan='" . $rowspans[$id] . "'>";
			            include "page_elements/room-description.php";
			            echo "</td>";
			            $description_displayed = true;
			        }          
			        
			        
			        echo "<td align='center'>";        
			        include "page_elements/persons-image.php";
			        echo "</td>";  
			
			        echo "<td align='center'>";
			        echo easyreservations_format_money($board_rate["price_one_day"] / $quantity, 1);
			        echo "</td>";  
			        
			        echo "<td align='center'>";
			        echo easyreservations_format_money($board_rate["price_whole_stay"] / $quantity, 1);
			        echo "</td>";
			        
			        echo "<td align='center'>";
			        echo $board_rate["name"];
			        echo "</td>";  
			        
			        echo "<td align='center'>";   
			        echo $quantity; 
			        echo "</td>";    
			        
			        echo "<td>";
			        echo easyreservations_format_money($board_rate["price_whole_stay"], 1);
			        echo "</td>";
			        
			    	echo "</tr>";
		    	
    			} else {  
    				//echo "qty is zero";
    			}    			
    			
	    	} //end foreach $boards_quantities
        } //end foreach $room_persons_boards_quantities
    	
    } //end foreach $rooms_to_reservate
    
?>        
        </td>
    </tr>
    <tr class='totals-row'>
        
        <td> </td>
        <td> </td>
        <td>Total: </td>
        <td class='totals-row-sum-field'><?php echo easyreservations_format_money($total, 1); ?></td>
        <td> </td>
        <td> </td>
        <td> </td>
        
    </tr>
</table>                                                                               
                                                                                     
                                                                                        
<?php                                                                                  
  
    echo "<br>Your name: " . $name;
    echo "<br>Your email: " . $email;
    echo "<br>Your country: " . $country;
    
    echo "<br><br>Reservation reference number: " . $grouped_reservation_id;
    

    switch_to_blog(1);
?>
 