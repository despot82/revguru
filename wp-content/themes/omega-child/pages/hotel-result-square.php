<a href=<?php echo site_url() 
                     . "/browse-hotel?hotel=" . $hotel_accommodation["hotel_id"]
	                 . "&arrival=" . $_GET["arrival"] 
                     . "&departure=" . $_GET["departure"] 
                     . "&people=" . $people; ?> target="_blank"> 
                     
<div id="hotel_<?php echo $hotel_accommodation["hotel_id"]; ?>" class="hotel">

<div id="hotel_<?php echo $hotel_accommodation["hotel_id"]; ?>_image" class="hotel_image" >    
    
         
        <?php 
        
        $sql = "SELECT blog_header_image_url " 
		 	 . "FROM blogs_header_images "
			 . "WHERE blog_id = " . $hotel_accommodation["hotel_id"];
				
		$results = $wpdb -> get_results($sql);
		
        ?>
    
    <img src="<?php echo $results[0] -> blog_header_image_url ?>" />
    
               
</div>
    
<div id="hotel_<?php echo $hotel_accommodation["hotel_id"]; ?>_accommodation_info" class="hotel_accommodation_info">
<?php 

    echo "<br>" . $hotel_accommodation["hotel_id"] . ". " . get_blog_option($hotel_accommodation["hotel_id"], 'blogname');
    
    //Sort the rooms of this hotel by groundprice in ascending order
    $prices = array();
    foreach ($hotel_accommodation["rooms"] as $room) {
    	 
    	$prices[] = $room["groundprice"];         
    }
    
    array_multisort($prices, $hotel_accommodation["rooms"]);
        
    //Display the rooms
    $max_rooms_to_display_per_found_hotel = 2;
    $rooms_displayed = 0;
    foreach ($hotel_accommodation["rooms"] as $room) {
        	
    	echo "<br>Room " . $room["resource_id"] . " with maximum occupancy: " . $room["max_persons_req"] . " with a price of " . $room["groundprice"];
    	$rooms_displayed++;
    	if($rooms_displayed == $max_rooms_to_display_per_found_hotel) {
    		break;
    	}    	
    }
    
    /*
    print "   - its rooms that can fully accommodate the groups: <pre>";
    print_r ($hotel_accommodation["rooms"]); 
    print "</pre>";
    */
      	
        	
?>


</div>
</div>
</a>
<br>