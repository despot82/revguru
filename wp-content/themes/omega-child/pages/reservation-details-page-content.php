<?php

    global $wp;
    //echo "user agent is:" . $_SERVER['HTTP_USER_AGENT'];
    require_once ABSPATH . "wp-content/plugins/easyreservations/rest/ServiceClasses/er-inbound-resource-api.php";
    
    //print "DEV: POST:<pre>"; print_r($_POST); print "</pre>";
        
    $network_theme_url = get_stylesheet_directory_uri();
    
    switch_to_blog($_POST["hotel"]);
    
    echo "<br>Hotel " . get_blog_option($_POST["hotel"], 'blogname') . "<br>";
?>
    <div class="jcarousel-wrapper">
        <div class="jcarousel">
            <ul>
<?php                 
                foreach (get_uploaded_header_images() as $name => $header_image ) {                	
                    echo "<li><img src=" . $header_image['url'] . " alt='Image'></li>";	                	
                }       			
?>
    		</ul>
        </div>
    
    	<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
    	<a href="#" class="jcarousel-control-next">&rsaquo;</a>
    
    	<p class="jcarousel-pagination"></p>    	
    </div>

<?php 

    if(!empty(get_option("reservations_custom_fields")["fields"]))
    foreach(get_option("reservations_custom_fields")["fields"] as $custom_field_id => $custom_field) {
       
    	if($custom_field["title"] == "star_rating") {
    		$star_rating = $custom_field["value"];
    		include "page_elements/star_rating_image.php";
    	}
    	 
    	if($custom_field["title"] == "description") {
    		echo "Description: " . $custom_field["value"] . "<br>";
    	}
    	
    	if($custom_field["title"] == "address") {
    		echo "Address: " . $custom_field["value"] . "<br>";
    	}
    	
    	if($custom_field["title"] == "phone") {
    		echo "Phone number: " . $custom_field["value"] . "<br>";
    	}
    	
    	if ($custom_field["title"] == "rates") {
    		$rates = $custom_field["options"];
    		$rates_custom_field_id = $custom_field_id;
    	}
    }
       
    $resource_api = new ER_Inbound_Resource_API($rates, $rates_custom_field_id );

    //Reformat dates
    $arrExp = explode("/", $_POST["arrival"]);
    $depExp = explode("/", $_POST["departure"]);
    $arrival = $arrExp[2] . "-" . $arrExp[1] . "-" . $arrExp[0] . "_00:00:00";
    $departure = $depExp[2] . "-" . $depExp[1] . "-" . $depExp[0] . "_00:00:00";
    
    
    if (empty($_POST["arrival"])) {
    	echo "<br>Arrival date is not set. Please go back and set the date of arrival.";
    	return;
    }
    
    //Date with slashes (aa/bb/cccc) is seen as mm/dd/yyyy by strtotime
    //Date with  dashes (aa-bb-cccc) is seen as dd-mm-yyyy by strtotime
    $arrival_with_dashes = str_replace("/", "-", $_POST["arrival"]);
    $departure_with_dashes = str_replace("/", "-", $_POST["departure"]);
  
    if(strtotime($arrival_with_dashes) < strtotime('now')) {
    	echo "<br>Arrival is in the past, this is not allowed. Please go back and pick another arrival date";
    	return;
    }
    
    if(strtotime($arrival_with_dashes) > strtotime($departure_with_dashes)) {
    	echo "<br>You can't have departure before arrival or at the same time as arrival, please go back and pick another departure date.";
    	return;
    }    
    
    $groups = explode(";", $_GET["people"]);
    
    $resource_api = new ER_Inbound_Resource_API($rates, $rates_custom_field_id);    
      	
?>  
    <br>
    <form id='rooms_form' action='reservation-confirmation' method='post' target='_self'>
    
    <input id='arrival' name='arrival' type='hidden' value='<?php echo $_POST["arrival"] ?>' />
    <input id='departure' name='departure' type='hidden' value='<?php echo $_POST["departure"] ?>' />
    <input id='hotel' name='hotel' type='hidden' value='<?php echo $_POST["hotel"] ?>' />

    <table>
    <thead> 
    <tr>
        <th align='center'>Room</th>
        <th align='center'>Persons allowed</th>        
        <th align='center'>Room price for one day</th>        
        <th align='center'>Room price for whole stay</th> 
        <th align='center'>Conditions (Board type)</th>
        <th align='center'>Nr. of rooms</th>
        <th align='center'>Subtotal</th>
        <th align='center'></th>
    </tr>
    </thead>
<?php    
    
    $reserve_button_displayed = false;
    
    $rooms_persons_boards_quantities = $_POST["selected"];
    //print "DEV: rooms_persons_boards_quantities is: <pre>"; print_r($rooms_persons_boards_quantities); print "</pre>";
   
    //Calculate rowspans for rooms (count number of non-zero POST-ed quantities for each room) and for reserve button
    $total_rowspan = 0;
    $rooms_rowspans = array();
    foreach ($rooms_persons_boards_quantities as $id => $room_persons_boards_quantities) {
    	$rooms_rowspans[$id] = 0;
    	foreach($room_persons_boards_quantities as $persons => $boards_quantities) {
    		foreach($boards_quantities as $board => $quantity) {
    			if($quantity != 0) {
    				$rooms_rowspans[$id] ++ ;
    				$total_rowspan ++ ;
    			}
    		}
    	}
    }
?>
    <div class="form-group date">
		<label for="arrival_display">Arrival</label> 
	    <input id="arrival_display" name="arrival_display" class="form-control" type="text" />	
	</div>
			
    <div class="form-group date">
		<label for="departure_display">Departure</label> 
	    <input id="departure_display" name="departure_display" class="form-control" type="text" />	
	</div>

    
    
<?php 
    
    $total = 0;
    
    foreach($rooms_persons_boards_quantities as $id => $room_persons_boards_quantities) { 
        
    	$availability = $resource_api -> get_availability_for_resource_in_period($id, $arrival, $departure, 1, 0 );
    	//print "DEV: availability for room " . $id . " is: <pre>"; print_r($availability); print "</pre>";
    	
    	?>
    	<script>
    	    save_availability_for_room(<?php echo $id; ?>, <?php echo $availability["number_of_available_rooms"]; ?>);
    	</script>
    	<?php 
    	
    	$description_displayed = false;
    	
    	foreach($room_persons_boards_quantities as $persons => $boards_quantities) {	
    		
    		//print "DEV: board_rates is: <pre>"; print_r($board_rates); print "</pre>";
    		
    		foreach($boards_quantities as $board_code => $quantity) {
    			
    			//echo "DEV: dollar board_code is: " . $board_code;
    			
    			if($quantity != 0) {
	    			
    				//filter custom fields, find rates filter, find room rates ($board_rates)
    				$board_rates = $resource_api -> get_all_board_rates_for_resource($id, $quantity, $arrival, $departure, $persons);
    				
    				$board_rate = $board_rates[$board_code];
    				
    				//Add the subtotal for this room with specified board rate and in specified $quantity
    				$total += $board_rate["price_whole_stay"];
    				
    			    //Set the class for responsive to know to mark it as room description td field
		    		if (!$description_displayed) {
		    		    echo "<tr class='room-first-row'>";
		    		} else {
		    			echo "<tr>";
		    		}
			        
		    	    if (!$description_displayed) {
			            echo "<td align='center' rowspan='" . $rooms_rowspans[$id] . "'>";
			            include "page_elements/room-description.php";
			            echo "</td>";
			            $description_displayed = true;
			        }          
			        
			        
			        echo "<td align='center'>";        
			        include "page_elements/persons-image.php";
			        echo "</td>";  
			
			        echo "<td class='room_price_one_day' align='center' data-price='" . $board_rate["price_one_day"] / $quantity . "'>";   
			        echo easyreservations_format_money($board_rate["price_one_day"] / $quantity, 1);
			        echo "</td>";  
			        
			        echo "<td class='room_price_whole_stay' align='center' data-price='" . $board_rate["price_whole_stay"] / $quantity . "'>";
			        echo easyreservations_format_money($board_rate["price_whole_stay"] / $quantity, 1);
			        echo "</td>";
			        
			        echo "<td align='center'>";
			        echo $board_rate["name"];
			        echo "</td>";  
			        
			        echo "<td align='center'>";   
			        
			        echo "<select onChange='quantity_changed(" . $id . "," . $persons . ", \"" . $board_code . "\", this, false);' 
                             	  name='selected[" . $id . "][" . $persons . "][" . $board_code . "]'
    		                      style='padding: 5px;'  
    		                      class='room_" . $id . "_quantity quantity'     		                       
    		                      form='rooms_form'>";
			        
			        $is_default_option_set = false; 
			        for($choice = 0; $choice <= $availability["number_of_available_rooms"]; $choice++  ) {
			        	if(!$is_default_option_set && $choice == $quantity) {   
			        		echo "<option value='" . $choice . "' selected='selected'>";        	    
			        	    $is_default_option_set = true;
			        	} else {
			        		echo "<option value='" . $choice . "' >";
			        	}   
			        	echo $choice . "</option>";
			        }          
			        
			        echo "</select>";
			        echo "</td>";   
			        
			        echo "<td class='subtotal' align='center' data-price='" . $board_rate["price_whole_stay"] . "'>";
			        echo easyreservations_format_money($board_rate["price_whole_stay"], 1);
			        echo "</td>";
			
			        if(!$reserve_button_displayed) {
			       
			            echo "<td class='submit-in-table' align='center' rowspan=" . $total_rowspan . ">";        
			            echo "<input type='submit' value='Book' />";        
			            echo "</td>";
			        
			            $reserve_button_displayed = true;
			        }
			        
			    	echo "</tr>";
		    	
    			} else {  
    				//echo "qty is zero";
    			}    			
    			
	    	} //end foreach $boards_quantities
        } //end foreach $room_persons_boards_quantities
?>
        <script type="text/javascript">
            quantity_changed(<?php echo $id . ", " . $persons . ", '" . $board_code ."', null, true"; ?>);
        </script>
<?php 
    } //end foreach $rooms_persons_boards_quantities as $id => $room_persons_boards_quantities
?>        
        
    <tr class='totals-row'>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td> </td>
        <td>Total: </td>
        <td class='totals-row-sum-field' align='center'><?php echo easyreservations_format_money($total, 1); ?></td>
    </tr>
</table>



<script type="text/javascript">
    set_arrival_field("<?php echo $_POST["arrival"]; ?>");
    set_departure_field("<?php echo $_POST["departure"]; ?>");
</script>

<?php 

	include "page_elements/guest-details-form.php";
?>	
	<div class='submit-below-table' align='center'>
	<input type='submit' value='Book' />
	</div>
	
</form>
	
<?php

	switch_to_blog(1);

?>
  