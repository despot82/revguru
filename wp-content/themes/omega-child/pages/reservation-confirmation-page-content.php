<?php
    
    require_once ABSPATH . "wp-content/plugins/easyreservations/rest/ServiceClasses/er-inbound-reservation-api.php";
    require_once ABSPATH . "wp-content/plugins/easyreservations/rest/ServiceClasses/er-inbound-resource-api.php";
    require_once ABSPATH . "braintree-php-3.2.0/lib/Braintree.php";
    //echo "user agent is:" . $_SERVER['HTTP_USER_AGENT'];
    global $wp;
    
    $reservation_api = new ER_Inbound_Reservation_API();
    
    $network_site_url_name = get_blog_details(0) -> path;
    $network_theme_url = get_stylesheet_directory_uri();
    
    $braintree_payment_option = get_option("braintree_payment_option");
    
    switch_to_blog($_POST["hotel"]);    
        
    //Display the hotel metadata
    if(!empty(get_option("reservations_custom_fields")["fields"])) {
    	foreach(get_option("reservations_custom_fields")["fields"] as $option) {
    		
    	    if($option["title"] == "star_rating") {
    		    $star_rating = $option["value"];
    		    include "page_elements/star_rating_image.php";
    	    }
    
    		if($option["title"] == "description") {
    			echo "Description: " . $option["value"] . "<br>";
    		}
    		 
    		if($option["title"] == "address") {
    			echo "Address: " . $option["value"] . "<br>";
    		}
    	}
    }
    
	$rooms_to_reservate = $_POST["selected"];
    $name = $_POST["thename"];
    $email = $_POST["email"];
    $country = $_POST["country"];
	
    echo "<br>Hotel " . $_POST["hotel"] . " - " . get_blog_option($_POST["hotel"], 'blogname') . "<br>";
    
?>
    
    <div class="jcarousel-wrapper">
        <div class="jcarousel">
            <ul>
<?php                 
                foreach (get_uploaded_header_images() as $header_image ) {                	
                    echo "<li><img src=" . $header_image['url'] . " alt='Image'></li>";	                	
                }       			
?>
    		</ul>
        </div>
    
    	<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
    	<a href="#" class="jcarousel-control-next">&rsaquo;</a>
    
    	<p class="jcarousel-pagination"></p>    	
    </div>
    
<?php 

    echo "<br>Arrival: " . $_POST["arrival"];
    echo "<br>Departure: " . $_POST["departure"];
    
    //Reformat dates
    $arrExp = explode("/", $_POST["arrival"]);
    $depExp = explode("/", $_POST["departure"]);
    
    $arrival = $arrExp[2] . "-" . $arrExp[1] . "-" . $arrExp[0] . "_00:00:00";
    $departure = $depExp[2] . "-" . $depExp[1] . "-" . $depExp[0] . "_00:00:00";
                                                                                  
   
?>
    <table>
    <thead> 
    <tr>
        <th align='center'>Room</th>
        <th align='center'>Persons allowed</th>        
        <th align='center'>Room price for one day</th>   
        <th align='center'>Room price for whole stay</th>      
        <th align='center'>Conditions (Board type)</th>
        <th align='center'>Nr. of rooms</th>   
        <th align='center'>Subtotal</th>     
    </tr>
    </thead>
<?php    
    
    $reserve_button_displayed = false;
        
    //Calculate rowspans for rooms (count number of non-zero POST-ed quantities for each room)    
    $rowspans = array();
    foreach ($rooms_to_reservate as $id => $room_persons_boards_quantities) {
    	$rowspans[$id] = 0;
    	foreach($room_persons_boards_quantities as $persons => $boards_quantities) {
    		foreach($boards_quantities as $board => $quantity) {
    			if($quantity != 0) {
    				$rowspans[$id] ++ ;
    			}
    		}
    	}
    }
    
    $hotel_custom_fields = get_option("reservations_custom_fields");
    
    //Find rates custom field
    if (!empty($hotel_custom_fields))
    foreach($hotel_custom_fields["fields"] as $custom_field_id => $custom_field) {
    	if ($custom_field["title"] == "rates") {
    		$rates = $custom_field["options"];
    		$rates_custom_field_id = $custom_field_id;
    		break;
    	}
    }
    
    $resource_api = new ER_Inbound_Resource_API($rates, $rates_custom_field_id);
    
    $total = 0;
    
    foreach($rooms_to_reservate as $id => $room_persons_boards_quantities) { 
        
    	$description_displayed = false;
    	
    	foreach($room_persons_boards_quantities as $persons => $boards_quantities) {	
    		
    		//print "DEV: board_rates is: <pre>"; print_r($board_rates); print "</pre>";
    		//print "DEV: boards_quantities is: <pre>"; print_r($boards_quantities); print "</pre>";
    		
    		foreach($boards_quantities as $board_code => $quantity) {
    			
    			$board_rates = $resource_api -> get_all_board_rates_for_resource($id, $quantity, $arrival, $departure, $persons);
    			
    			if($quantity != 0) {
	    			
    				$board_rate = $board_rates[$board_code];
    				
    				//Add the subtotal for this room with specified board rate and in specified $quantity
    				$total += $board_rate["price_whole_stay"]; 
    				
    			    //Set the class for responsive to know to mark it as room description td field
		    		if (!$description_displayed) {
		    		    echo "<tr class='room-first-row'>";
		    		} else {
		    			echo "<tr>";
		    		}
			        
		    	    if (!$description_displayed) {
			            echo "<td align='center' rowspan='" . $rowspans[$id] . "'>";
			            include "page_elements/room-description.php";
			            echo "</td>";
			            $description_displayed = true;
			        }          
			        
			        
			        echo "<td align='center'>";        
			        include "page_elements/persons-image.php";
			        echo "</td>";  
			
			        echo "<td align='center'>";
			        echo easyreservations_format_money($board_rate["price_one_day"] / $quantity, 1);
			        echo "</td>";  
			        
			        echo "<td align='center'>";
			        echo easyreservations_format_money($board_rate["price_whole_stay"] / $quantity, 1);
			        echo "</td>";
			        
			        echo "<td align='center'>";
			        echo $board_rate["name"];
			        echo "</td>";  
			        
			        echo "<td align='center'>";   
			        echo $quantity; 
			        echo "</td>";    
			        
			        echo "<td>";
			        echo easyreservations_format_money($board_rate["price_whole_stay"], 1);
			        echo "</td>";
			        
			    	echo "</tr>";
		    	
    			} else {  
    				//echo "qty is zero";
    			}    			
    			
	    	} //end foreach $boards_quantities
        } //end foreach $room_persons_boards_quantities
    	
    } //end foreach $rooms_to_reservate
    
?>        
        </td>
    </tr>
    <tr class='totals-row'>
        
        <td> </td>
        <td> </td>
        <td>Total: </td>
        <td class='totals-row-sum-field'><?php echo easyreservations_format_money($total, 1); ?></td>
        <td> </td>
        <td> </td>
        <td> </td>
        
    </tr>
</table>                                                                               
                                                                                     
                                                                                        
<?php                                                                                  
  
    echo "<br>Your name: " . $name;
    echo "<br>Your email: " . $email;
    echo "<br>Your country: " . $country;
    
    //Here we make the reservations and make assocations with the grouped reservation that holds them
     
    date_default_timezone_set('Europe/Belgrade');
    
    //Make this reservation group through a separate function make_grouped_reservation which 
    //will make unapproved reservations for separate roomnumber-s of the room in question
    $reservating_result = $reservation_api -> make_grouped_reservation($rooms_to_reservate, $arrival, $departure, $name, $email, $country, $braintree_payment_option, $rates_custom_field_id);
    
    if ($reservating_result["success"]) {
    	
    	$reservations = $reservating_result["created_reservations"];
    	
    	//echo "<br>reservations[0] is: " . $reservations[0];
    	
    	$grouped_reservation_id = $reservating_result["grouped_reservation_post_id"];
    }
    
    switch_to_blog(0);
    
    Braintree_Configuration::environment('sandbox');
    Braintree_Configuration::merchantId('9sk9j69284sm4c5h');
    Braintree_Configuration::publicKey('hsqzztygnpvpxk2p');
    Braintree_Configuration::privateKey('3ec5b86dc1dca25e6b1aac2c0907e711');
    
    switch($braintree_payment_option) {
    	case 1: $sum_to_pay = 0.1 * $reservating_result["total_sum"]; break;
    	case 2: $sum_to_pay = $reservating_result["total_sum"]; break;
    	case 3: $sum_to_pay = 0; break;
    	default: $sum_to_pay = $reservating_result["total_sum"]; break;
    }
  
?>

    <form id="checkout" method="post" action="<?php echo $network_site_url_name; ?>thank-you">
        <div id="payment-form"></div>    
	
        <input type="hidden" name="amount" value="<?php echo $sum_to_pay; ?>">
        <input type="hidden" name="grouped_reservation_id" value="<?php echo $grouped_reservation_id; ?>">
        <input type="hidden" name="selected" value="<?php echo base64_encode(serialize($rooms_to_reservate)); ?>">
        
        <input type="hidden" name="thename" value="<?php echo $name; ?>">
        <input type="hidden" name="email" value="<?php echo $email; ?>">
        <input type="hidden" name="country" value="<?php echo $country; ?>">
        
        <input type="hidden" name="arrival" value="<?php echo $_POST["arrival"]; ?>">
        <input type="hidden" name="departure" value="<?php echo $_POST["departure"]; ?>">
        
        <input id='hotel' name='hotel' type='hidden' value='<?php echo $_POST["hotel"] ?>' />

        <input type="submit" value="Pay $<?php echo $sum_to_pay; ?>">

    </form>
    
    <script src="https://js.braintreegateway.com/v2/braintree.js"></script>
    <script>
    
	    var clientToken = "<?php echo($clientToken = Braintree_ClientToken::generate()); ?>";
	    
	    braintree.setup(clientToken, "dropin", {
	    	container: "payment-form"
	    });
	    
    </script>
<?php 
    switch_to_blog(1);
?>
 