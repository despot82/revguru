<?php
    //print "DEV: <pre>"; print_r($_GET); print "</pre>";    
    global $wp;

    require ABSPATH . "wp-content/plugins/easyreservations/rest/ServiceClasses/hotel-search-helper.php";
        
    if (empty($_GET["arrival"])) {
    	echo "<br>Arrival date is not set. Please go back and set the date of arrival.";
    	return;
    }
    
    //Date with slashes (aa/bb/cccc) is seen as mm/dd/yyyy by strtotime
    //Date with  dashes (aa-bb-cccc) is seen as dd-mm-yyyy by strtotime
    $arrival_with_dashes = str_replace("/", "-", $_GET["arrival"]);
    $departure_with_dashes = str_replace("/", "-", $_GET["departure"]);
    
    if(strtotime($arrival_with_dashes) < strtotime('now')) {
    	echo "<br>Arrival is in the past, this is not allowed. Please go back and pick another arrival date";
    	return;
    }
        
    if(strtotime($arrival_with_dashes) > strtotime($departure_with_dashes)) {
    	echo "<br>You can't have departure before arrival or at the same time as arrival, please go back and pick another departure date.";
    	return;
    }
    
    $network_theme_url = get_stylesheet_directory_uri();
    
    
    switch_to_blog($_GET["hotel"]);
    
    echo "<br>Hotel " . get_blog_option($_GET["hotel"], 'blogname') . "<br>";

?>
    <div class="jcarousel-wrapper">
        <div class="jcarousel">
            <ul>
<?php                 
                foreach (get_uploaded_header_images() as $name => $header_image ) {                	
                    echo "<li><img src=" . $header_image['url'] . " alt='Image'></li>";	                	
                }       			
?>
    		</ul>
        </div>
    
    	<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
    	<a href="#" class="jcarousel-control-next">&rsaquo;</a>
    
    	<p class="jcarousel-pagination"></p>    	
    </div>
<?php 
    
    if(!empty(get_option("reservations_custom_fields")["fields"]))
    foreach(get_option("reservations_custom_fields")["fields"] as $custom_field_id => $custom_field) {
    	
    	if($custom_field["title"] == "star_rating") {
    		$star_rating = $custom_field["value"];
    		include "page_elements/star_rating_image.php";
    	}
    	
    	if($custom_field["title"] == "description") {
    		echo "Description: " . $custom_field["value"] . "<br>";
    	}
    	
    	if($custom_field["title"] == "address") {
    		echo "Address: " . $custom_field["value"] . "<br>";
    	}
    	
    	if($custom_field["title"] == "phone") {
    		echo "Phone number: " . $custom_field["value"] . "<br>";
    	}
    	
    	if ($custom_field["title"] == "rates") {
    		$rates = $custom_field["options"];
    		$rates_custom_field_id = $custom_field_id;
    	}
    }
    
    
?>

<div id="roomsets">

<br><br>

<?php 
    
	include "page_elements/browse-hotel-page-roomsets.php";
	
?>
</div> <!-- end roomsets -->

<?php 

    switch_to_blog(1);
    
?>

