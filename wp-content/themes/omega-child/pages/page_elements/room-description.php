<?php

if(empty($form)) $form = 0;

if (has_post_thumbnail($id) ) {  ?>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".form-<?php echo $form; ?>-room-<?php echo $id; ?>">
    <div class='img-wrapper'>   
<?php                 
            $thumb_id = get_post_thumbnail_id($id);
            $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
            $thumb_url = $thumb_url_array[0];
            //echo "<a href='" . the_permalink() . "'><img src='".$thumb_url."' alt='photo' /></a>";
            echo "<img src='".$thumb_url."' alt='photo' />";
?>   
    </div> 
</button>
  
<div class="modal fade form-<?php echo $form; ?>-room-<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">


        <div id="carousel-form-<?php echo $form; ?>-room-<?php echo $id; ?>" class="carousel slide" data-ride="carousel">
		  
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
    
<?php
    foreach (get_post_gallery_images($id) as $image_id => $image_url) {
    	if ($image_id == 0) { ?>  
    	    <li data-target="#carousel-form-<?php echo $form; ?>-room-<?php echo $id; ?>" data-slide-to="0" class="active"></li>
<?php
    	} else {
?>
    		<li data-target="#carousel-form-<?php echo $form; ?>-room-<?php echo $id; ?>" data-slide-to="<?php echo $image_id; ?>"></li>
<?php 
    	}
   	
    }   
?> 

		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner" role="listbox">
<?php
    foreach (get_post_gallery_images($id) as $image_id => $image_url) {
    	if ($image_id == 0) { ?>  
    	    <div class='item active'>
<?php
    	} else {
?>
    		<div class='item'>
<?php 
    	}
?>
              <img src="<?php echo $image_url; ?>" alt="...">
		      <div class="carousel-caption">
		        caption
		      </div>
		    </div>
<?php     	
    }   
?>        
		  
          <!-- Controls -->
		  <a class="left carousel-control" href="#carousel-form-<?php echo $form; ?>-room-<?php echo $id; ?>" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-form-<?php echo $form; ?>-room-<?php echo $id; ?>" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		  </a>
		</div>  <!-- END carousel-example-generic -->
 
      </div>  <!--  wrapper for slides (<div class="carousel-inner") -->
      </div>
    </div>
  </div>
</div>

<?php   
}
	            
echo get_the_title($id);   
echo "<br>Description: " . get_post_meta($id, "description", true);


?>