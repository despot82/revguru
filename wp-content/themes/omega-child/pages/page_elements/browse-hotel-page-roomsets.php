<?php 


    include "browse-hotel-page-calculate.php";

    //print "<pre>"; print_r($optimum_roomset); print "</pre>";
    
    if($optimum_roomset["can_accommodate"] != 1) {
    	
    	return;
    }
    
?>
	Suggested rooms for you. Feel free to select the rate that suits you:<br>
	
    <form id='optimal_roomset_form' action='../reservation-details' method='post' target='_self'>
    <input id='arrival' name='arrival' type='hidden' value='<?php echo $_GET["arrival"]; ?>' />
    <input id='departure' name='departure' type='hidden' value='<?php echo $_GET["departure"]; ?>' />
    <input id='hotel' name='hotel' type='hidden' value='<?php echo $_GET["hotel"]; ?>' />
	
	<table id='optimal_roomset_table' >
	    <thead> 
	     <tr>
	        <th align='center'>Room</th>
	        <th align='center'>Persons allowed</th>        
	        <th align='center'>Room price for one day</th>   
	        <th align='center'>Room price for whole stay</th>      
	        <th align='center'>Conditions (Board type)</th>
	        <th align='center'>Nr. of rooms</th>
	        <th align='center'></th>
	    
	     </tr>
	    </thead>
<?php    
	    
	    $reserve_button_displayed = false;

	    //print "DEV: optimum_roomset: <pre>"; print_r ($optimum_roomset); print "</pre>";
	    
	    $form = 1; //Form indicator
	    //Go through each room, calculate various rates and display them    
	    foreach($optimum_roomset as $id => $room) {  
	    	
	    	//Ignore the first field, since it is just a success indicator for the hotel's capability to accommodate all
	    	if($id == "can_accommodate") continue;
	    	
	    	//print "DEV: room:<pre>"; print_r($room); print "</pre>";
	        //image + name + description; max number of persons; price; board; quantity; reserve button
	        
	    	$resource = new Resource($id);
	    	
	    	$description_displayed = false;
	    	
	    	//Make the rows with image, number of persons, price, board type and Reserve button
	    	foreach ($persons_filter_conditions[$id] as $persons)
	    	{	
	    		
	    		//To get all rates, make a fake reservation for each variation of board type and number of persons filter
	    		//and calculate the reservation's price
	    		$board_rates = $resource_api -> get_all_board_rates_for_resource($id, 1, $arrival, $departure, $persons);
	    		
	    		//print "DEV: board_rates for $id with $persons persons are: <pre>"; print_r($board_rates); print "</pre>";
	    		
		    	foreach ($board_rates as $code => $board_rate)
		    	{
		    		
		    		//Set the class for responsive to know to mark it as room description td field
		    		if (!$description_displayed) {
		    		    echo "<tr class='room-first-row'>";
		    		} else {
		    			echo "<tr>";
		    		}
		    		
		            if (!$description_displayed) {
		            	echo "<td align='center' rowspan='" . count($persons_filter_conditions[$id]) * count($board_rates) . "'>";
		            	include "room-description.php";
		            	echo "</td>";
		            
		            } 
		         
			        echo "<td align='center'>";
			        include "persons-image.php";    
			        echo "</td>"; 
			              
			        echo "<td align='center'>";        
			        echo easyreservations_format_money($board_rate["price_one_day"], 1);            
			        echo "</td>";  
			        
			        echo "<td align='center'>";
			        echo easyreservations_format_money($board_rate["price_whole_stay"], 1);
			        echo "</td>";
			          
			        echo "<td align='center'>";
			        echo $board_rate["name"];
			        echo "</td>";  
			        
			        echo "<td align='center'>";   
			        echo "<select onChange='optimum_roomset_form_quantity_changed(" . $id . ");' 
	    		                  id='optimum_form_room_" . $id . "_persons_" . $persons . "_rate_" . $code . "'    
	    		                  name='selected[" . $id . "][" . $persons . "][" . $code . "]' 
	    		                  style='padding: 5px;' 
	    		                  class='optimum_form_room_" . $id . "_quantity'  
	    		                  form='optimal_roomset_form'>";
			        
			        for($choice = 0; $choice <= $room["available_quantity"]; $choice++  ) {
			        	if(!$description_displayed && $choice == $optimum_roomset[$id]["counted"]) {   
			        		echo "<option value='" . $choice . "' selected='selected'>";
			        	} else {
			        		echo "<option value='" . $choice . "' >";
			        	}   
			        	echo $choice . "</option>";
			        }          
			        
			        echo "</select>";
			        echo "</td>";   
			      
			        if(!$reserve_button_displayed) {
			        	
			            echo "<td class='submit-in-table' align='center' rowspan=" . $reserve_button_rowspan_for_optimum_roomset . ">";        
			            echo "<input type='submit' value='Book' />";        
			            echo "</td>";
			            
			            $reserve_button_displayed = true;
			        }
			        
			    	echo "</tr>";
			    	
			    	$description_displayed = true;
		    	}
	        }

?>
    <script>optimum_roomset_form_quantity_changed(<?php echo $id; ?>);</script>
<?php 
	        
	    	/* print "DEV: <pre>"; print_r($room); print "</pre>"; */
	    }
	    
	?>
	           
	            </td>
	        </tr>
	    </table>     
	    <div class='submit-below-table' align='center'>      
	        <input type='submit' value='Book' />       
	    </div> 
	</form>
	
	<br><br>All potential rooms:<br>

    <form id='all_rooms_form' action='../reservation-details' method='post' target='_self'>		
	<input id='arrival' name='arrival' type='hidden' value='<?php echo $_GET["arrival"]; ?>' />
	<input id='departure' name='departure' type='hidden' value='<?php echo $_GET["departure"]; ?>' />
    <input id='hotel' name='hotel' type='hidden' value='<?php echo $_GET["hotel"]; ?>' />

	<table>
	    <thead>
	    <tr>
	        <th align='center'>Room</th>       
	        <th align='center'>Persons allowed</th>
	        <th align='center'>Room price for one day</th>    
	        <th align='center'>Room price for whole stay</th>     
	        <th align='center'>Conditions (Board type)</th>
	        <th align='center'>Nr. of rooms</th>
	        <th align='center'></th>
	    
	    </tr>
	    </thead>
	
	<?php     
	
		$reserve_button_displayed = false;
		
		$all_usable_rooms = $h -> get_usable_rooms();
		
		//print "DEV: all: <pre>"; print_r($all_usable_rooms); print "</pre>";
		
		$form = 2; //Form indicator
	    foreach($all_usable_rooms as $id => $room) {   	
	    	
	        //print "DEV: room:<pre>"; print_r($room); print "</pre>";	        
	    	//Ignore the first field, since it is just a success indicator for the hotel's capability to accommodate all
	    	
	    	?>
	    	<script>
	    	    save_availability_for_room(<?php echo $id; ?>, <?php echo $room["available_quantity"]; ?>);
	    	</script>
	    	<?php 
	    	
	    	$resource = new Resource($id);
	    	
	    	//print "DEV: persons_filter_conditions <pre>"; print_r($persons_filter_conditions); print "</pre>";
	    	
	    	$description_displayed = false;
	    	
	    	//Make the rows with image, number of persons, price, board type and Reserve button
	    	foreach ($persons_filter_conditions[$id] as $persons)
	    	{	
	    		//To get all rates, make a fake reservation for each variation of board type and number of persons filter
	    		//and calculate the reservation's price
	    		$board_rates = $resource_api -> get_all_board_rates_for_resource($id, 1, $arrival, $departure, $persons);
	    		
		    	foreach ($board_rates as $code => $board_rate)
		    	{
		    		//Set the class for responsive to know to mark it as room description td field
		    		if (!$description_displayed) {
		    			echo "<tr class='room-first-row'>";
		    		} else {
		    			echo "<tr>";
		    		}
		    		
		            if (!$description_displayed) {
		            	$description_displayed = true;
		            	echo "<td align='center' rowspan='" . count($persons_filter_conditions[$id]) * count($board_rates) . "'>";
		            	include "room-description.php";
		            	echo "</td>";
		            } 
		         
			        echo "<td align='center'>";
			        include "persons-image.php";    
			        echo "</td>";        
			         
			              
			        echo "<td align='center'>";   
			        echo easyreservations_format_money($board_rate["price_one_day"], 1);
			        echo "</td>";  
			        
			        echo "<td align='center'>";
			        echo easyreservations_format_money($board_rate["price_whole_stay"], 1);
			        echo "</td>";
			          
			        echo "<td align='center'>";
			        echo $board_rate["name"];
			        echo "</td>";  
			        
			        echo "<td align='center'>";   
			        
			        echo "<select onChange='all_rooms_form_quantity_changed(" . $id . ");'
	    		                  id='all_rooms_form_room_" . $id . "_persons_" . $persons . "_rate_" . $code . "'    
	    		                  name='selected[" . $id . "][" . $persons . "][" . $code . "]' 
	    		                  style='padding: 5px;' 
	    		                  class='all_rooms_form_room_" . $id . "_quantity'  
	    		                  form='all_rooms_form'>";
			        
			        for($choice = 0; $choice <= $room["available_quantity"]; $choice++  ) {
			        	if($choice == 0) {   
			        		echo "<option value='" . $choice . "' selected='selected'>";
			        	} else {
			        		echo "<option value='" . $choice . "' >";
			        	}   
			        	echo $choice . "</option>";
			        }          
			        
			        echo "</select>";
			        echo "</td>";    
			      
			        if(!$reserve_button_displayed) {
			        
			            echo "<td class='submit-in-table' align='center' rowspan=" . $reserve_button_rowspan_for_relevant_roomset . ">";        
			            echo "<input type='submit' value='Book' />";        
			            echo "</td>";
			            
			            $reserve_button_displayed = true;
			        }
			        
			        
			    	echo "</tr>";
		    	}
	        }
	    	
	    	//print "<pre>"; print_r($room); print "</pre>";	    	
	        
	    }	    
	    
	?>
	            
	        </td>
	    </tr>
	</table>
	<div class='submit-below-table' align='center'>      
	    <input type='submit' value='Book' />       
	</div>
</form>