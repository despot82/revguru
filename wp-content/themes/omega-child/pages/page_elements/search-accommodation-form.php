<form method="get" id="search_form"  action="<?php echo site_url() . "/search-results"; ?>">
        <div class="form-group">
			<label id="destination_label" for="destination">Destination:</label> 
			<input id="destination" name="destination" class="form-control" type="text"
				   placeholder="Enter destination" />
		</div>
		<div id="search_form_inner" class="form-inline">
			<div class="form-group date-recap">
				<label for="arrival">Arrival:</label> 
				<input id="arrival" name="arrival" class="form-control margined" type="text"
					placeholder="Enter arrival" />	
			</div>
			<div class="form-group date-recap">
				<label for="departure">Departure:</label> 
				<input id="departure" name="departure" class="form-control margined" type="text"
					   placeholder="Enter departure" />
			</div>
			<div class="form-group">
				<input id="destination_type" name="destination_type" type="hidden" />
			</div>
			<div class="form-group">
				<input id="destination_id" name="destination_id" type="hidden" />
			</div>
			<br>
			<label>
			Rooms:
			</label>
			<div class="room margined">
			    <label class="room-label">1.</label>
				<label>Adults</label> 
				<select class="form-control adults-number-field" name="adults[]">				
					<option>1</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
					<option>6</option>
					<option>7</option>
					<option>8</option>
					<option>9</option>
					<option>10</option>
				</select> 
				<label>Children</label> 
				<select class="form-control children-number-field" name="childs[]">
					<option>0</option><option>1</option><option>2</option><option>3</option><option>4</option>
					<option>5</option><option>6</option><option>7</option><option>8</option><option>9</option>
					<option>10</option>
				</select>
					
			</div>		

		</div>
		<input id="add_more_groups" class="btn btn-default add-more-groups-button margined"
			   name="add_more_groups" type="button"
			   value="Add more rooms" />
			  
<?php
    global $wp;
    
    //if this is search-results page, prepopulate all the fields
    if ($wp -> request == "search-results") { 
    	
    	foreach ($_GET["adults"] as $key => $adults) {
    		$groups[] = array("adults" => $_GET["adults"][$key], "childs" => $_GET["childs"][$key]);    		 
    	
    		$people .= ";" . $_GET["adults"][$key] . "," . $_GET["childs"][$key];    	
    	}	
    	
    	$people = substr($people, 1, strlen($people) - 1);
    	
    	
?>

    	<input id="search_button_ajax" class="btn btn-default search-button margined"
    	       type="button" value="Search" />
    	
    	<script type="text/javascript">
    	    //alert("just before setting datepickers");
            set_arrival_field("<?php echo $_GET["arrival"]; ?>", false);
            set_departure_field("<?php echo $_GET["departure"]; ?>", false);
            set_destination_fields("<?php echo $_GET["destination"]; ?>", 
            		               "<?php echo $_GET["destination_type"]; ?>",
            		               "<?php echo $_GET["destination_id"]; ?>");
            set_rooms_fields("<?php echo $people; ?>");
        </script>       
<?php 

    } else { //if this is another page, ie. search page
?>

        <button id="search_button" class="btn btn-default search-button margined"
			name="search_button" type="submit">Search
		</button>  
		
		<script type="text/javascript">
    	  
            set_arrival_field("", "");
            set_departure_field("", "");
            
        </script>  	 

<?php 		
    }		
?>	  
		
		
</form>
