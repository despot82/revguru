<?php
 
    global $wp;
    
    require_once ABSPATH . "/wp-content/plugins/easyreservations/rest/ServiceClasses/hotel-search-helper.php";

    //If this is a recurring search from the same page, using ajax, blog needs to be switched here, otherwise
    //the hotel has already been switched
    if(get_current_blog_id() == 1 || get_current_blog_id() == 0) {
    	$network_theme_url = get_stylesheet_directory_uri();
        switch_to_blog($_GET["hotel"]);
    }

    //Reformat dates
    $arrExp = explode("/", $_GET["arrival"]);
    $depExp = explode("/", $_GET["departure"]);
    $arrival = $arrExp[2] . "-" . $arrExp[1] . "-" . $arrExp[0] . " 00:00:00";
    $departure = $depExp[2] . "-" . $depExp[1] . "-" . $depExp[0] . " 00:00:00";
    
    //echo "<br>Arrival parameter is: " . $arrival;
    //echo "<br>now is: " . date("Y-m-d H:i:s");
    
    $groups = explode(";", $_GET["people"]);
    
    $h = new Hotel_Search_Helper($_GET["hotel"]);
    
    $sort_by = "size";
    
    $h -> calculate_relevant_hotel_rooms_for_criteria($groups, $arrival, $departure, $_GET["hotel"]);
    
    $relevant_rooms = $h -> get_usable_rooms();
	
    if(is_null($relevant_rooms)) {
    	echo "<br>Sorry, but this hotel can't accommodate you at this moment. Please select different number of rooms or stay time.";
    	return;
    }

	if(!isset($relevant_rooms)) {
    	echo "<br>Sorry, but this hotel can't accommodate you at this moment. Please select different number of rooms or stay time.";
    	return;
    }
    
    $optimum_roomset = $h -> get_smallest_room_set_for_criteria($_GET["hotel"],
    		                                                    $groups,
    		                                                    $arrival,
    		                                                    $departure);
    
    if($optimum_roomset["can_accommodate"] == 0) {
    	echo "<br>Sorry, but this hotel doesn't have enough capacity to accommodate you at this moment. Please select different number of rooms or stay time.";
    	return;
    }
    
    //Do a quick calculation of rowspan for reserve button for optimum roomset
    $reserve_button_rowspan_for_optimum_roomset = 0;
    $hotel_custom_fields = get_option("reservations_custom_fields");
    
    //Find rates custom field
    if (!empty($hotel_custom_fields))
    foreach($hotel_custom_fields["fields"] as $custom_field_id => $custom_field) {
    	if ($custom_field["title"] == "rates") {
    		$rates = $custom_field["options"];
    		$rates_custom_field_id = $custom_field_id;
    		break;
    	}
    }
   
    $resource_api = new ER_Inbound_Resource_API($rates, $rates_custom_field_id);
    
    foreach($optimum_roomset as $id => $room) {
    	if($id == "can_accommodate") continue;
    	$resource = new Resource($id);
    
    	$persons_filter_conditions = array();
    	
    	//The maximum persons allowed from the requirements is also available to select as a persons rate, so add it to the array
    	$persons_filter_conditions[] = $room["max_occupancy"];
    	
    	//Reorganize price filters for persons to conform to new persons filtering design (display prices for reservation with
    	//specified maximum nuymber of persons allowed)
    	if(!empty($resource -> fitlers))
    	{
    		foreach ($resource -> fitlers as $filter) {
    			if ($filter["type"] == "pers") {
    				
    				
    			    if(($filter["cond"] == 1) || ($filter["cond"] > $room["max_occupancy"] )) {
    					//No need to process the filter for 1 person, since it will be processed in succeeding checks. Also don't process the filters for \
    					//persons number above or same as the maximum allowed persons in the room since it will be added at the end anyway
    				} else {
    					
    					//Since the filter for persons (["type"] == "pers") looks at ["cond"] and above, add the decremented ["cond"] (and below) for display 
    					//on frontend, to make more sense to the end user
    				    $persons_filter_conditions[] = $filter["cond"] - 1; 
    				}
    				
    			}
    		}
    	}
    
    	//print "DEV: persons filter conditions is: <pre>"; print_r($persons_filter_conditions); print "</pre>";
    
    	$board_rates = $resource_api -> get_all_board_rates_for_resource($id, 1, $arrival, $departure, $persons_filter_conditions[0]);
    	
    	$reserve_button_rowspan_for_optimum_roomset += count($board_rates) * count($persons_filter_conditions);
    	//echo "after room $id <br> count for reserve button is: " . $reserve_button_rowspan_for_optimum_roomset;
    	
    }
    
    //Do a quick calculation of rowspan for reserve button for relevant roomset
    $all_usable_rooms = $h -> get_usable_rooms();
    
    $reserve_button_rowspan_for_relevant_roomset = 0;
    
    foreach($all_usable_rooms as $id => $room) {
    	if($id == "can_accommodate") continue;
    	$resource = new Resource($id);
    
    	$persons_filter_conditions[$id] = array();
    	
    	//The maximum persons allowed from the requirements is also available to select as a persons rate, so add it to the array
    	$persons_filter_conditions[$id][] = $room["max_occupancy"];
    	
    	if(!empty($resource -> fitlers))
    	{
    		foreach ($resource -> fitlers as $filter) {
    			if ($filter["type"] == "pers") {
    				
    				if(($filter["cond"] == 1) || ($filter["cond"] > $room["max_occupancy"] )) {
    					//No need to process the filter for 1 person, since it will be processed in succeeding checks. Also don't process the filters for \
    					//persons number above or same as the maximum allowed persons in the room since it will be added at the end anyway
    				} else {
    					
    					//Since the filter for persons (["type"] == "pers") looks at ["cond"] and above, add the decremented ["cond"] (and below) for display 
    					//on frontend, to make more sense to the end user
    				    $persons_filter_conditions[$id][] = $filter["cond"] - 1; 
    				}
    				
    				
    			}
    		}
    	}
    	
    	//print "DEV: persons filter conditions for room  $id is: <pre>"; print_r($persons_filter_conditions); print "</pre>";
    	
    	$board_rates = $resource_api -> get_all_board_rates_for_resource($id, 1, $arrival, $departure, $persons_filter_conditions[$id][0]);
    	
    	$reserve_button_rowspan_for_relevant_roomset += count($board_rates) * count($persons_filter_conditions[$id]);    	
    
    }
   
?>
    
    