<?php
    
    //print "GET is: <pre>";    print_r ($_GET);    print "</pre>";

    $plugins_dir = ABSPATH . 'wp-content/plugins/';

    require_once $plugins_dir . "easyreservations/rest/ServiceClasses/search-rest-service.php";
    require_once $plugins_dir . "easyreservations/rest/ServiceClasses/constants.php";

	if (empty($_GET["destination"])) {
		echo "<br>Destination is not set. Please set the destination and try again.";
		return;
	}    
	
    if (empty($_GET["arrival"])) {
    	echo "<br>Arrival date is not set. Please go back and set the date of arrival.";
    	return;
    }
      
    
    //Date with slashes (aa/bb/cccc) is seen as mm-dd-yyyy by strtotime
    //Date with  dashes (aa-bb-cccc) is seen as dd-mm-yyyy by strtotime
    $arrival_with_dashes = str_replace("/", "-", $_GET["arrival"]);
    $departure_with_dashes = str_replace("/", "-", $_GET["departure"]);    
    
    if(strtotime($arrival_with_dashes) < strtotime('now')) {    	
    	echo "<br>Arrival is in the past, this is not allowed. Please go back and pick another arrival date";
    	return;
    }
        
    if(strtotime($arrival_with_dashes) > strtotime($departure_with_dashes)) {
    	echo "<br>You can't have departure before arrival or at the same time as arrival, please go back and pick another departure date.";
    	return;
    }
    
    if (empty($_GET["adults"])) {
    	echo "<br>Number of people is not set. Please set the number of people for accommodation.";
    	return;
    }
    
    $rs = new Search_Rest_Service();
    
    $groups = array();
    $people = ""; 
    
    foreach ($_GET["adults"] as $key => $adults) {
    	$groups[] = array("adults" => $_GET["adults"][$key],
    			          "childs" => $_GET["childs"][$key]);
    	
    	$people .= ";" . $_GET["adults"][$key] . "," . $_GET["childs"][$key];
    
    }
    
    $people = substr($people, 1, strlen($people) - 1);
    
    //Form dates for PHP method that uses dates in SQL scripts
    $arrExp = explode("/", $_GET["arrival"]);
    $depExp = explode("/", $_GET["departure"]);    
    $arrival = $arrExp[2] . "-" . $arrExp[1] . "-" . $arrExp[0] . " 00:00:00";
    $departure = $depExp[2] . "-" . $depExp[1] . "-" . $depExp[0] . " 00:00:00";
    
    
    $hs = $rs -> get_all_hotels_that_can_accommodate(
        array("destination_type" => $_GET["destination_type"],  
        	  "destination_id" => $_GET["destination_id"]), 
    		  $groups, 
    		  $arrival, 
    		  $departure);
    
    //print "   - hs: <pre>";    print_r ($hs);    print "</pre>";
    
    echo "<br>Search Results";
    
    foreach ( $hs as $hotel_accommodation) {

    	    include "hotel-result-square.php";
        	
    }        
    
  
?>