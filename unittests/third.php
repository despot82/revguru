<?php
class Example extends PHPUnit_Extensions_SeleniumTestCase
{
  protected function setUp()
  {
    $this->setBrowser("*chrome");
    $this->setBrowserUrl("http://localhost/");
  }

  public function testMyTestCase() 
  {
    $this -> open("/wp/intercontinental/wp-login.php?loggedout=true");
    $this -> type("id=user_login", "vlado");
    $this -> type("id=user_pass", "1234567");
    $this -> click("id=wp-submit");
    $this -> waitForPageToLoad("30000");
  }
}
?>