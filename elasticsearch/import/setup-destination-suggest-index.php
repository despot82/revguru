<?php 

ini_set('display_errors', '1');
error_reporting(E_ALL);

require '../vendor/autoload.php';


$client = Elasticsearch\ClientBuilder::create() -> build();

// Index Settings
$indexParams['index']  = 'er';
$indexParams['body']['settings'] = array(
    "index" => array(
      "number_of_replicas" => 0,
      "number_of_shards" => 1,
      "analysis" => array(
			"tokenizer" => array(
				"ngram_tokenizer" => array(
					"type" => "edgeNGram",
					"min_gram" => "2",
					"max_gram" => "15",
					"token_chars" => [ "letter", "digit" ]
				)
			),
			"analyzer" => array(
			    //"index_ngram_analyzer" => array(
				"analyzer" => array(
					"type" => "custom",
					"tokenizer" => "ngram_tokenizer",
					"filter" => [ "lowercase" ]
				),
				"search_term_analyzer" => array(
					"type" => "custom",
					"tokenizer" => "keyword",
					"filter" => "lowercase"
				)
			)
       )
    )
  );

// Hotel Document Type Mapping
$hotelMappings = array(    
    'properties' => array(
        'hotelName' => array(
            'type' => 'string'
        ),
        'geo_name' => array(
        	'type' => 'string',
            //'index_analyzer' => 'index_ngram_analyzer',
			'analyzer' => 'analyzer',
    		'search_analyzer' => 'search_term_analyzer'
        )
    )
);


// City Document Type Mapping
$cityMappings = array(
		'properties' => array(
				'cityName' => array(
						'type' => 'string'
				),				
				'latitude' => array(
						'type' => 'double'
				),
				'longitude' => array(
						'type' => 'double'
				),
				'timezone' => array(
						'type' => 'float'
				),
				'countryId' => array(
						'type' => 'integer'
				),
				'stateId' => array(
						'type' => 'integer'
				),
				'active' => array(
						'type' => 'integer'
				),
				
				'geo_name' => array(
						'type' => 'string',
						//'index_analyzer' => 'index_ngram_analyzer',
			            'analyzer' => 'analyzer',
						'search_analyzer' => 'search_term_analyzer'
				)
		)
);


// State Document Type Mapping
$stateMappings = array(
		'properties' => array(
				'stateName' => array(
						'type' => 'string'
				),
				'stateCode' => array(
						'type' => 'string'
				),
				'countryId' => array(
						'type' => 'integer'
				),				
				'active' => array(
						'type' => 'integer'
				),
				'geo_name' => array(
						'type' => 'string',
						//'index_analyzer' => 'index_ngram_analyzer',
			            'analyzer' => 'analyzer',
						'search_analyzer' => 'search_term_analyzer'
				)
		)
);


// Region Document Type Mapping
$regionMappings = array(
		'properties' => array(
				'regionName' => array(
						'type' => 'string'
				),
				'countryId' => array(
						'type' => 'integer'
				),
				'regionTypeId' => array(
						'type' => 'integer'
				),
				'geo_name' => array(
						'type' => 'string',
						//'index_analyzer' => 'index_ngram_analyzer',
			            'analyzer' => 'analyzer',
						'search_analyzer' => 'search_term_analyzer'
				)
		)
);


// Country Document Type Mapping
$countryMappings = array(
		'properties' => array(
				'currencyCode' => array(
						'type' => 'string'
				),
				'countryCode' => array(
						'type' => 'string'
				),
				'countryName' => array(
						'type' => 'string'
				),
				'continentName' => array(
						'type' => 'string'
				),
				'active' => array(
						'type' => 'integer'
				),
				'geo_name' => array(
						'type' => 'string',
						//'index_analyzer' => 'index_ngram_analyzer',
			            'analyzer' => 'analyzer',
						'search_analyzer' => 'search_term_analyzer'
				)
		)
);


/* Point of interest Document Type Mapping
$pointOfInterestMappings = array(
		'properties' => array(
				
		)
);
$indexParams['body']['mappings']['pointofinterest'] = $pointOfInterestMappings;
*/


$indexParams['body']['mappings']['hotel'] = $hotelMappings;
$indexParams['body']['mappings']['city'] = $cityMappings;
$indexParams['body']['mappings']['region'] = $regionMappings;
$indexParams['body']['mappings']['state'] = $stateMappings;
$indexParams['body']['mappings']['country'] = $countryMappings;

// Create the index
try
{
	$client -> indices() -> create($indexParams);
}
//catch (Guzzle\Http\Exception\BadResponseException $e) {
catch (Exception $e) {
	echo 'Uh oh! ' . $e->getMessage();
}


return;



?>