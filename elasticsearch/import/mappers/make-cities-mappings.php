<?php 

require '../../vendor/autoload.php';

$client = new Elasticsearch\Client();
$indexParams['index']  = 'easyreservations';

// Index Settings
//$indexParams['body']['settings']['number_of_shards']   = 3;
//$indexParams['body']['settings']['number_of_replicas'] = 2;

// Index Mappings
$cityMappings = array(    
    'properties' => array(
        'cityName' => array(
            'type' => 'string'
        ),
    	'location' => array(
    	    'type' => 'geo_point'
    	),		
    	'timezone' => array(
    			'type' => 'integer'
    	),    		
    	'countryId' => array(
    			'type' => 'integer'
    	),    		
    	'stateId' => array(
    			'type' => 'integer'
    	),    		
    	'active' => array(
    			'type' => 'integer'
    	),
    	'name_suggest' => array( 'type' => 'completion'
    	)
    )
);



$indexParams['body']['mappings']['city'] = $cityMappings;

// Create the index
$client -> indices() -> create($indexParams);


?>