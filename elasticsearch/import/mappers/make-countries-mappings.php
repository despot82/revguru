<?php 

require '../../vendor/autoload.php';

$client = new Elasticsearch\Client();
$indexParams['index']  = 'easyreservations';

// Index Settings
//$indexParams['body']['settings']['number_of_shards']   = 3;
//$indexParams['body']['settings']['number_of_replicas'] = 2;

// Example Index Mapping
$countryMappings = array(    
    'properties' => array(
        'name' => array(
            'type' => 'string'
        ),
        'city' => array(
            'type' => 'string'
        ),
    	'name_suggest' => array( 'type' => 'completion'
    	)
    )
);

$indexParams['body']['mappings']['hotel'] = $hotelMappings;

// Create the index
$client->indices()->create($indexParams);


?>