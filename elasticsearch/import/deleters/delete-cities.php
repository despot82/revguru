<?php
    
	$params = array();

	include  ABSPATH . 'elasticsearch/vendor/autoload.php';

	try {
        $client = Elasticsearch\ClientBuilder::create() -> build();
	} catch(Exception $e) {
		print "exception is: <pre>"; print_r($e); print "</pre>";
	}
	
	$searchParams = array();
	$searchParams['index'] = 'er';
	$searchParams['type']  = 'city';
	$searchParams['size'] = 10000;
	
	
	try {
	    $result = $client -> search($searchParams);
	} catch (Exception $e) {
		print "DEV: exception during -> search method execution<pre>"; print_r ($e); print "</pre>";
	}
	
	//print "all first cities are:<pre>"; print_r ($result); print "</pre>"; 
	
	foreach ($result["hits"]["hits"] as $i => $res) {
		$params ['body'][] = array(
			'delete' => array(
				'_index' => 'er',
				'_type' => 'city',
				'_id' => $res["_id"]
				
			)
		);

		// Every so documents stop and send the bulk request
		if ($i % 4000 == 1) {
			$responses = $client -> bulk($params);

			// erase the old bulk request
			$params = array();

			// unset the bulk response when you are done to save memory
			unset($responses);
		}
		
		 
	}
	
	//Do the one last bulk
	$client -> bulk($params);
	
	return;
	
	
?>