<?php

	require_once ABSPATH . 'elasticsearch/vendor/autoload.php';

	ini_set('max_execution_time', 60 * 60 * 4);
	
	$client = Elasticsearch\ClientBuilder::create() -> build();
    
    global $wpdb;
    
    $sql =  " SELECT bl.blog_id, ih.Hotel_Name, concat(ci.cityName, ', ', st.stateName, ', ', co.countryName) as location "
    	  . " FROM " . $wpdb -> prefix . "blogs bl" 
    	  . " LEFT JOIN imported_hotels ih on(ih.Hotel_Id = bl.hotel_import_id) "
		  . " LEFT JOIN city ci using(cityId) "
		  . " LEFT JOIN state st using(stateId) "
		  . " LEFT JOIN country co on(ci.countryId = co.countryId) WHERE ci.countryId in (12, 27)";
	
	echo $sql;	  
		  
    $results = $wpdb -> get_results($sql);
    
    print "DEV: results are: <pre>"; print_r ($results); print "</pre>";
    
    foreach ($results as $result)
    {    	
	    $name = $result -> Hotel_Name;  

        echo "Currently at indexation of hotel: " . $name . "<br/>";		
    	
    	$params = array();
    	$params['index'] = 'er';
    	$params['type']  = 'hotel';
    	$params['id']    = $result -> blog_id;
    	
    	$params['body']  = array(
    			'geo_name' => $name . ", " . $result -> location,
    			'hotelName' => $name);
    	                                             
    	$ret = $client -> index($params);
    
    }
    
    echo "Done";
    
    
?>