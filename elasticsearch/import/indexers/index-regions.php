<?php
    
    require_once ABSPATH . 'elasticsearch/vendor/autoload.php';

	ini_set('max_execution_time', 60 * 60 * 4);
	
	$client = Elasticsearch\ClientBuilder::create() -> build();
    
    global $wpdb;
    
    $sql =  " SELECT regionId, regionName, countryId, regionTypeId, country.countryName as location "
    	  . " FROM region "
    	  . " LEFT JOIN country using(countryId) WHERE region.countryId in (12, 27)";
    
    //echo $sql . "<br/>";
    
    $results = $wpdb -> get_results($sql);
    
    foreach ($results as $result)
    {    	
    	echo "Currently at indexation of region: " . $result -> regionName . "<br/>";    	
    
    	$params = array();
    	$params['index'] = 'er';
    	$params['type']  = 'region';
    	$params['id']  = $result -> regionId;
    	
    	$params['body']  = array(
    			'regionName' => $result -> regionName,
    			'countryId' => $result -> countryId,
    			'regionTypeId' => $result -> regionTypeId,
    			'geo_name' => $result -> regionName . ", " . $result -> location 
    			              );
    
    	
    	try
    	{
    	    $ret = $client -> index($params);
    	}
    	//catch (Guzzle\Http\Exception\BadResponseException $e) {
    	catch (Exception $e) {
    		echo 'Uh oh! ' . $e->getMessage();
    	}    	
    	
    	//print "<pre>"; print_r ($ret); print "</pre>";
    
    }
    
    echo "Done";
    
    
?>