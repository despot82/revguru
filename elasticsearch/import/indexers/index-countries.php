<?php
    
    require_once ABSPATH . 'elasticsearch/vendor/autoload.php';

	ini_set('max_execution_time', 60 * 60 * 4);
	
	$client = Elasticsearch\ClientBuilder::create() -> build();
    
    global $wpdb;
    
    $sql =  " SELECT countryId, currencyCode, countryCode, countryName, continentName, active "
    	  . " FROM country WHERE countryId in (12, 27)";    	  
    	  
    
    //echo $sql . "<br/>";
    
    $results = $wpdb -> get_results($sql);
    
    foreach ($results as $result)
    {    	
    	echo "Currently at indexation of country: " . $result ->countryName . "<br/>";
    	
    	$params = array();
    	$params['index'] = 'er';
    	$params['type']  = 'country';
    	$params['id']  = $result -> countryId;
    	
    	$params['body']  = array(
    			'currencyCode' => $result -> currencyCode,
    			'countryCode' => $result -> countryCode,
    			'countryName' => $result -> countryName,
    			'continentName' => $result -> continentName,
    			'active' => $result -> active,    			
    			'geo_name' => $result -> countryName 
    			              );
    	
    	try
    	{
    	    $ret = $client -> index($params);
    	}
    	//catch (Guzzle\Http\Exception\BadResponseException $e) {
    	catch (Exception $e) {
    		echo 'Uh oh! ' . $e->getMessage();
    	}    	
    	
    	//print "<pre>";
    	//print_r ($ret);
    	//print "</pre>";
    
    }
    
    echo "Done";
    
    
?>