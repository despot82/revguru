<?php
    
	require_once ABSPATH . 'elasticsearch/vendor/autoload.php';

	ini_set('max_execution_time', 60 * 60 * 4);
	
	$client = Elasticsearch\ClientBuilder::create() -> build();
    
    global $wpdb;
    
    $sql =  " SELECT cityId, cityName, concat(stateName, ', ', country.countryName) as location, latitude, longitude, timezone, city.countryId as countryId, city.stateId as stateId, city.active as active "
    	  . " FROM city "    	  
    	  . " LEFT JOIN state using(stateId) "
    	  . " LEFT JOIN country on(city.countryId = country.countryId) "
    	  . " WHERE city.countryId in (12, 27)";
    	 
    echo $sql;
    	  
    $results = $wpdb -> get_results($sql);
    
    print "DEV: results are: <pre>"; print_r ($results); print "</pre>";
    
    foreach ($results as $result)
    {    	
    	echo "Currently at indexation of city: " . $result -> cityName . "<br/>";
       
    	$params = array();
    	$params['index'] = 'er';
    	$params['type']  = 'city';
    	$params['id']  = $result -> cityId;
    	
    	$params['body']  = array(
    			'cityName' => $result -> cityName,
    			'latitude' => $result -> latitude,
    			'longitude' => $result -> longitude,
    			'timezone' => $result -> timezone,
    			'countryId' => $result -> countryId,
    			'stateId' => $result -> stateId,
    			'active' => $result -> active,    			
    			'geo_name' => $result -> cityName . ", " . $result -> location 
    			              );
    
    	
    	try
    	{
    	    $ret = $client -> index($params);
    	}
    	//catch (Guzzle\Http\Exception\BadResponseException $e) {
    	catch (Exception $e) {
    		echo 'Uh oh! ' . $e->getMessage();
    	}    	
    	
    	//print "<pre>"; print_r ($ret); print "</pre>";    
    }
	
	echo "Done";
    
?>