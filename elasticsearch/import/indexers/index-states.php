<?php
    
    require_once ABSPATH . 'elasticsearch/vendor/autoload.php';

	ini_set('max_execution_time', 60 * 60 * 4);
	
	$client = Elasticsearch\ClientBuilder::create() -> build();
    
    global $wpdb;
    
    $sql =  " SELECT stateId, stateName, stateCode, country.countryName as location, state.active, countryId "
    	  . " FROM state "    	  
    	  . " LEFT JOIN country USING(countryId) WHERE state.countryId in (12, 27)";
    	  
    
    //echo $sql . "<br/>";
    
    $results = $wpdb -> get_results($sql);
    
    foreach ($results as $result)
    {    	
    	echo "Currently at indexation of state: " . $result -> stateName . "<br/>";
    	
    	$params = array();
    	$params['index'] = 'er';
    	$params['type']  = 'state';
    	$params['id']  = $result -> stateId;
    	
    	$params['body']  = array(
    			'stateName' => $result -> stateName,    	
    			'stateCode' => $result -> stateCode,
    			'countryId' => $result -> countryId,    			
    			'active' => $result -> active,    			
    			'geo_name' => $result -> stateName . ", " . $result -> location 
    			              );
    	
    	try
    	{
    	    $ret = $client -> index($params);
    	}
    	//catch (Guzzle\Http\Exception\BadResponseException $e) {
    	catch (Exception $e) {
    		echo 'Uh oh! ' . $e->getMessage();
    	}    	
    	
    	//print "<pre>"; print_r ($ret); print "</pre>";
    
    }
    
    echo "Done";
    
?>