<?php

	require '../../vendor/autoload.php';

	ini_set('memory_limit', '1024M');
	ini_set('max_execution_time', 60 * 60 * 2);
	
    $client = Elasticsearch\ClientBuilder::create() -> build();

    //Add a document
    /*
	$params = array();
	$params['body']  = array('name' => 'NameOfDocAddedThroughPHP_2');
	$params['index'] = 'hotels';
	$params['type']  = 'hotel';
	//$params['id']    = 'my_id';
	$ret = $client -> index($params);
	print "<pre>";
	print_r ($ret);
	print "</pre>";
	*/
	
	
	//Return a document with the given ID
	/*
	$getParams = array();
	$getParams['index'] = 'hotels';
	$getParams['type']  = 'my_type';
	$getParams['id']    = 'my_id';
	$retDoc = $client->get($getParams);
	*/
	
	
	//Search a document
	$searchParams = array();
	$searchParams['index'] = 'er';
	//$searchParams['type']  = 'hotel';
	//$searchParams['body']['query']['match_all'] = new stdClass();
	//$searchParams['body']['query']['match']['testField'] = 'abc';
	//$searchParams['body']['query']['match']['_id'] = 1;	
	$searchParams['size'] = 1200000;
	$retDoc = $client -> search($searchParams);
	
	print "<pre>";
	print_r ($retDoc);
	print "</pre>";
	

?>

