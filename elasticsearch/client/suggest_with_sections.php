<?php
     
	require '../vendor/autoload.php';

    //$client = new Elasticsearch\Client();
	$client = Elasticsearch\ClientBuilder::create() -> build();
			
    if (!empty($_GET["text"])) {    	
    	$text = $_GET["text"];    	
    } else {
    	$text = $_POST["text"];
    }
    
    $json = '{
    "query":
    {
        "query_string":
        {
    		"query": "' . $text . '",
            "fields": ["geo_name"]
        }
    },
    "aggs":
    {
    	"top-types":
    	{
    		"terms":
    		{
    			"field": "_type"
    		},
    		"aggs":
    		{
    			"top_docs":
    			{
    				"top_hits":
    				{
    					"sort":
    					[
    					{
    				        "_type":
    						{
    							"order": "desc"
    						},
    						"_score":
    						{
    							"order": "desc"
    						}
    				        
    					}
    					],
    				    "_source": {
                            "include": [
                                "geo_name"    
                            ]
                        },
    				    "highlight":
					    {
					        "pre_tags":
					    	[
					    	    "<strong>"
					    	],
					        "post_tags":
					    	[
					    	    "</strong>"
					    	],
					        "fields":
					        {
					            "geo_name": {}
					        }
					    },
    					"size": 5
    				}
    			}
    		}
    	}
    },
    "highlight":
    {
        "pre_tags":
    	[
    	    "<strong>"
    	],
        "post_tags":
    	[
    	    "</strong>"
    	],
        "fields":
        {
            "geo_name": {}
        }
    }
    }';
    
    
    
    
    
   
	$params['index'] = 'er';
	//$params['type']  = 'hotel';
	$params['body']  = $json;
	
	
	try
	{
	    $results = $client -> search($params);
	}
	catch(Exception $e)
	{
		echo $e;
	}
	
	//echo $results;
	
	$ret = array();
	
	
	/*
	print "<pre>";
	print_r ($results);
	//print_r ($results["aggregations"]);
	print "</pre>";
	*/
	
	
	$debug_mode = false;
	if($debug_mode)
	foreach ($results["hits"]["hits"] as $result) {
		if($result["_type"] == "country") {
			echo "<br>country";
		}
		
		if($result["_type"] == "state") {
			echo "<br>state";
		}
		
		if($result["_type"] == "region") {
			echo "<br>region";
		}
		
		if($result["_type"] == "city") {
			echo "<br>city";
		}
		
		if($result["_type"] == "hotel") {
			echo "<br>hotel";
		}
		
		
		
		
	}
	
	//Go through the result and select first 3 countries, first 3 states, regions, cities and hotels. 3 of each if any.
	$res = array();
	
	foreach ($results["aggregations"]["top-types"]["buckets"] as $bucket) {
		
		if($bucket["key"] == "country") {
		    $res[0] = $bucket;
		}
		
		if($bucket["key"] == "state") {
			$res[1] = $bucket;
		}
		
		if($bucket["key"] == "region") {
			$res[2] = $bucket;
		}
		
		if($bucket["key"] == "city") {
			$res[3] = $bucket;
		}
		
		if($bucket["key"] == "hotel") {
			$res[4] = $bucket;
		}
		
		
		
	}
	
	
	$ret = array();
	
	for($i = 0; $i < 5; $i++) {
		if(!empty($res[$i])) {
		    $bucket = $res[$i];
		    $ret = array_merge($ret, $bucket["top_docs"]["hits"]["hits"]);
		}
	}
	
	
	

	echo json_encode($ret);
	
	//print "<pre>";	print_r ($ret);	print "</pre>";

?>