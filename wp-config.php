<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%ByJ>1*dd1ejY!h/orPAfrh>J+.C|N_Y:Sl(:cGOWf&)C9*otx@> VUY{C8>-p--');
define('SECURE_AUTH_KEY',  '=pwwQeuit+-x>MpNgsGZL2f^jNXF~fIh9+j^QU0VC_9B3waAAW^D,1(NU1g/{piM');
define('LOGGED_IN_KEY',    '2Ic|G/unoi]$J%JoG_[bm~O-v~$lAd-X[m)kl19WA}$c:sZ(^^3[#6=1RY-N1lc-');
define('NONCE_KEY',        'I{[~Dovl2JG>Y6V9V;K3Z-:p7IG+<Tta;[@T7,2ZD^2Hd*Ec>ey&?zx/SB|-;W~i');
define('AUTH_SALT',        'gZ$S@X/9x}ItZQ;:;9kR[VD8(-m8G=T{Ioihq!)+Y{#FRgv+0<r(ixq-$E<c~+`|');
define('SECURE_AUTH_SALT', 'fxI9-ziJ>eFFZC9Eu5BaPaHC9-pq9a%44ND[|3&e>pA?+~mP>4u]Wj1{5KbI3hVf');
define('LOGGED_IN_SALT',   'aKr)=;7|lGO*.280{yZn.+e|1aTpa^) Vw^h:V5j7&7DSV4>6I.+WV#*r(O;!)}f');
define('NONCE_SALT',       'Sph`CNG3L&J;3Y +[&P6Zr+a8u?wYJx$ng 8dstN%mJuY%:h.Gttia5&j<D?:HfB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
//define('WP_DEBUG', true);
define('WP_DEBUG', false);

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/wp/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('WP_CACHE', 'false');
