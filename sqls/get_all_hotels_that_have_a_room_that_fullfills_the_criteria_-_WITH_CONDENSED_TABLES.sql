SELECT room.hotelId,  room.ID as room_resource_id,  roomcount.meta_value-count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM all_rooms as room   #all posts (resources(rooms))
JOIN all_postmeta as roomcount on(room.hotelId = roomcount.hotelId
                                  AND room.ID = roomcount.post_id 
                                  AND roomcount.meta_key = 'roomcount'
                                  AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
LEFT JOIN all_postmeta as requirements on(room.hotelId = requirements.hotelId
                                     AND room.ID = requirements.post_id 
                                     AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN all_postmeta as filters on(room.ID = filters.post_id 
                                     AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN all_reservations reservations on(room.hotelId = reservations.hotelId
                                           AND reservations.room = room.id 
                                           AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
											       (reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
JOIN wp_blogs hotels on(hotels.blog_id = room.hotelId)
WHERE 1 = 1
AND hotels.blog_id in (4)
#AND (roomcount.meta_value-count(distinct reservations.roomnumber) > 0)
GROUP BY room_resource_id
HAVING available>0
ORDER BY room.hotelId asc, room_resource_id asc;


