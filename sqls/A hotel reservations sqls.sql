#hotel reservations table
select * from wp_8_reservations where 1=1  order by id desc;

#hotel posts table
select * from wp_8_posts where 1=1
and post_type='groupedreservation' order by ID desc;

#hotel postmeta table
select * from wp_8_postmeta where 1=1 order by meta_id desc;

#reset grouped reservations
DELETE FROM wp_8_posts WHERE post_type='groupedreservation' AND ID > 19;
SELECT @ai := (SELECT MAX(ID) + 1 FROM wp_8_posts);
SET @qry = CONCAT('ALTER TABLE wp_8_posts AUTO_INCREMENT=', @ai);
PREPARE stmt FROM @qry; EXECUTE stmt;

#reset reservations
DELETE FROM wp_8_reservations WHERE 1=1;
ALTER TABLE wp_8_reservations AUTO_INCREMENT = 1;




