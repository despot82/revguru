SELECT room.ID, roomcount.meta_value-count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM wp_4_posts as room   #all posts (resources(rooms))
JOIN wp_4_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_4_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_4_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_4_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') ))  #join with reservations in the period, later for total number of these to be subtracted from total roomcount
WHERE 1=1
ORDER BY hotelId asc;




#select * from wp_4_reservations;





