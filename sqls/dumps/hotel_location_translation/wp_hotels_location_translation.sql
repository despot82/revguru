-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: wp
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hotels_location_translation`
--

DROP TABLE IF EXISTS `hotels_location_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotels_location_translation` (
  `hotel_id` int(4) DEFAULT NULL,
  `hotel_name` varchar(45) DEFAULT NULL,
  `geography_id` int(4) DEFAULT NULL,
  `city_name` varchar(45) DEFAULT NULL,
  `hotels_location_translationcol` varchar(45) NOT NULL,
  PRIMARY KEY (`hotels_location_translationcol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotels_location_translation`
--

LOCK TABLES `hotels_location_translation` WRITE;
/*!40000 ALTER TABLE `hotels_location_translation` DISABLE KEYS */;
INSERT INTO `hotels_location_translation` VALUES (1,'Anand Resort.TERM',530,'Vasai','1'),(10,'Moneta',524,'Pune','10'),(100,'Happyland',523,'Mumbai','100'),(101,'Paris Residency',523,'Mumbai','101'),(102,'Parth Hotel',532,'Navi Mumbai','102'),(103,'The Metro Park Hotel',474,'Ahmedabad','103'),(104,'Hotel Om Palace',474,'Ahmedabad','104'),(105,'Hotel Canada Palace',474,'Ahmedabad','105'),(106,'Hotel New Balaji',474,'Ahmedabad','106'),(107,'Hotel Orchid.TERM',474,'Ahmedabad','107'),(108,'Hotel Grand Ambiance',474,'Ahmedabad','108'),(109,'Hotel Skylon.TERM',474,'Ahmedabad','109'),(11,'Aishwarya Lodging',527,'Pimpri - Chinchwad','11'),(110,'Rajmahal',528,'Nashik','110'),(111,'Vaishali',528,'Nashik','111'),(112,'Silver Inn',528,'Nashik','112'),(113,'Hotel Mann Residency',474,'Ahmedabad','113'),(114,'Hotel Radiant',474,'Ahmedabad','114'),(115,'Pankaj Executive',661,'Old Mahabaleshwar','115'),(116,'Hotel Blue Heaven.TERM',661,'Old Mahabaleshwar','116'),(117,'Hotel Anupam',661,'Old Mahabaleshwar','117'),(118,'Hotel R K Palace',661,'Old Mahabaleshwar','118'),(119,'Hotel Inter plaza',661,'Old Mahabaleshwar','119'),(12,'Orchard Hotel',524,'Pune','12'),(120,'Hotel Royal Residency',661,'Old Mahabaleshwar','120'),(121,'Hotel Mann Palace',661,'Old Mahabaleshwar','121'),(122,'Hotel Sona palace',661,'Old Mahabaleshwar','122'),(123,'Hotel Rudra Mahal',474,'Ahmedabad','123'),(124,'Hotel Sunlight',474,'Ahmedabad','124'),(125,'Hotel The Royal Plaza',474,'Ahmedabad','125'),(126,'Hotel Keshav.TERM',474,'Ahmedabad','126'),(127,'De Ecobiz Hotel',474,'Ahmedabad','127'),(128,'Hotel Park Residency A Boutique Hotel',474,'Ahmedabad','128'),(129,'Hotel Konark Palace',474,'Ahmedabad','129'),(13,'Poonam Hotel',524,'Pune','13'),(130,'Hotel Classic Inn',474,'Ahmedabad','130'),(131,'Hotel Kailash',480,'Junagadm','131'),(132,'Hotel Krishna',480,'Junagadm','132'),(133,'Hotel Shiv Sadan',480,'Junagadm','133'),(135,'Hotel Manorath',528,'Nashik','135'),(136,'Cosmos Luxury Service Apartments Kharadi',524,'Pune','136'),(137,'EQC HOTEL 18',524,'Pune','137'),(138,'EQC TEST HOTEL2',524,'Pune','138'),(14,'Om Sai Palace.TERM',524,'Pune','14'),(15,'Lakme Executive',524,'Pune','15'),(16,'The Citiotel.TERM',524,'Pune','16'),(17,'Orchard Resort',524,'Pune','17'),(18,'J M Four.TERM',524,'Pune','18'),(19,'Paras.TERM',524,'Pune','19'),(2,'Moss Resort.TERM',523,'Mumbai','2'),(20,'Ratnalok.TERM',524,'Pune','20'),(21,'Shipra Regency',527,'Pimpri - Chinchwad','21'),(22,'Suraj Lodge',524,'Pune','22'),(23,'Royal Palace',524,'Pune','23'),(24,'Jagannath Hotel',524,'Pune','24'),(25,'Royalty.TERM',524,'Pune','25'),(26,'Sagar Inn.TERM',524,'Pune','26'),(27,'Chandan Residency.TEST',524,'Pune','27'),(28,'Hotel Chandan Residency',524,'Pune','28'),(29,'Yash Residency',524,'Pune','29'),(3,'Vikrant Residency',527,'Pimpri - Chinchwad','3'),(30,'Hotel Annas',524,'Pune','30'),(31,'Hotel Yashraj Inn',524,'Pune','31'),(32,'Hotel Paras',527,'Pimpri - Chinchwad','32'),(33,'Hotel Haveli',524,'Pune','33'),(34,'Hotel Bhagyashree',524,'Pune','34'),(35,'Hotel New Kubers Residency',532,'Navi Mumbai','35'),(36,'Hotel Navi Mumbai',532,'Navi Mumbai','36'),(37,'Palms   Khalsa enterprises',532,'Navi Mumbai','37'),(38,'Hotel Rex Plaza',532,'Navi Mumbai','38'),(39,'Neelam Executive',524,'Pune','39'),(4,'Shiv Inn',527,'Pimpri - Chinchwad','4'),(40,'Agoda Test Hotel',524,'Pune','40'),(41,'Coconut Gove',524,'Pune','41'),(42,'Pratik Sai',524,'Pune','42'),(43,'Bhakti Park Inn',524,'Pune','43'),(44,'Gangotri Sai',524,'Pune','44'),(45,'Bandhan',524,'Pune','45'),(46,'Varsha Sai',524,'Pune','46'),(47,'Indrayani Motels',524,'Pune','47'),(48,'Muktangan',524,'Pune','48'),(49,'HMR Royal Inn',524,'Pune','49'),(5,'Vikrant Hotel',527,'Pimpri - Chinchwad','5'),(50,'Trimurti Pride',524,'Pune','50'),(51,'Honeydew Hospitality Apt Hadapsar.TERM',524,'Pune','51'),(52,'Honeydew Hospitality Apt Kharadi.TERM',524,'Pune','52'),(53,'Innovative Services Aundh',524,'Pune','53'),(54,'Innovative Services Hinjewadi',524,'Pune','54'),(55,'Style O Style Service Apt',524,'Pune','55'),(56,'Home At Business Service Apartment.TERM',524,'Pune','56'),(57,'Ravikant',524,'Pune','57'),(58,'Raj',524,'Pune','58'),(59,'Lokesh',524,'Pune','59'),(6,'Hotel Vikrant.ERROR',527,'Pimpri - Chinchwad','6'),(60,'Bhola',527,'Pimpri - Chinchwad','60'),(61,'Kamini',527,'Pimpri - Chinchwad','61'),(62,'Nanashree Grand',524,'Pune','62'),(63,'Samruddhi and Shreeinn Lodge',524,'Pune','63'),(64,'Vishal',524,'Pune','64'),(65,'Suraj Classic',524,'Pune','65'),(67,'Picasso Resort.TERM',523,'Mumbai','67'),(68,'K N Park Hotel',523,'Mumbai','68'),(69,'Nisarg Lounge',524,'Pune','69'),(7,'Rajlakshmi',527,'Pimpri - Chinchwad','7'),(70,'Nikhil Garden',524,'Pune','70'),(71,'Rajyog',524,'Pune','71'),(72,'Silver Oak Inn',524,'Pune','72'),(73,'Shivalin',524,'Pune','73'),(74,'Firangai Lodging',524,'Pune','74'),(75,'Maharaja Inn',524,'Pune','75'),(76,'Oasis Hotel',524,'Pune','76'),(77,'Western Park',524,'Pune','77'),(78,'Rajshree Lodging',528,'Nashik','78'),(79,'Sai Basera',528,'Nashik','79'),(8,'Plaza Place',527,'Pimpri - Chinchwad','8'),(80,'City Palace',528,'Nashik','80'),(81,'Radhika',528,'Nashik','81'),(82,'Vikrant Residency',528,'Nashik','82'),(83,'Riverdale Stay Service Apartment',524,'Pune','83'),(84,'Tamrind Studios Service Apartment.TERM',524,'Pune','84'),(85,'Woodside Meadows Service Apartment',524,'Pune','85'),(86,'Ashiyana Hospitality Service Apartment',524,'Pune','86'),(87,'Cherry Trees Service Apartment',524,'Pune','87'),(88,'Sky Homes Realty Service Apartment.TERM',524,'Pune','88'),(89,'GMA Guest House Service Apartment',524,'Pune','89'),(9,'Silver Seven Hotel.TERM',524,'Pune','9'),(90,'Hotel Kalasagar.TERM',527,'Pimpri - Chinchwad','90'),(91,'Innovative Services Kalyaninagar',524,'Pune','91'),(92,'Monika',527,'Pimpri - Chinchwad','92'),(93,'Sahil Residency',524,'Pune','93'),(94,'Sahyadhri',528,'Nashik','94'),(95,'Pooja International',528,'Nashik','95'),(96,'Sri Jai Palace',528,'Nashik','96'),(97,'Pacific Suites.TERM',524,'Pune','97'),(98,'Q3 Hotel.TERM',526,'Thane','98'),(99,'Karishma',523,'Mumbai','99');
/*!40000 ALTER TABLE `hotels_location_translation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-01 15:25:34
