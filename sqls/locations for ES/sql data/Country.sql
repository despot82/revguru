/*
Navicat MySQL Data Transfer

Source Server         : MariaDB2 - Master
Source Server Version : 50533
Target Server Type    : MYSQL
Target Server Version : 50533
File Encoding         : 65001

Date: 2014-12-03 16:32:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `Country`
-- ----------------------------
DROP TABLE IF EXISTS `Country`;
CREATE TABLE `Country` (
  `countryId` int(11) NOT NULL AUTO_INCREMENT,
  `currencyCode` varchar(3) DEFAULT NULL,
  `languageCode` varchar(5) DEFAULT NULL,
  `countryCode` varchar(45) NOT NULL,
  `countryName` varchar(100) NOT NULL,
  `continentName` varchar(45) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`countryId`),
  KEY `fk_Country_Currency1_idx` (`currencyCode`),
  KEY `fk_Country_Language1_idx` (`languageCode`),
  CONSTRAINT `fk_Country_Currency1` FOREIGN KEY (`currencyCode`) REFERENCES `Currency` (`currencyCode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Country_Language1` FOREIGN KEY (`languageCode`) REFERENCES `Language` (`languageCode`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of Country
-- ----------------------------
INSERT INTO `Country` VALUES ('-1', null, null, 'unknown', 'unknown', null, '0');
INSERT INTO `Country` VALUES ('1', null, null, 'JM', 'Jamaica', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('2', null, null, 'NF', 'Norfolk Island', 'Oceania', '1');
INSERT INTO `Country` VALUES ('3', 'XOF', null, 'SN', 'Senegal', 'Africa', '1');
INSERT INTO `Country` VALUES ('4', 'KZT', null, 'KZ', 'Kazakhstan', 'Asia', '1');
INSERT INTO `Country` VALUES ('5', null, null, 'MV', 'Maldives', 'Asia', '1');
INSERT INTO `Country` VALUES ('6', 'EUR', null, 'PF', 'French Polynesia', 'Oceania', '1');
INSERT INTO `Country` VALUES ('7', null, null, 'TO', 'Tonga', 'Oceania', '1');
INSERT INTO `Country` VALUES ('8', 'USD', null, 'US', 'U.S.A.', 'North America', '1');
INSERT INTO `Country` VALUES ('9', 'EUR', null, 'GR', 'Greece', 'Europe', '1');
INSERT INTO `Country` VALUES ('10', null, null, 'BM', 'Bermuda', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('11', null, null, 'LV', 'Latvia', 'Europe', '1');
INSERT INTO `Country` VALUES ('12', 'CNY', null, 'CN', 'China', 'Asia', '1');
INSERT INTO `Country` VALUES ('13', null, null, 'PY', 'Paraguay', 'South America', '1');
INSERT INTO `Country` VALUES ('14', null, null, 'AG', 'Antigua & Barbuda', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('15', 'TWD', null, 'TW', 'Taiwan', 'Asia', '1');
INSERT INTO `Country` VALUES ('16', 'NAD', null, 'NA', 'Namibia', 'Africa', '1');
INSERT INTO `Country` VALUES ('17', null, null, 'MF', 'Saint Martin', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('18', 'IDR', null, 'ID', 'Indonesia', 'Asia', '1');
INSERT INTO `Country` VALUES ('19', 'EUR', null, 'MT', 'Malta', 'Europe', '1');
INSERT INTO `Country` VALUES ('20', null, null, 'SZ', 'Swaziland', 'Africa', '1');
INSERT INTO `Country` VALUES ('21', null, null, 'VU', 'Vanuatu', 'Oceania', '1');
INSERT INTO `Country` VALUES ('22', 'AED', null, 'AE', 'United Arab Emirates', 'Middle East', '1');
INSERT INTO `Country` VALUES ('23', 'GBP', null, 'GB', 'United Kingdom', 'Europe', '1');
INSERT INTO `Country` VALUES ('24', 'BGN', null, 'BG', 'Bulgaria', 'Europe', '1');
INSERT INTO `Country` VALUES ('25', null, null, 'NG', 'Nigeria', 'Africa', '1');
INSERT INTO `Country` VALUES ('26', 'KWD', null, 'KW', 'Kuwait', 'Middle East', '1');
INSERT INTO `Country` VALUES ('27', 'INR', null, 'IN', 'India', 'Asia', '1');
INSERT INTO `Country` VALUES ('28', 'EUR', null, 'ES', 'Spain', 'Europe', '1');
INSERT INTO `Country` VALUES ('29', null, null, 'AL', 'Albania', 'Europe', '1');
INSERT INTO `Country` VALUES ('30', null, null, 'PE', 'Peru', 'South America', '1');
INSERT INTO `Country` VALUES ('31', 'ILS', null, 'IL', 'Israel', 'Middle East', '1');
INSERT INTO `Country` VALUES ('32', 'NZD', null, 'NZ', 'New Zealand', 'Oceania', '1');
INSERT INTO `Country` VALUES ('33', null, null, 'FK', 'Falkland Islands (Malvinas)', 'South America', '1');
INSERT INTO `Country` VALUES ('34', 'EUR', null, 'SI', 'Slovenia', 'Europe', '1');
INSERT INTO `Country` VALUES ('35', 'LTL', null, 'LT', 'Lithuania', 'Europe', '1');
INSERT INTO `Country` VALUES ('36', null, null, 'RE', 'Reunion', 'Africa', '1');
INSERT INTO `Country` VALUES ('37', null, null, 'KG', 'Kyrgyzstan', 'Asia', '1');
INSERT INTO `Country` VALUES ('38', null, null, 'PH', 'Philippines', 'Asia', '1');
INSERT INTO `Country` VALUES ('39', null, null, 'WS', 'Samoa', 'Oceania', '1');
INSERT INTO `Country` VALUES ('40', 'HKD', null, 'HK', 'Hong Kong', 'Asia', '1');
INSERT INTO `Country` VALUES ('41', null, null, 'GT', 'Guatemala', 'Central America', '1');
INSERT INTO `Country` VALUES ('42', null, null, 'LS', 'Lesotho', 'Africa', '1');
INSERT INTO `Country` VALUES ('43', null, null, 'BT', 'Bhutan', 'Asia', '1');
INSERT INTO `Country` VALUES ('44', null, null, 'SM', 'San Marino', 'Europe', '1');
INSERT INTO `Country` VALUES ('45', 'EUR', null, 'LU', 'Luxembourg', 'Europe', '1');
INSERT INTO `Country` VALUES ('46', null, null, 'AI', 'Anguilla', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('47', null, null, 'MA', 'Morocco', 'Africa', '1');
INSERT INTO `Country` VALUES ('48', 'KRW', null, 'KR', 'South Korea', 'Asia', '1');
INSERT INTO `Country` VALUES ('49', null, null, 'GL', 'Greenland', 'Europe', '1');
INSERT INTO `Country` VALUES ('50', 'EUR', null, 'FI', 'Finland', 'Europe', '1');
INSERT INTO `Country` VALUES ('51', null, null, 'UY', 'Uruguay', 'South America', '1');
INSERT INTO `Country` VALUES ('52', 'COP', null, 'CO', 'Colombia', 'South America', '1');
INSERT INTO `Country` VALUES ('53', 'SEK', null, 'SE', 'Sweden', 'Europe', '1');
INSERT INTO `Country` VALUES ('54', null, null, 'PW', 'Palau', 'Oceania', '1');
INSERT INTO `Country` VALUES ('55', null, null, 'TT', 'Trinidad and Tobago', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('56', 'UAH', null, 'UA', 'Ukraine', 'Europe', '1');
INSERT INTO `Country` VALUES ('57', 'CHF', null, 'CH', 'Switzerland', 'Europe', '1');
INSERT INTO `Country` VALUES ('58', null, null, 'SY', 'Syria', 'Middle East', '1');
INSERT INTO `Country` VALUES ('59', null, null, 'TN', 'Tunisia', 'Africa', '1');
INSERT INTO `Country` VALUES ('60', null, null, 'GP', 'Guadeloupe', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('61', 'GBP', null, 'GI', 'Gibraltar', 'Europe', '1');
INSERT INTO `Country` VALUES ('62', null, null, 'BA', 'Bosnia and Herzegovina', 'Europe', '1');
INSERT INTO `Country` VALUES ('63', null, null, 'ME', 'Montenegro', 'Europe', '1');
INSERT INTO `Country` VALUES ('64', 'EUR', null, 'PT', 'Portugal', 'Europe', '1');
INSERT INTO `Country` VALUES ('65', 'EUR', null, 'NL', 'Netherlands', 'Europe', '1');
INSERT INTO `Country` VALUES ('66', 'YEN', null, 'JP', 'Japan', 'Asia', '1');
INSERT INTO `Country` VALUES ('67', null, null, 'GU', 'Guam', 'Oceania', '1');
INSERT INTO `Country` VALUES ('68', null, null, 'BO', 'Bolivia', 'South America', '1');
INSERT INTO `Country` VALUES ('69', 'PLN', null, 'PL', 'Poland', 'Europe', '1');
INSERT INTO `Country` VALUES ('70', null, null, 'LY', 'Libya', 'Africa', '1');
INSERT INTO `Country` VALUES ('71', null, null, 'PR', 'Puerto Rico', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('72', 'JOD', null, 'JO', 'Jordan', 'Middle East', '1');
INSERT INTO `Country` VALUES ('73', null, null, 'MS', 'Montserrat', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('74', null, null, 'LA', 'Laos', 'Asia', '1');
INSERT INTO `Country` VALUES ('75', null, null, 'HT', 'Haiti', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('76', 'GBP', null, 'FO', 'Faroe Islands', 'Europe', '1');
INSERT INTO `Country` VALUES ('77', null, null, 'KY', 'Cayman Islands', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('78', 'XOF', null, 'CI', 'Cote D\'Ivoire', 'Africa', '1');
INSERT INTO `Country` VALUES ('79', null, null, 'AO', 'Angola', 'Africa', '1');
INSERT INTO `Country` VALUES ('80', null, null, 'MO', 'Macao', 'Asia', '1');
INSERT INTO `Country` VALUES ('81', null, null, 'GM', 'Gambia', 'Africa', '1');
INSERT INTO `Country` VALUES ('82', null, null, 'RS', 'Serbia', 'Europe', '1');
INSERT INTO `Country` VALUES ('83', null, null, 'MN', 'Mongolia', 'Asia', '1');
INSERT INTO `Country` VALUES ('84', null, null, 'FM', 'Micronesia', 'Oceania', '1');
INSERT INTO `Country` VALUES ('85', 'EUR', null, 'EE', 'Estonia', 'Europe', '1');
INSERT INTO `Country` VALUES ('86', 'EUR', null, 'IE', 'Ireland', 'Europe', '1');
INSERT INTO `Country` VALUES ('87', 'XOF', null, 'ML', 'Mali', 'Africa', '1');
INSERT INTO `Country` VALUES ('88', null, null, 'YE', 'Yemen', 'Middle East', '1');
INSERT INTO `Country` VALUES ('89', 'SAR', null, 'SA', 'Saudi Arabia', 'Middle East', '1');
INSERT INTO `Country` VALUES ('90', null, null, 'IQ', 'Iraq', 'Middle East', '1');
INSERT INTO `Country` VALUES ('91', null, null, 'LK', 'Sri Lanka', 'Asia', '1');
INSERT INTO `Country` VALUES ('92', 'VEF', null, 'VE', 'Venezuela', 'South America', '1');
INSERT INTO `Country` VALUES ('93', 'EUR', null, 'BE', 'Belgium', 'Europe', '1');
INSERT INTO `Country` VALUES ('94', null, null, 'IS', 'Iceland', 'Europe', '1');
INSERT INTO `Country` VALUES ('95', null, null, 'CK', 'Cook Islands', 'Oceania', '1');
INSERT INTO `Country` VALUES ('96', null, null, 'EC', 'Ecuador', 'South America', '1');
INSERT INTO `Country` VALUES ('97', null, null, 'NP', 'Nepal', 'Asia', '1');
INSERT INTO `Country` VALUES ('98', null, null, 'DM', 'Dominica', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('99', null, null, 'LB', 'Lebanon', 'Middle East', '1');
INSERT INTO `Country` VALUES ('100', null, null, 'MK', 'Macedonia', 'Europe', '1');
INSERT INTO `Country` VALUES ('101', 'THB', null, 'TH', 'Thailand', 'Asia', '1');
INSERT INTO `Country` VALUES ('102', null, null, 'BW', 'Botswana', 'Africa', '1');
INSERT INTO `Country` VALUES ('103', 'EUR', null, 'AT', 'Austria', 'Europe', '1');
INSERT INTO `Country` VALUES ('104', null, null, 'CR', 'Costa Rica', 'Central America', '1');
INSERT INTO `Country` VALUES ('105', null, null, 'AW', 'Aruba', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('106', 'MDL', null, 'MD', 'Moldova', 'Europe', '1');
INSERT INTO `Country` VALUES ('107', null, null, 'PA', 'Panama', 'Central America', '1');
INSERT INTO `Country` VALUES ('108', 'EUR', null, 'SK', 'Slovakia', 'Europe', '1');
INSERT INTO `Country` VALUES ('109', null, null, 'GY', 'Guyana', 'South America', '1');
INSERT INTO `Country` VALUES ('110', null, null, 'MU', 'Mauritius', 'Africa', '1');
INSERT INTO `Country` VALUES ('111', null, null, 'PK', 'Pakistan', 'Asia', '1');
INSERT INTO `Country` VALUES ('112', 'DKK', null, 'DK', 'Denmark', 'Europe', '1');
INSERT INTO `Country` VALUES ('113', null, null, 'PS', 'Palestinian Territory', 'Middle East', '1');
INSERT INTO `Country` VALUES ('114', null, null, 'SX', 'Sint Maarten', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('115', null, null, 'LI', 'Liechtenstein', 'Europe', '1');
INSERT INTO `Country` VALUES ('116', null, null, 'KI', 'Kiribati', 'Oceania', '1');
INSERT INTO `Country` VALUES ('117', null, null, 'ZM', 'Zambia', 'Africa', '1');
INSERT INTO `Country` VALUES ('118', 'AUD', null, 'AU', 'Australia', 'Oceania', '1');
INSERT INTO `Country` VALUES ('119', null, null, 'KN', 'Saint Kitts and Nevis', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('120', 'RUB', null, 'RU', 'Russia', 'Europe', '1');
INSERT INTO `Country` VALUES ('121', null, null, 'SR', 'Suriname', 'South America', '1');
INSERT INTO `Country` VALUES ('122', null, null, 'KE', 'Kenya', 'Africa', '1');
INSERT INTO `Country` VALUES ('123', 'RON', null, 'RO', 'Romania', 'Europe', '1');
INSERT INTO `Country` VALUES ('124', null, null, 'VN', 'Vietnam', 'Asia', '1');
INSERT INTO `Country` VALUES ('125', null, null, 'BS', 'Bahamas', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('126', null, null, 'SB', 'Solomon Islands', 'Oceania', '1');
INSERT INTO `Country` VALUES ('127', null, null, 'VG', 'U.K. Virgin Islands', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('128', null, null, 'KH', 'Cambodia', 'Asia', '1');
INSERT INTO `Country` VALUES ('129', 'HUF', null, 'HU', 'Hungary', 'Europe', '1');
INSERT INTO `Country` VALUES ('130', 'EUR', null, 'GF', 'French Guiana', 'South America', '1');
INSERT INTO `Country` VALUES ('131', null, null, 'MP', 'Northern Mariana Islands', 'Oceania', '1');
INSERT INTO `Country` VALUES ('132', 'TRY', null, 'TR', 'Turkey', 'Europe', '1');
INSERT INTO `Country` VALUES ('133', null, null, 'MC', 'Monaco', 'Europe', '1');
INSERT INTO `Country` VALUES ('134', 'NOK', null, 'NO', 'Norway', 'Europe', '1');
INSERT INTO `Country` VALUES ('135', 'MYR', null, 'MY', 'Malaysia', 'Asia', '1');
INSERT INTO `Country` VALUES ('136', null, null, 'XA', 'Abkhazia, Georgia', 'Europe', '1');
INSERT INTO `Country` VALUES ('137', null, null, 'LC', 'Saint Lucia', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('138', null, null, 'CM', 'Cameroon', 'Africa', '1');
INSERT INTO `Country` VALUES ('139', null, null, 'NI', 'Nicaragua', 'Central America', '1');
INSERT INTO `Country` VALUES ('140', 'QAR', null, 'QA', 'Qatar', 'Middle East', '1');
INSERT INTO `Country` VALUES ('141', null, null, 'BQ', 'Bonaire St Eustatius and Saba', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('142', null, null, 'BL', 'Saint Barthelemy', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('143', 'BRL', null, 'BR', 'Brazil', 'South America', '1');
INSERT INTO `Country` VALUES ('144', null, null, 'ST', 'São Tomé and Príncipe', 'Africa', '1');
INSERT INTO `Country` VALUES ('145', null, null, 'MQ', 'Martinique', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('146', 'AZM', null, 'AZ', 'Azerbaijan', 'Europe', '1');
INSERT INTO `Country` VALUES ('147', 'SGD', null, 'SG', 'Singapore', 'Asia', '1');
INSERT INTO `Country` VALUES ('148', null, null, 'VC', 'Saint Vincent & Grenadines', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('149', null, null, 'CW', 'Curaçao', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('150', null, null, 'AS', 'American Samoa', 'Asia', '1');
INSERT INTO `Country` VALUES ('151', 'CZK', null, 'CZ', 'Czech Republic', 'Europe', '1');
INSERT INTO `Country` VALUES ('152', null, null, 'PM', 'St. Pierre and Miquelon', 'North America', '1');
INSERT INTO `Country` VALUES ('153', null, null, 'DZ', 'Algeria', 'Africa', '1');
INSERT INTO `Country` VALUES ('154', 'ARS', null, 'AR', 'Argentina', 'South America', '1');
INSERT INTO `Country` VALUES ('155', null, null, 'AM', 'Armenia', 'Europe', '1');
INSERT INTO `Country` VALUES ('156', null, null, 'BD', 'Bangladesh', 'Asia', '1');
INSERT INTO `Country` VALUES ('157', 'ZAR', null, 'ZA', 'South Africa', 'Africa', '1');
INSERT INTO `Country` VALUES ('158', null, null, 'CC', 'Cocos (K) I.', 'Oceania', '1');
INSERT INTO `Country` VALUES ('159', 'EUR', null, 'DE', 'Germany', 'Europe', '1');
INSERT INTO `Country` VALUES ('160', 'GEL', null, 'GE', 'Georgia', 'Europe', '1');
INSERT INTO `Country` VALUES ('161', 'FJD', null, 'FJ', 'Fiji', 'Oceania', '1');
INSERT INTO `Country` VALUES ('162', null, null, 'HN', 'Honduras', 'Central America', '1');
INSERT INTO `Country` VALUES ('163', null, null, 'HR', 'Croatia', 'Europe', '1');
INSERT INTO `Country` VALUES ('164', 'CAD', null, 'CA', 'Canada', 'North America', '1');
INSERT INTO `Country` VALUES ('165', null, null, 'BZ', 'Belize', 'Central America', '1');
INSERT INTO `Country` VALUES ('166', 'MXP', null, 'MX', 'Mexico', 'North America', '1');
INSERT INTO `Country` VALUES ('167', null, null, 'BB', 'Barbados', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('168', 'EUR', null, 'AD', 'Andorra', 'Europe', '1');
INSERT INTO `Country` VALUES ('169', null, null, 'SC', 'Seychelles', 'Africa', '1');
INSERT INTO `Country` VALUES ('170', null, null, 'MR', 'Mauritania', 'Africa', '1');
INSERT INTO `Country` VALUES ('171', null, null, 'GD', 'Grenada', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('172', 'EGP', null, 'EG', 'Egypt', 'Africa', '1');
INSERT INTO `Country` VALUES ('173', null, null, 'NC', 'New Caledonia', 'Oceania', '1');
INSERT INTO `Country` VALUES ('174', null, null, 'TC', 'Turks & Caicos Islands', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('175', 'EUR', null, 'CY', 'Cyprus', 'Europe', '1');
INSERT INTO `Country` VALUES ('176', null, null, 'GH', 'Ghana', 'Africa', '1');
INSERT INTO `Country` VALUES ('177', null, null, 'XK', 'Kosovo', 'Europe', '1');
INSERT INTO `Country` VALUES ('178', null, null, 'MG', 'Madagascar', 'Africa', '1');
INSERT INTO `Country` VALUES ('179', null, null, 'MH', 'Marshall Islands', 'Oceania', '1');
INSERT INTO `Country` VALUES ('180', 'OMR', null, 'OM', 'Oman', 'Middle East', '1');
INSERT INTO `Country` VALUES ('181', 'USD', null, 'VI', 'U.S. Virgin Islands', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('182', 'CLP', null, 'CL', 'Chile', 'South America', '1');
INSERT INTO `Country` VALUES ('183', null, null, 'NU', 'Niue', 'Oceania', '1');
INSERT INTO `Country` VALUES ('184', null, null, 'CV', 'Cape Verde', 'Africa', '1');
INSERT INTO `Country` VALUES ('185', null, null, 'UZ', 'Uzbekistan', 'Asia', '1');
INSERT INTO `Country` VALUES ('186', 'EUR', null, 'FR', 'France', 'Europe', '1');
INSERT INTO `Country` VALUES ('187', null, null, 'BH', 'Bahrain', 'Middle East', '1');
INSERT INTO `Country` VALUES ('188', null, null, 'SV', 'El Salvador', 'Central America', '1');
INSERT INTO `Country` VALUES ('189', null, null, 'TL', 'East Timor', 'Asia', '1');
INSERT INTO `Country` VALUES ('190', null, null, 'PG', 'Papua New Guinea', 'Oceania', '1');
INSERT INTO `Country` VALUES ('191', null, null, 'BN', 'Brunei Darussalam', 'Asia', '1');
INSERT INTO `Country` VALUES ('192', null, null, 'BY', 'Belarus', 'Europe', '1');
INSERT INTO `Country` VALUES ('193', null, null, 'MM', 'Myanmar', 'Asia', '1');
INSERT INTO `Country` VALUES ('194', null, null, 'TZ', 'Tanzania', 'Africa', '1');
INSERT INTO `Country` VALUES ('195', null, null, 'DO', 'Dominican Republic', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('196', 'EUR', null, 'IT', 'Italy', 'Europe', '1');
INSERT INTO `Country` VALUES ('197', null, null, 'AF', 'Afghanistan', 'Middle East', '1');
INSERT INTO `Country` VALUES ('198', null, null, 'WF', 'Wallis and Futuna', 'Australia - New Zealand and the South Pacific', '1');
INSERT INTO `Country` VALUES ('199', null, null, 'ER', 'Eritrea', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('200', null, null, 'TM', 'Turkmenistan', 'Asia', '1');
INSERT INTO `Country` VALUES ('201', null, null, 'CU', 'Cuba', 'Caribbean', '1');
INSERT INTO `Country` VALUES ('202', null, null, 'SD', 'Sudan', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('203', null, null, 'SO', 'Federal Republic of Somalia', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('204', null, null, 'RW', 'Rwanda', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('205', null, null, 'KP', 'North Korea', 'Asia', '1');
INSERT INTO `Country` VALUES ('206', null, null, 'SJ', 'Svalbard', 'Europe', '1');
INSERT INTO `Country` VALUES ('207', null, null, 'BI', 'Burundi', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('208', null, null, 'GQ', 'Equatorial Guinea', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('209', null, null, 'MZ', 'Mozambique', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('210', null, null, 'KM', 'Comoros', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('211', null, null, 'SS', 'South Sudan', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('212', 'XOF', null, 'NE', 'Niger', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('213', null, null, 'DJ', 'Djibouti', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('214', null, null, 'CF', 'Central African Republic', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('215', null, null, 'TD', 'Chad', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('216', null, null, 'BV', 'Bouvet Island', 'Antarctica', '1');
INSERT INTO `Country` VALUES ('217', null, null, 'UG', 'Uganda', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('218', null, null, 'IR', 'Iran', 'Middle East', '1');
INSERT INTO `Country` VALUES ('219', null, null, 'PN', 'Pitcairn Island', 'Australia - New Zealand and the South Pacific', '1');
INSERT INTO `Country` VALUES ('220', 'XOF', null, 'BJ', 'Benin', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('221', null, null, 'SH', 'St. Helena', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('222', null, null, 'UM', 'US Minor Outlying Islands', 'Australia - New Zealand and the South Pacific', '1');
INSERT INTO `Country` VALUES ('223', null, null, 'TJ', 'Tajikistan', 'Asia', '1');
INSERT INTO `Country` VALUES ('224', null, null, 'LR', 'Liberia', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('225', null, null, 'HM', 'Heard and McDonald Islands', 'Antarctica', '1');
INSERT INTO `Country` VALUES ('226', null, null, 'NR', 'Nauru', 'Australia - New Zealand and the South Pacific', '1');
INSERT INTO `Country` VALUES ('227', 'XOF', null, 'GW', 'Guinea-Bissau', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('228', null, null, 'MW', 'Malawi', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('229', 'EUR', null, 'VA', 'Vatican City', 'Europe', '1');
INSERT INTO `Country` VALUES ('230', null, null, 'IO', 'British Indian Ocean Territory', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('231', null, null, 'CD', 'Democratic Republic of the Congo', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('232', null, null, 'TG', 'Togo', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('233', null, null, 'TV', 'Tuvalu', 'Australia - New Zealand and the South Pacific', '1');
INSERT INTO `Country` VALUES ('234', null, null, 'GA', 'Gabon', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('235', null, null, 'SL', 'Sierra Leone', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('236', null, null, 'TK', 'Tokelau', 'Australia - New Zealand and the South Pacific', '1');
INSERT INTO `Country` VALUES ('237', null, null, 'CX', 'Christmas Island', 'Australia - New Zealand and the South Pacific', '1');
INSERT INTO `Country` VALUES ('238', 'EUR', null, 'TF', 'French Southern and Antarctic Territories', 'Antarctica', '1');
INSERT INTO `Country` VALUES ('239', null, null, 'ZW', 'Zimbabwe', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('240', null, null, 'ET', 'Ethiopia', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('241', 'XOF', null, 'BF', 'Burkina Faso', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('242', null, null, 'GN', 'Guinea', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('243', null, null, 'CG', 'Republic of the Congo', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('244', null, null, 'YT', 'Mayotte', 'Africa and Indian Ocean', '1');
INSERT INTO `Country` VALUES ('245', 'EUR', null, 'AN', 'Netherlands Antilles', null, '1');
INSERT INTO `Country` VALUES ('246', null, null, 'AQ', 'Antarctica', null, '1');
INSERT INTO `Country` VALUES ('247', null, null, 'XC', 'Crimea', 'Europe', '1');
