/*
Navicat MySQL Data Transfer

Source Server         : MariaDB2 - Master
Source Server Version : 50533
Target Server Type    : MYSQL
Target Server Version : 50533
File Encoding         : 65001

Date: 2014-12-03 16:36:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `PointOfInterest`
-- ----------------------------
DROP TABLE IF EXISTS `PointOfInterest`;
CREATE TABLE `PointOfInterest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `cityId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_CityId` (`cityId`),
  CONSTRAINT `fk_CityId` FOREIGN KEY (`cityId`) REFERENCES `City` (`cityId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of PointOfInterest
-- ----------------------------
