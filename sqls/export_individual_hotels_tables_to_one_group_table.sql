#EXPORT ALL POSTMETA TABLES TO ONE BIG TABLE
drop table if exists all_postmeta;

create table all_postmeta as 
select 1 as hotelId, r.*
from wp_2_postmeta r;

select * from all_postmeta;

delete from all_postmeta;

insert into all_postmeta 
select 2, p2.* from wp_2_postmeta as p2
union select 3, p3.* from wp_3_postmeta as p3
union select 4, p4.* from wp_4_postmeta as p4 
union select 5, p5.* from wp_5_postmeta as p5 
union select 6, p6.* from wp_6_postmeta as p6 
union select 7, p7.* from wp_7_postmeta as p7
union select 8, p8.* from wp_8_postmeta as p8
union select 9, p9.* from wp_9_postmeta as p9
union select 10, p10.* from wp_10_postmeta as p10
union select 11, p11.* from wp_11_postmeta as p11
union select 12, p12.* from wp_12_postmeta as p12
union select 13, p13.* from wp_13_postmeta as p13
union select 14, p14.* from wp_14_postmeta as p14
union select 15, p15.* from wp_15_postmeta as p15
union select 16, p16.* from wp_16_postmeta as p16
union select 17, p17.* from wp_17_postmeta as p17
union select 18, p18.* from wp_18_postmeta as p18
union select 19, p19.* from wp_19_postmeta as p19
union select 20, p20.* from wp_20_postmeta as p20;

#EXPORT ALL POSTS TABLES TO ONE BIG TABLE
drop table if exists all_posts;

create table all_posts as 
select 1 as hotelId, r.*
from wp_2_posts r;

select * from all_posts;

delete from all_posts;

insert into all_posts 
select 2, p2.* from wp_2_posts as p2
union select 3, p3.* from wp_3_posts as p3
union select 4, p4.* from wp_4_posts as p4 
union select 5, p5.* from wp_5_posts as p5 
union select 6, p6.* from wp_6_posts as p6 
union select 7, p7.* from wp_7_posts as p7
union select 8, p8.* from wp_8_posts as p8
union select 9, p9.* from wp_9_posts as p9
union select 10, p10.* from wp_10_posts as p10
union select 11, p11.* from wp_11_posts as p11
union select 12, p12.* from wp_12_posts as p12
union select 13, p13.* from wp_13_posts as p13
union select 14, p14.* from wp_14_posts as p14
union select 15, p15.* from wp_15_posts as p15
union select 16, p16.* from wp_16_posts as p16
union select 17, p17.* from wp_17_posts as p17
union select 18, p18.* from wp_18_posts as p18
union select 19, p19.* from wp_19_posts as p19
union select 20, p20.* from wp_20_posts as p20;


#EXPORT ALL RESERVATIONS TABLES TO ONE BIG TABLE
drop table if exists all_reservations;

create table all_reservations as 
select 1 as hotelId, r.*
from wp_2_reservations r;

select * from all_reservations;

delete from all_reservations;

insert into all_reservations 
select 2, p2.* from wp_2_reservations as p2
union select 3, p3.* from wp_3_reservations as p3
union select 4, p4.* from wp_4_reservations as p4 
union select 5, p5.* from wp_5_reservations as p5 
union select 6, p6.* from wp_6_reservations as p6 
union select 7, p7.* from wp_7_reservations as p7
union select 8, p8.* from wp_8_reservations as p8
union select 9, p9.* from wp_9_reservations as p9
union select 10, p10.* from wp_10_reservations as p10
union select 11, p11.* from wp_11_reservations as p11
union select 12, p12.* from wp_12_reservations as p12
union select 13, p13.* from wp_13_reservations as p13
union select 14, p14.* from wp_14_reservations as p14
union select 15, p15.* from wp_15_reservations as p15
union select 16, p16.* from wp_16_reservations as p16
union select 17, p17.* from wp_17_reservations as p17
union select 18, p18.* from wp_18_reservations as p18
union select 19, p19.* from wp_19_reservations as p19
union select 20, p20.* from wp_20_reservations as p20;