select distinct roomnumber
from wp_4_reservations r where 1=1
and now() between arrival and departure
and room = 10;

update wp_4_reservations set arrival = arrival - INTERVAL 3 HOUR;

update wp_4_reservations set arrival = arrival + INTERVAL 3 HOUR;

select from_unixtime() from wp_4_reservations limit 1;
