SELECT 2 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_2_posts as room   #all posts (resources(rooms))
JOIN wp_2_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_2_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_2_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_2_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 3 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_3_posts as room   #all posts (resources(rooms))
JOIN wp_3_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_3_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_3_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_3_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 4 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_4_posts as room   #all posts (resources(rooms))
JOIN wp_4_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_4_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_4_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_4_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 5 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_5_posts as room   #all posts (resources(rooms))
JOIN wp_5_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_5_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_5_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_5_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 6 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_15_posts as room   #all posts (resources(rooms))
JOIN wp_6_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_6_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_6_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_6_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 7 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_7_posts as room   #all posts (resources(rooms))
JOIN wp_7_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_7_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_7_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_7_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 8 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_8_posts as room   #all posts (resources(rooms))
JOIN wp_8_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_8_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_8_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_8_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 9 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_9_posts as room   #all posts (resources(rooms))
JOIN wp_9_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_9_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_9_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_9_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 10 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_10_posts as room   #all posts (resources(rooms))
JOIN wp_10_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_10_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_10_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_10_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 11 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_11_posts as room   #all posts (resources(rooms))
JOIN wp_11_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_11_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_11_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_11_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 12 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_12_posts as room   #all posts (resources(rooms))
JOIN wp_12_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_12_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_12_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_12_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 13 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_13_posts as room   #all posts (resources(rooms))
JOIN wp_13_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_13_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_13_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_13_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 14 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_14_posts as room   #all posts (resources(rooms))
JOIN wp_14_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_14_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_14_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_14_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 15 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_15_posts as room   #all posts (resources(rooms))
JOIN wp_15_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_15_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_15_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_15_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 16 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_16_posts as room   #all posts (resources(rooms))
JOIN wp_16_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_16_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_16_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_16_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 17 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_17_posts as room   #all posts (resources(rooms))
JOIN wp_17_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_17_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_17_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_17_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 18 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_18_posts as room   #all posts (resources(rooms))
JOIN wp_18_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_18_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_18_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_18_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 19 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_19_posts as room   #all posts (resources(rooms))
JOIN wp_19_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_19_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_19_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_19_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID
UNION
SELECT 20 as hotel, room.ID, roomcount.meta_value - count(distinct reservations.roomnumber) as available, requirements.meta_value as requirements, filters.meta_value as filters
FROM 
wp_20_posts as room   #all posts (resources(rooms))
JOIN wp_20_postmeta as roomcount on(room.ID = roomcount.post_id 
                                   AND roomcount.meta_key = 'roomcount'
                                   AND meta_value REGEXP '^-?[0-9]+$') #join with roomcount postmeta of per-object rooms
JOIN wp_20_postmeta as requirements on(room.ID = requirements.post_id 
                                      AND requirements.meta_key = 'easy-resource-req') #join with requirements postmeta
LEFT JOIN wp_20_postmeta as filters on(room.ID = filters.post_id 
                                 AND filters.meta_key = 'easy_res_filter') #join with filters postmeta
LEFT JOIN wp_20_reservations reservations on(reservations.room = room.id 
                                            AND NOT((reservations.arrival < '2014-12-28 00:00:00' AND reservations.departure < '2014-12-28 00:00:00') OR
													(reservations.arrival > '2015-01-08 00:00:00' AND reservations.departure > '2015-01-08 00:00:00') )) 
GROUP BY room.ID;

