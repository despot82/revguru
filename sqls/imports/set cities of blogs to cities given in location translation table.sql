UPDATE wp_blogs bl
JOIN hotels_location_translation hlt ON(bl.hotel_import_id = hlt.hotel_id) #join with hotels location translations
JOIN city ON(city.cityName = hlt.city_name) #connect to city table
SET bl.cityId = city.cityId;